--  START TRANSACTION;
--  ROLLBACK;
--
-- Table structure for table `ref_address_type`
--

CREATE TABLE if not exists `ref_address_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` TEXT DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `address`
--

CREATE TABLE if not exists `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` TEXT DEFAULT NULL,
  `ref_address_type_id` int(11) DEFAULT NULL,
  `name` TEXT DEFAULT NULL,
  `shortname` TEXT DEFAULT NULL,
  `parent_guid` TEXT DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_address_address_type_idx` (`ref_address_type_id`),

  CONSTRAINT `fk_address_address_type` FOREIGN KEY (`ref_address_type_id`) REFERENCES `ref_address_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `ref_enterprise_type`
--

CREATE TABLE if not exists `ref_enterprise_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` TEXT NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `ref_business_entity_type`
--

CREATE TABLE if not exists `ref_business_entity_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` TEXT NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `ref_vet_document_form`
--

CREATE TABLE if not exists `ref_vet_document_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` TEXT NOT NULL,
  `description` TEXT DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `ref_vet_document_type`
--

CREATE TABLE if not exists `ref_vet_document_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` TEXT NOT NULL,
  `description` TEXT DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `ref_document_status`
--

CREATE TABLE if not exists `ref_vet_document_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` TEXT NOT NULL,
  `description` TEXT DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `ref_product`
--

CREATE TABLE if not exists `ref_product_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` TEXT NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `ref_product`
--

CREATE TABLE if not exists `ref_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` TEXT NOT NULL,
  `name` TEXT NOT NULL,
  `code` TEXT NOT NULL,
  `ref_product_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),

  KEY `fk_product_product_type_idx` (`ref_product_type_id`),

  CONSTRAINT `fk_product_product_type` FOREIGN KEY (`ref_product_type_id`) REFERENCES `ref_product_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `ref_sub_product`
--

CREATE TABLE if not exists `ref_sub_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` TEXT NOT NULL,
  `name` TEXT NOT NULL,
  `code` TEXT NOT NULL,
  `ref_product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sub_product_product_idx` (`ref_product_id`),

  CONSTRAINT `fk_sub_product_product` FOREIGN KEY (`ref_product_id`) REFERENCES `ref_product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `ref_product`
--

CREATE TABLE if not exists `ref_unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` TEXT NOT NULL,
  `name` TEXT NOT NULL,
  `full_name` TEXT DEFAULT NULL,
  `factor` int DEFAULT 1,
  `ref_common_unit_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_unit_common_unit_idx` (`ref_common_unit_id`),

  CONSTRAINT `fk_unit_common_unit` FOREIGN KEY  (`ref_common_unit_id`) REFERENCES `ref_unit` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `business_entity`
--

CREATE TABLE if not exists `business_entity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` TEXT DEFAULT NULL,
  `name` TEXT DEFAULT NULL,
  `name` TEXT DEFAULT NULL,
  `ref_business_entity_type_id` int(11) DEFAULT NULL,
  `inn` TEXT DEFAULT NULL,
  `kpp` TEXT DEFAULT NULL,
  `ogrn` TEXT DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `locality_id` int(11) DEFAULT NULL,
  `address_view` TEXT DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_business_entity_business_entity_type_idx` (`ref_business_entity_type_id`),
  KEY `fk_business_entity_country_idx` (`country_id`),
  KEY `fk_business_entity_region_idx` (`region_id`),
  KEY `fk_business_entity_district_idx` (`district_id`),
  KEY `fk_business_entity_locality_idx` (`locality_id`),
  CONSTRAINT `fk_business_entity_business_entity_type` FOREIGN KEY (`ref_business_entity_type_id`) REFERENCES `ref_business_entity_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_business_entity_country` FOREIGN KEY (`country_id`) REFERENCES `address` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_business_entity_region` FOREIGN KEY (`region_id`) REFERENCES `address` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_business_entity_district` FOREIGN KEY (`district_id`) REFERENCES `address` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_business_entity_locality` FOREIGN KEY (`locality_id`) REFERENCES `address` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `enterprise`
--

CREATE TABLE if not exists `enterprise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` TEXT DEFAULT NULL,
  `name` TEXT DEFAULT NULL,
  `ref_enterprise_type_id` int(11) DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `locality_id` int(11) DEFAULT NULL,
  `address_view` TEXT DEFAULT NULL,
  `business_entity_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_enterprise_enterprise_type_idx` (`ref_enterprise_type_id`),
  KEY `fk_enterprise_country_idx` (`country_id`),
  KEY `fk_enterprise_region_idx` (`region_id`),
  KEY `fk_enterprise_district_idx` (`district_id`),
  KEY `fk_enterprise_locality_idx` (`locality_id`),
  KEY `fk_enterprise_business_entity_idx` (`business_entity_id`),


  CONSTRAINT `fk_enterprise_enterprise_type` FOREIGN KEY (`ref_enterprise_type_id`) REFERENCES `ref_enterprise_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_enterprise_country` FOREIGN KEY (`country_id`) REFERENCES `address` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_enterprise_region` FOREIGN KEY (`region_id`) REFERENCES `address` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_enterprise_district` FOREIGN KEY (`district_id`) REFERENCES `address` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_enterprise_locality` FOREIGN KEY (`locality_id`) REFERENCES `address` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_enterprise_business_entity` FOREIGN KEY (`business_entity_id`) REFERENCES `business_entity` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `endpoint`
--

CREATE TABLE if not exists `endpoint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_entity_id` int(11) DEFAULT NULL,
  `enterprise_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_endpoint_business_entity` FOREIGN KEY (`business_entity_id`) REFERENCES `business_entity` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_endpoint_enterprise_type` FOREIGN KEY (`enterprise_id`) REFERENCES `enterprise` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `batch`
--

CREATE TABLE if not exists `batch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_product_id` int(11) NOT NULL,
  `ref_sub_product_id` int(11) NOT NULL,
  `productItem` TEXT DEFAULT NULL,
  `volume` FLOAT DEFAULT 0.0,
  `ref_unit_id` int(11) NOT NULL,
  `dateOfProduction` date DEFAULT NULL,
  `expiryDate` date DEFAULT NULL,
  `country_of_origin_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_batch_product_idx` (`ref_product_id`),
  KEY `fk_batch_sub_product_idx` (`ref_sub_product_id`),
  KEY `fk_batch_unit_idx` (`ref_unit_id`),
  KEY `fk_batch_country_of_origin_idx` (`country_of_origin_id`),

  CONSTRAINT `fk_batch_product` FOREIGN KEY (`ref_product_id`) REFERENCES `ref_product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_batch_sub_product` FOREIGN KEY (`ref_sub_product_id`) REFERENCES `ref_sub_product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_batch_unit` FOREIGN KEY (`ref_unit_id`) REFERENCES ref_unit (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_batch_country_of_origin` FOREIGN KEY (`country_of_origin_id`) REFERENCES `address` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION


) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `office`
--

CREATE TABLE if not exists `office` (
  `id`   INT(11) NOT NULL AUTO_INCREMENT,
  `name` TEXT DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `expert`
--

CREATE TABLE if not exists `expert` (
  `id`   INT(11) NOT NULL AUTO_INCREMENT,
  `name` TEXT DEFAULT NULL,
  `office_id` INT(11) DEFAULT NULL,

  PRIMARY KEY (`id`),
  KEY `fk_expert_office_idx` (`office_id`),

  CONSTRAINT `fk_expert_office` FOREIGN KEY (`office_id`) REFERENCES `office` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `vet_document`
--

CREATE TABLE if not exists `vet_document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` TEXT DEFAULT NULL,
  `issueSeries` TEXT DEFAULT NULL,
  `issueNumber` TEXT DEFAULT NULL,
  `issueDate` date DEFAULT NULL,
  `ref_vet_document_form_id` int(11) NOT NULL,
  `ref_vet_document_type_id` int(11) NOT NULL,
  `ref_vet_document_status_id` int(11) NOT NULL,

  `consignor_endpoint_id` int(11) NOT NULL,
  `consignee_endpoint_id` int(11) NOT NULL,

  `ref_batch_id` int(11) NOT NULL,

  `ref_expert_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_vet_document_form_idx` (`ref_vet_document_form_id`),
  KEY `fk_vet_document_type_idx` (`ref_vet_document_type_id`),
  KEY `fk_vet_document_status_idx` (`ref_vet_document_status_id`),
  KEY `fk_vet_document_consignor_endpoint_idx` (`consignor_endpoint_id`),
  KEY `fk_vet_document_consignee_endpoint_idx` (`consignee_endpoint_id`),
  KEY `fk_vet_document_batch_idx` (`ref_batch_id`),
  KEY `fk_vet_document_expert_idx` (`ref_expert_id`),

  CONSTRAINT `fk_vet_document_document_form` FOREIGN KEY (`ref_vet_document_form_id`) REFERENCES `ref_vet_document_form` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_vet_document_document_type` FOREIGN KEY (`ref_vet_document_type_id`) REFERENCES `ref_vet_document_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_vet_document_document_status` FOREIGN KEY (`ref_vet_document_status_id`) REFERENCES `ref_vet_document_status` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_vet_document_consignor_endpoint` FOREIGN KEY (`consignor_endpoint_id`) REFERENCES `endpoint` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_vet_document_consignee_endpoint` FOREIGN KEY (`consignee_endpoint_id`) REFERENCES `endpoint` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_vet_document_batch` FOREIGN KEY (`ref_batch_id`) REFERENCES `batch` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_vet_document_expert` FOREIGN KEY (`ref_expert_id`) REFERENCES `expert` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION

) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;