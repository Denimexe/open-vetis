package ru.denimexe.vetis.web.JMS;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;
//import ru.denimexe.vetis.core.data.entities.Notification;
//import ru.denimexe.vetis.core.data.services.NotificationService;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by denimexe on 16.10.17.
 */
@Service
public class WebJMSListener {

    private final Logger LOGGER = LogManager.getLogger(WebJMSListener.class);

//    @Autowired
//    private NotificationService notificationService;

    @JmsListener(destination = "web")
    public void processMessage(String text){
        System.out.println("Web received: " + text);
        //notificationService.save(new Notification(text));
        //LOGGER.info(new SimpleDateFormat("dd.MM.yy [hh:mm:ss]").format(new Date()) + ": '" + text + "' ");
    }
}
