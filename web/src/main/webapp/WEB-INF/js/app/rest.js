(function () {
    'use strict';

    var coreUrl = "http://localhost:8080/vetis_core/";

    angular.module("vetis.rest", [
        'ngResource'
    ])
        .factory('NotificationFactory', ['$resource', function ($resource) {
            return {
                notifications: $resource(coreUrl + 'rest/notifications'),
                viewed: $resource(coreUrl + 'rest/notifications/viewed/:id', {id: '@id'})
            }
        }])
        .factory('AddressFactory', ['$resource', function ($resource) {
            return {
                addresses: $resource(coreUrl + 'rest/values'),
                addressesByType: $resource(coreUrl + 'rest/values/bytype/:type', {type: '@type'}),
                addressesByParentGuid: $resource(coreUrl + 'rest/values/byparentguid/:guid', {guid: '@guid'}),

                address: $resource(coreUrl + 'rest/value/:id', {id: '@id'}),
                addressByGuid: $resource(coreUrl + 'rest/value/guid/:guid', {guid: '@guid'}),

                types: $resource(coreUrl + 'rest/types'),

                type: $resource(coreUrl + 'rest/type/:id', {id: '@id'}),
                typeByName: $resource(coreUrl + 'rest/type/name/:name', {name: '@name'})

            };
        }])

        .factory('DictionaryFactory', ['$resource', function ($resource) {
            return {
                units: $resource(coreUrl + 'rest/units')
            };
        }])

        .factory('EnterpriseFactory', ['$resource', '$http', function ($resource, $http) {
            return {
                enterpriseTypes: $resource(coreUrl + 'rest/enterprise/types'),

                businessEntityTypes: $resource(coreUrl + 'rest/businessEntity/types'),

                businessEntities: $resource(coreUrl + 'rest/businessEntity/all'),

                businessEntityFull: $resource(coreUrl + 'rest/businessEntity/full/:id', {id: '@id'}),

                enterprises: $resource(coreUrl + 'rest/enterprise/all'),

                loadBusinessEntity: $resource(coreUrl + 'rest/businessEntity/get/:guid', {guid: '@guid'}),

                loadBusinessEntityByInn: $resource(coreUrl + 'rest/businessEntity/get/inn/:inn', {inn: '@inn'}),

                loadEnterprise: $resource(coreUrl + 'rest/enterprise/get/:guid', {guid: '@guid'}),

                businessEntityEnterprises: $resource(coreUrl + 'rest/businessEntity/enterprises/:id', {id: '@id'}),

                putEnterprise: function(data) {
                    // console.log("i was here");
                    // $resource(coreUrl + 'rest/enterprise/save', {data: data}).post();
                    return $http.post(coreUrl + 'rest/enterprise/save', data)
                        .success(function(data) {
                            return data === "true";
                        })
                },

                putBusinessEntity: function(data) {
                    var result = false;

                    return $http.post(coreUrl + 'rest/businessEntity/save', data)
                        .then( function(data) {
                            return data === "true";
                        });
                },

                putBusinessEntityEnterprises: function(data) {

                    return $http.post(coreUrl + 'rest/businessEntity/saveEnterprises', data)
                    .then(function(result) {
                        return result === "true";
                    })
                }
            };
        }])

        .factory('DocumentFactory', ['$resource', '$http', function ($resource, $http) {
            return {

                businessEntities: $resource(coreUrl + 'rest/businessEntity/all'),

                businessEntityEnterprises: $resource(coreUrl + 'rest/businessEntity/enterprises/:id', {id: '@id'}),

                getReport: function() {
                    return $http.post(coreUrl + 'rest/document/report')
                        .then(function(result) {
                            return result.data;
                        });
                },

                documentsRequest: function(beId, eId) {
                    return $http.post(coreUrl + 'rest/document/request/' + beId + '/' + eId)
                    .then(function(result) {
                        return result.data;
                    });
                },

                fetchDocuments: function(id) {
                    return $http.post(coreUrl + 'rest/document/request/:id', {id: '@id'})
                        .then(function(result) {
                            return result.data;
                        });
                },

                requests: $resource(coreUrl + 'rest/document/request/all')


            };
        }])

        ;



}());