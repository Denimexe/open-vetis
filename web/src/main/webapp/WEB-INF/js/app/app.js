(function () {
    'use strict';

    angular.module("vetis", [
            'ui.router',
            'vetis.rest',
            'vetis.main',
            'fxpicklist'
        ])
        .config(['$urlRouterProvider', function ($urlRouterProvider) {
            $urlRouterProvider.otherwise('/');
        }])
        .config(function($stateProvider){
            $stateProvider.state({
                name: 'empty',
                url: '/',
                templateUrl: 'views/main.html',
            });
            // $stateProvider.state({
            //     name: 'dictionary',
            //     url: '/dictionary',
            //     templateUrl: 'views/dictionary.html',
            // });
            // $stateProvider.state({
            //     name: 'employees',
            //     url: '/employees',
            //     templateUrl: 'views/employees.html',
            // });
            // $stateProvider.state({
            //     name: 'journal',
            //     url: '/journal',
            //     templateUrl: 'views/journal.html',
            // });
            // $stateProvider.state({
            //     name: 'reports',
            //     url: '/reports',
            //     templateUrl: 'views/reports.html',
            // });
            // Clients перенесены в ClientJournalController
            // $stateProvider.state({
            //     name: 'pets',
            //     url: '/clinic/pets',
            //     templateUrl: 'views/clinic/pets.html',
            // });
            // $stateProvider.state({
            //     name: 'vaccinations',
            //     url: '/vaccinations',
            //     templateUrl: 'views/clinic/vaccinations.html',
            // });
            // $stateProvider.state({
            //     name: 'settings',
            //     url: '/settings',
            //     templateUrl: 'views/settings.html',
            // });
            // $stateProvider.state({
            //     name: 'login',
            //     url: '/login',
            //     templateUrl: 'login.html',
            // });
        })
        // .controller("HelloController", [ '$scope', 'HelloMessages', function($scope, HelloMessages) {
        //     $scope.helloTo = {};
        //
        //     HelloMessages.get({id: 1}, function (data) {
        //         $scope.helloTo.Comment = data;
        //         console.log(data);
        //     });
        //     $scope.helloTo.title = "World, AngularJS";
        // }]);

}());
