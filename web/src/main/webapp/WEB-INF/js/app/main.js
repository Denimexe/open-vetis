(function () {
    'use strict';

    function treatAsUTC(date) {
        var result = new Date(date);
        result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
        return result;
    }

    angular.module("vetis.main", [
        'ui.router',
        "vetis.rest",
        'ui.bootstrap',
        'localytics.directives'
    ]).config(['$stateProvider', function ($stateProvider) {
        $stateProvider.state('main', {
            url: '/main',
            templateUrl: 'views/main.html',
            controller: 'MainController'
        });

        $stateProvider.state({
            name: 'address',
            url: '/address',
            templateUrl: 'views/address.html',
            controller: 'AddressController'
        });

        $stateProvider.state({
            name: 'dictionary',
            url: '/dictionary',
            templateUrl: 'views/dictionary.html',
            controller: 'DictionaryController'
        });

        $stateProvider.state({
            name: 'enterprise',
            url: '/enterprise',
            templateUrl: 'views/enterprise.html',
            controller: 'EnterpriseController'
        });

        $stateProvider.state({
            name: 'document',
            url: '/document',
            templateUrl: 'views/document.html',
            controller: 'DocumentController'
        });
    }])
    // Контроллер для управления стартовой страницей и меню навигации
        .controller("MainController",
            [ '$scope', 'NotificationFactory', function($scope, notificationFactory) {

                $scope.fetchResult = "failure";
                $scope.showTypeSingleResult = false;

                $scope.notifications = [];//new Map();
                $scope.unviewedAmount = 0;

                $scope.fetchNotifications = function () {
                    var received;

                    notificationFactory.notifications.query(function (data) {
                        received = data;

                        received.forEach(function (entry) {
                            //$scope.notifications.set(entry.id, entry);
                            // if(!entry.viewed) {
                            //     newNotificationsAmount++;
                            // }

                            var i = 0;
                            var consist = false;

                            for(i; i < $scope.notifications.length; i++) {
                                if ($scope.notifications[i].id === entry.id) {
                                    consist = true;
                                    break;
                                }
                            }
                            if(!consist) {
                                $scope.notifications.push(entry);
                                if(!entry.viewed) {
                                    $scope.unviewedAmount++;
                                }

                            }
                        });

                    });
                };

                $scope.formatDate = function(date){

                    var d = new Date(date);
                    var curdate = new Date();
                    var millisecondsPerDay = 24 * 60 * 60 * 1000;
                    var deltaDate = (treatAsUTC(curdate) - treatAsUTC(d)) / millisecondsPerDay;

                    var timestr = "";

                    if(deltaDate > 0 && deltaDate <= 1) {
                        timestr = "сегодня, ";
                    } else if(deltaDate > 1 && deltaDate <= 2) {
                        timestr = "вчера, ";
                    } else {
                        timestr = (d.getDate() < 9 ? "0" : "") + d.getDate() + "." +
                            (d.getMonth() < 9 ? "0" : "") + (d.getMonth() + 1) + "." +
                            (d.getYear() < 110 ? "0" : "") + (d.getYear() - 100) + " ";
                    }
                    timestr += (d.getHours() < 10 ? "0" : "") + d.getHours() + ":" +
                        (d.getMinutes() < 10 ? "0" : "") + d.getMinutes();

                    return timestr;
                };

                $scope.setViewed = function(item) {
                    item.viewed = true;
                    notificationFactory.Viewed.get({id: item.id});

                };

                $scope.expired = function(date) {
                    var curdate = new Date();
                    var millisecondsPerMinute = 60 * 1000;
                    var deltaMinute = (treatAsUTC(curdate) - treatAsUTC(date)) / millisecondsPerMinute;
                    return deltaMinute > 0.2;
                };

                $scope.fetchNotifications();

                // /*
                setInterval(function() {
                    $scope.fetchNotifications();
                }, 5000);
                // */

            }]

        )
        .controller("AddressController",
            [ '$scope', 'AddressFactory', function($scope, addressFactory) {
                $scope.id = 1;
                $scope.name = "";

                $scope.typeSingleResult = null;
                $scope.typeTableResult = "";

                $scope.showTypeTableResult = false;
                $scope.showTypeSingleResult = false;

                $scope.countries = [];
                $scope.country = "";
                $scope.regions = [];
                $scope.region = "";
                $scope.districts = [];
                $scope.district = "";
                $scope.localities = [];
                $scope.locality = "";

                $scope.getTypes = function () {
                    $scope.showTypeTableResult = false;
                    $scope.typeTableResult = addressFactory.types.query();
                    $scope.showTypeTableResult = true;
                };

                $scope.getType = function () {
                    $scope.showTypeSingleResult = false;
                    $scope.typeSingleResult = addressFactory.type.get({id: $scope.id});
                    $scope.showTypeSingleResult = true;
                };

                $scope.getTypeByName = function () {
                    $scope.showTypeSingleResult = false;
                    $scope.typeSingleResult = addressFactory.typeByName.get({name: $scope.name});
                    $scope.showTypeSingleResult = true;
                };

                //~~~~~~~~~~~

                $scope.singleResult = null;
                $scope.tableResult = "";

                $scope.showTableResult = false;
                $scope.showSingleResult = false;

                $scope.getAddresses = function () {
                    $scope.showTableResult = false;
                    $scope.tableResult = addressFactory.addresses.query();
                    $scope.showTableResult = true;
                };

                $scope.getAddress = function () {
                    $scope.showSingleResult = false;
                    $scope.singleResult = addressFactory.address.get({id: $scope.id});
                    $scope.showSingleResult = true;
                };

                $scope.getAddressesByTypeId = function (_id) {
                    $scope.showTableResult = false;
                    $scope.tableResult = addressFactory.addressesByType.query({type: _id});
                    $scope.showTableResult = true;
                };


                $scope.getCountries = function () {
                    var received;
                    addressFactory.addressesByType.query({type: 1}, function (data) {
                        received = data;

                        $scope.countries = [];
                        $scope.regions = [];
                        $scope.districts = [];
                        $scope.localities = [];
                        $scope.country = "";
                        $scope.region = "";
                        $scope.district = "";
                        $scope.locality = "";

                        received.forEach(function (entry) {
                            $scope.countries.push(entry);
                        });
                    });
                };

                $scope.getRegions = function () {

                    var received;
                    addressFactory.addressesByParentGuid.query({guid: $scope.country.guid}, function (data) {
                        received = data;

                        $scope.regions = [];
                        $scope.districts = [];
                        $scope.localities = [];
                        $scope.region = "";
                        $scope.district = "";
                        $scope.locality = "";

                        received.forEach(function (entry) {
                            $scope.regions.push(entry);
                        });
                    });
                };

                $scope.getDistricts = function () {

                    var received;
                    addressFactory.addressesByParentGuid.query({guid: $scope.region.guid}, function (data) {
                        received = data;

                        $scope.districts = [];
                        $scope.localities = [];
                        $scope.district = "";
                        $scope.locality = "";

                        received.forEach(function (entry) {
                            if(entry.type.id === 3) {
                                $scope.districts.push(entry);
                            }
                            if(entry.type.id === 4) {
                                $scope.localities.push(entry);
                            }
                        });
                    });
                };

                $scope.getLocalities = function () {

                    if($scope.district === "" || $scope.district === undefined || $scope.district === null) {
                        $scope.getDistricts();
                    } else {

                        var received;
                        addressFactory.addressesByParentGuid.query({guid: $scope.district.guid}, function (data) {
                            received = data;

                            $scope.localities = [];
                            $scope.locality = "";

                            received.forEach(function (entry) {
                                $scope.localities.push(entry);
                            });
                        });
                    }
                };



                $scope.getCountries();

            }]

        )

        .controller("DictionaryController",
            [ '$scope', 'DictionaryFactory', function($scope, dictionaryFactory) {

                $scope.dictionarySingleResult = "";
                $scope.dictionaryTableResult = null;
                $scope.showDictionarySingleResult = false;
                $scope.showDictionaryTableResult = false;

                $scope.getUnits = function () {
                    $scope.showDictionaryTableResult = false;
                    $scope.dictionaryTableResult = dictionaryFactory.units.query();
                    $scope.showDictionaryTableResult = true;
                };

            }]
        )


        .controller("EnterpriseController",
            [ '$scope', 'EnterpriseFactory', function ($scope, enterpriseFactory) {
                $scope.guid = "";
                $scope.enterprises = [];
                $scope.enterpriseTypes = [];
                $scope.selectedEnterprise = null;
                $scope.enterprise = null;
                $scope.businessEntities = [];
                $scope.businessEntityEnterprises = [];
                $scope.businessEntityTypes = [];
                $scope.selectedBusinessEntity = null;
                $scope.businessEntity = null;
                $scope.foundNothing = false;
                $scope.loading = false;
                $scope.pageNum = 1;

                $scope.chosenBE = new Array();
                $('#selector1').trigger("chosen:updated");

                $scope.byInn = 0;

                $scope.clearFields = function () {
                    $scope.foundNothing = false;
                    $scope.loading = false;
                    $scope.byInn = 0;
                };

                $scope.getBusinessEntities = function () {

                    $scope.businessEntities = [];
                    var received;

                    enterpriseFactory.businessEntities.query(function (data) {
                        received = data;

                        received.forEach(function (entry) {
                            $scope.businessEntities.push(entry);
                        });
                    });
                };

                $scope.getEnterprises = function () {

                    $scope.enterprises = [];
                    var received;

                    enterpriseFactory.enterprises.query(function (data) {
                        received = data;

                        received.forEach(function (entry) {
                            $scope.enterprises.push(entry);
                        });
                    });
                };

                $scope.getEnterprisesPage = function () {

                    $scope.enterprises = [];
                    var received;

                    enterpriseFactory.enterprisesPage.query(function (data) {
                        received = data;

                        received.forEach(function (entry) {
                            $scope.enterprises.push(entry);
                        });
                    });
                };

                $scope.getEnterpriseTypes = function() {

                    $scope.enterpriseTypes = [];
                    var received;

                    enterpriseFactory.enterpriseTypes.query(function (data) {
                        received = data;

                        received.forEach(function (entry) {
                            $scope.enterpriseTypes.push(entry);
                        });
                    });


                };

                $scope.getBusinessEntityTypes = function() {
                    $scope.businessEntityTypes = [];
                    var received;

                    enterpriseFactory.businessEntityTypes.query(function (data) {
                        received = data;

                        received.forEach(function (entry) {
                            $scope.businessEntityTypes.push(entry);
                        });
                    });


                };

                $scope.getEnterprise = function(guid) {
                    $scope.enterprise = null;
                    $scope.foundNothing = false;
                    $scope.loading = true;

                    enterpriseFactory.loadEnterprise.get({guid: guid}, function (data) {
                        if (data.guid !== undefined) {
                            $scope.enterprise = data;
                            $scope.loading = false;
                        } else {
                            $scope.foundNothing = true;
                        }
                        $scope.loading = false;
                    });
                };

                $scope.getBusinessEntity = function(guid) {
                    $scope.businessEntity = null;
                    $scope.foundNothing = false;
                    $scope.loading = true;

                    if($scope.byInn === "0") {
                        enterpriseFactory.loadBusinessEntity.get({guid: guid}, function (data) {
                            if (data.guid !== undefined) {
                                $scope.businessEntity = data;
                            } else {
                                $scope.foundNothing = true;
                            }
                            $scope.loading = false;
                        });
                    } else {
                        enterpriseFactory.loadBusinessEntityByInn.get({inn: guid}, function (data) {
                            if (data.guid !== undefined) {
                                $scope.businessEntity = data;
                            } else {
                                $scope.foundNothing = true;
                            }
                            $scope.loading = false;
                        });
                    }

                };

                $scope.saveBusinessEntity = function() {

                    if($scope.businessEntity.linked)
                        return;

                    var newBEnt = {
                        id: null,
                        guid: $scope.businessEntity.guid,
                        name: $scope.businessEntity.name,
                        type: $scope.businessEntity.type,
                        inn: $scope.businessEntity.inn,
                        kpp: $scope.businessEntity.kpp,
                        ogrn: $scope.businessEntity.ogrn,
                        country: $scope.businessEntity.country,
                        region: $scope.businessEntity.region,
                        district: $scope.businessEntity.district,
                        locality: $scope.businessEntity.locality,
                        addressView: $scope.businessEntity.addressView
                    };

                    var flag = enterpriseFactory.putBusinessEntity(JSON.stringify(newBEnt));

                    flag.then(function(result) {
                        if(result) {
                            $scope.businessEntity.linked = !$scope.businessEntity.linked;
                        }
                    });
                };

                $scope.getBusinessEntities = function () {

                    $scope.businessEntities = [];
                    var received;

                    enterpriseFactory.businessEntities.query(function (data) {
                        received = data;

                        received.forEach(function (entry) {
                            $scope.businessEntities.push(entry);
                        });
                    });
                };

                $scope.getBusinessEntityEnterprises = function (id) {

                    $scope.businessEntityEnterprises = [];
                    var received;

                    enterpriseFactory.businessEntityEnterprises.query({id: id}, function (data) {
                        received = data;

                        received.forEach(function (entry) {
                            $scope.businessEntityEnterprises.push(entry);
                        });
                    });
                };


                $scope.saveEnterprise = function() {

                    if($scope.enterprise.linked)
                        return;

                    var newEnt = {
                        id: null,
                        guid: $scope.enterprise.guid,
                        name: $scope.enterprise.name,
                        type: $scope.enterprise.type,
                        country: $scope.enterprise.country,
                        region: $scope.enterprise.region,
                        district: $scope.enterprise.district,
                        locality: $scope.enterprise.locality,
                        addressView: $scope.enterprise.addressView
                        // businessEntity: $scope.enterprise.businessEntity
                    };
                    if(enterpriseFactory.putEnterprise(JSON.stringify(newEnt))) {
                        //$scope.hideBESelect();
                        $scope.enterprise.linked = !$scope.enterprise.linked;
                    }

                };

                $scope.showToolTip = function (selector, boolVar) {
                    if(!boolVar) {
                        $(selector).addClass('toggled');
                    }
                };

                $scope.hideToolTip = function (selector) { $(selector).removeClass('toggled'); };

                $scope.showBESelect = function () {
                    clearArray($scope.chosenBE);
                    $('#selector1').trigger("chosen:updated");

                    $scope.businessEntityEnterprises.forEach(function (entry) {
                        $scope.chosenBE.push(entry);
                    });

                    $('#selector1').trigger("chosen:updated");

                    hide('beDetailsDlg');
                    show('beSelectDlg');

                };

                $scope.hideBESelect = function () {
                    hide('beSelectDlg');
                    show('beDetailsDlg');
                };

                $scope.showBEDetails = function (be) {
                    $scope.selectedBusinessEntity = be;
                    $scope.getBusinessEntityEnterprises(be.id);
                    show('beDetailsDlg');
                };

                $scope.hideEntSelect = function () { hide('entSelectDlg'); };

                $scope.getFullBusinessEntity = function (id) {
                    $scope.businessEntity = enterpriseFactory.businessEntityFull.get({id: id});
                };

                $scope.saveBusinessEntityEnterprises = function () {
                    var ids = new Array();

                    ids.push($scope.selectedBusinessEntity.id);

                    $scope.chosenBE.forEach(function(item) {
                       ids.push(item.id);
                    });

                    var flag = enterpriseFactory.putBusinessEntityEnterprises(ids);

                    flag.then(function(result) {
                        if(result) {
                            $scope.getBusinessEntityEnterprises($scope.selectedBusinessEntity.id);
                            $scope.hideBESelect();
                        }
                    })

                };

                $scope.getEnterpriseTypes();
                $scope.getEnterprises();
                $scope.getBusinessEntityTypes();
                $scope.getBusinessEntities();

                function clearArray(data) {

                    while(data.length > 0) {
                        data.pop();
                    }
                }

            }]
        )

        .controller("DocumentController",
            [ '$scope', 'DocumentFactory', function($scope, documentFactory) {

                $scope.enterprises = [];
                $scope.businessEntities = [];
                $scope.enterprise = "";
                $scope.businessEntity = "";
                $scope.documents = [];
                $scope.requests = [];
                $scope.infoMessage = "";

                $scope.getBusinessEntities = function () {

                    $scope.businessEntities = [];
                    var received;

                    documentFactory.businessEntities.query(function (data) {
                        received = data;

                        received.forEach(function (entry) {
                            $scope.businessEntities.push(entry);
                        });
                    });
                };

                $scope.getBusinessEntityEnterprises = function () {

                    $scope.businessEntityEnterprises = [];
                    var received;

                    documentFactory.businessEntityEnterprises.query({id: $scope.businessEntity.id}, function (data) {
                        received = data;

                        received.forEach(function (entry) {
                            $scope.businessEntityEnterprises.push(entry);
                        });
                    });
                };

                $scope.sendRequest = function () {
                    var docs = documentFactory.documentsRequest($scope.businessEntity.id, $scope.enterprise.id);

                    docs.then(function(result) {
                        $scope.getRequests();
                    });
                };

                $scope.showDocuments = function(request) {

                    var data = $scope.getRequestDocuments(request);

                    console.log(data);

                };

                $scope.getReport = function() {
                    var key = documentFactory.getReport();

                    key.then(function(result){
                        console.log(result.result);
                    });
                };

                $scope.getRequestDocuments = function(request) {
                    var key = documentFactory.fetchDocuments(request.id);

                    key.then(function(result){
                        console.log(result.result);
                        console.log(result.info);
                        if(result.result.valueType === "FALSE"){
                            $scope.infoMessage = result.info.string;
                            show('ackDlg');
                        }
                    });
                };

                $scope.getRequests = function() {
                    $scope.requests = [];
                    var received;

                    documentFactory.requests.query( function (data) {
                        received = data;

                        received.forEach(function (entry) {
                            $scope.requests.push(entry);
                        });
                    });
                };

                $scope.getBusinessEntities();
                $scope.getRequests();

            }]
        )



}());