function initialize() {

    $.fn.selection = function(){
        if(this.hasClass('selector')) {
            return this.children('li.selected').index();
        }
    };

    $('.selector li:first-child').toggleClass('selected');

    $('.selector li').click(function(e) {
        var target = $(e.target);
        $('.selector li.selected').toggleClass('selected');
        target.toggleClass('selected');
    });

    $('[data-toggle="tooltip"]').tooltip();

    // $('.select2').select2({
    //     language:"ru",
    //     placeholder: 'Выберите значение'
    // });

    initPickList();


}

function initPickList(){
    $('.add').click(function(){
        $('.all').prop("checked",false);
        var items = $("#list1 input:checked:not('.all')");
        var n = items.length;
        if (n > 0) {
            items.each(function(idx,item){
                var choice = $(item);
                choice.prop("checked",false);
                choice.parent().appendTo("#list2");
            });
        }
        // else {
        //     alert("Выберите элементы из левого списка");
        // }
    });

    $('.remove').click(function(){
        $('.all').prop("checked",false);
        var items = $("#list2 input:checked:not('.all')");
        items.each(function(idx,item){
            var choice = $(item);
            choice.prop("checked",false);
            choice.parent().appendTo("#list1");
        });
    });

    /* toggle all checkboxes in group */
    $('.all').click(function(e){
        e.stopPropagation();
        var $this = $(this);
        if($this.is(":checked")) {
            $this.parents('.list-group').find("[type=checkbox]").prop("checked",true);
        }
        else {
            $this.parents('.list-group').find("[type=checkbox]").prop("checked",false);
            $this.prop("checked",false);
        }
    });

    $('[type=checkbox]').click(function(e){
        e.stopPropagation();
    });

    /* toggle checkbox when list group item is clicked */
    $('.list-group a').click(function(e){

        e.stopPropagation();

        var $this = $(this).find("[type=checkbox]");
        if($this.is(":checked")) {
            $this.prop("checked",false);
        }
        else {
            $this.prop("checked",true);
        }

        if ($this.hasClass("all")) {
            $this.trigger('click');
        }
    });
}

function show(modalName){
    $('#' + modalName).modal('show');
}

function hide(modalName){
    $('#' + modalName).modal('hide');
}
