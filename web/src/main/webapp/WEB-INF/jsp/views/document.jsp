<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" language="java" %>

<script>
    $(document).ready(initialize())
</script>

<div class="container-fluid" ng-controller="DocumentController">
    <h1>Документы и отчеты</h1>

    <div class="panel panel-default">
        <div class="panel-body">

            <div class="row toolpanel">
                <div class="col-md-5">
                    <select chosen style="width: 100%"
                            ng-options="item.name for item in businessEntities track by item.id"
                            ng-model="businessEntity" ng-change="getBusinessEntityEnterprises()">
                        <option value=""></option>
                    </select>
                </div>

                <div class="col-md-5">
                    <select chosen style="width: 100%"
                            ng-options="item.name for item in businessEntityEnterprises track by item.id"
                            ng-model="enterprise">
                        <option value=""></option>
                    </select>
                </div>

                <div class="col-md-2">
                    <a class="btn btn-default" id="getDocBtn" ng-click="sendRequest()">
                        <span class="fa fa-file"/> Получить документы
                    </a>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table bordered">
                        <tr>
                            <th>#</th>
                            <th>ХС</th>
                            <th>Предприятие</th>
                            <th>время запроса</th>
                            <th>документы</th>
                        </tr>

                        <tr ng-repeat="r in requests">
                            <td>{{r.id}}</td>
                            <td>{{r.businessEntity.name}}</td>
                            <td>{{r.enterprise.name}}</td>
                            <td>{{r.timeStr}}</td>
                            <td style="text-align: center">
                                <a class="btn btn-sm btn-default" ng-click="showDocuments(r)">
                                    <span class="fa fa-info"/>
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" role="dialog" id="ackDlg" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    Уведомление
                </div>

                <div class="modal-body">
                    {{infoMessage}}
                </div>

                <div class="modal-footer">

                    <a class="btn btn-danger" onclick="hide('ackDlg');">
                        <span class="fa fa-check"></span> &nbsp; OK
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-md-2">
                <a class="btn btn-default" id="getReport" ng-click="getReport()">
                    <span class="fa fa-file"/> Получить отчёт
                </a>
            </div>

            <div id="reportData"></div>
        </div>
    </div>


    <div class="modal fade" role="dialog" id="docDlg" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <!--h3>{{selectedBusinessEntity.name}}</h3-->
                    Заголовок окна
                </div>

                <div class="modal-body">

                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <table class="table bordered pressable">
                                    <tr>
                                        <th>#</th>
                                        <th>название</th>
                                        <th></th>
                                    </tr>

                                    <tr ng-repeat="item in documents track by item.id">
                                        <td>{{item.id}}</td>
                                        <td>{{item.name}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">

                    <a class="btn btn-danger" onclick="hide('docDlg');">
                        <span class="fa fa-times"></span> &nbsp; Закрыть
                    </a>
                </div>
            </div>
        </div>
    </div> <!-- //beDetailsDlg -->

</div>
