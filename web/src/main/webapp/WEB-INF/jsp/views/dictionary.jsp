<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" language="java" %>

<script>
    $(document).ready(initialize())
</script>

<div class="container-fluid" ng-controller="DictionaryController">
    <h1>Справочные данные</h1>

    <div class="panel panel-default">
        <div class="panel-heading">Единицы измерений</div>
        <div class="row">
            <div class="col-md-1">
                <div class="btn-group">
                    <a class="btn btn-default" ng-click="getUnits()"><span class="fa fa-bank"/> getAll</a>
                </div>
            </div>
        </div>

        <div class="spacediv"/>
        <table class="table bordered striped" ng-if="showDictionaryTableResult">
            <tr>
                <th>#</th>
                <th>название</th>
                <th>полное название</th>
                <th>основа</th>
                <th>коэффициент</th>
            </tr>
            <tr ng-repeat="item in dictionaryTableResult">
                <td>{{item.id}}</td>
                <td>{{item.name}}</td>
                <td>{{item.fullName}}</td>
                <td>{{item.commonUnit.name}}</td>
                <td>{{item.factor}}</td>
            </tr>
        </table>
    </div>


</div>
