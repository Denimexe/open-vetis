<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" language="java" %>

<script>
    $(document).ready(initialize());
</script>

<div class="container-fluid" ng-controller="EnterpriseController">
    <h1>ХС и подразделения</h1>

    <div class="row">
        <!--div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="input-group">
                        <input type="number" class="form-control" placeholder="#id" ng-model="id"/>
                        <span class="input-group-btn">
                            <a class="btn btn-default btn-secondary" ng-click="getBusinessEntityEnterprises(id)">
                                <span class="fa fa-address-card"/> getBusinessEntityEnterprises
                            </a>
                        </span>
                    </div>
                </div>
                <div class="panel-body">

                    <span>{{businessEntity.name}}</span>

                    <table class="table bordered">
                        <tr>
                            <th>#</th>
                            <th>название</th>
                        </tr>

                        <tr ng-repeat="item in businessEntityEnterprises">
                            <td>{{item.id}}</td>
                            <td>{{item.name}}</td>
                        </tr>
                    </table>
                </div>
            </div>

        </div-->

        <div class="col-md-12">
            <div class="btn-group"><!--18148d7c-ae46-44b6-9407-fd637e763f84 -->
                <!--ng-click="getEnterprise('0', '9b7520f1-3b2f-4e92-8ae7-1fc00267f14c')"-->
                <a class="btn btn-default" onclick="show('findEnterprise')"><span class="fa fa-zoom"/> Поиск предприятий</a>
                <a class="btn btn-default" onclick="show('findBusinessEntity')"><span class="fa fa-zoom"/> Поиск ХС</a>
            </div>
        </div>
    </div>

    <div class="spacediv"></div>

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Типы ХС</div>
                <div class="panel-body">
                    <table class="table bordered">
                        <tr>
                            <th>#</th>
                            <th>название</th>
                        </tr>

                        <tr ng-repeat="item in businessEntityTypes track by item.id">
                            <td>{{item.id}}</td>
                            <td>{{item.name}}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Типы предприятий</div>
                <div class="panel-body">
                    <table class="table bordered">
                        <tr>
                            <th>название</th>
                            <th>guid</th>
                        </tr>

                        <tr ng-repeat="item in enterpriseTypes track by item.id">
                            <td>{{item.name}}</td>
                            <td>{{item.guid}}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Хозяйствующие субъекты</div>
                <div class="panel-body">
                    <table class="table bordered pressable">
                        <tr>
                            <th>название</th>
                            <th>guid</th>
                            <th></th>
                        </tr>

                        <tr ng-repeat="item in businessEntities track by item.id">
                            <td>{{item.name}}</td>
                            <td>{{item.guid}}</td>
                            <td>
                                <a class="btn btn-xs btn-default" ng-click="showBEDetails(item);">
                                    <span class="fa fa-list"/>
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="findBusinessEntity" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                    <div class="modal-header">Поиск</div>

                    <div class="modal-body">

                        <div class="form-horizontal">

                            <div class="form-group">
                                <ul class="selector">
                                    <li>
                                        <input type="radio" name="toggle" id="guid" value="0" ng-model="byInn" checked>
                                        <label for="guid">по GUID</label>
                                    </li>
                                    <li>
                                        <input type="radio" name="toggle" id="inn" value="1" ng-model="byInn">
                                        <label for="inn">по ИНН</label>
                                    </li>
                                </ul>
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="GUID/ИНН" ng-model="guid"/>
                            </div>

                            <div class="form-group">
                                <a class="btn btn-success" ng-click="getBusinessEntity(guid);">
                                    <span class="fa fa-search"></span> &nbsp; найти
                                </a>
                            </div>

                            <div class="form-group" ng-if="businessEntity != null">
                                <div class="card businessEntity">
                                    <div class="type">
                                        &nbsp;{{businessEntity.type.name}}
                                    </div>

                                    <div class="tooltip">
                                        <div class="db_tooltip">Сохранить в базу</div>
                                        <div class="triangle"></div>
                                    </div>

                                    <div class="db" ng-click="saveBusinessEntity(); hideToolTip('.tooltip');"
                                         ng-mouseenter="showToolTip('.tooltip', businessEntity.linked)"
                                         ng-mouseleave="hideToolTip('.tooltip')">
                                        <a class="switch fa fa-save"
                                           ng-class="{'on': businessEntity.linked, 'off': !businessEntity.linked}">
                                        </a>
                                    </div>

                                    <div class="guid">{{businessEntity.guid}}</div>
                                    <div class="name">{{businessEntity.name}}</div>
                                    <table class="bank_plus_address">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="bank_data">
                                                        <table>
                                                            <tbody>
                                                            <tr>
                                                                <td>ИНН</td>
                                                                <td>{{businessEntity.inn}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>КПП</td>
                                                                <td>{{businessEntity.kpp}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>ОГРН</td>
                                                                <td>{{businessEntity.ogrn}}</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>

                                                <td>
                                                    <div class="address">{{businessEntity.addressView}}</div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="form-group" ng-if="loading">
                                <div class="alert alert-loading" role="alert">
                                    <img src="images/loading.gif"/>
                                </div>
                            </div>

                            <div class="form-group" ng-if="foundNothing">
                                <div class="alert alert-danger" role="alert">
                                    Ничего не найдено
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <a class="btn btn-danger" onclick="hide('findBusinessEntity')" ng-click="clearFields(); getBusinessEntities()">
                            <span class="fa fa-times"></span> &nbsp; закрыть
                        </a>
                    </div>
            </div>
        </div>
    </div><!-- /.modal -->

    <div class="modal fade" tabindex="-1" role="dialog" id="findEnterprise" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">Поиск</div>

                <div class="modal-body">

                    <div class="form-horizontal">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="введите GUID" ng-model="guid"/>
                        </div>

                        <div class="form-group">
                            <a class="btn btn-success" ng-click="getEnterprise(guid);">
                                <span class="fa fa-search"></span> &nbsp; найти
                            </a>
                        </div>

                        <div class="form-group" ng-if="enterprise != null">
                            <div class="card enterprise">
                                <div class="type">
                                    &nbsp;{{enterprise.type.name}}
                                </div>

                                <div class="tooltip">
                                    <div class="db_tooltip">Сохранить в базу</div>
                                    <div class="triangle"></div>
                                </div>

                                <div class="db" ng-click="saveEnterprise(); hideToolTip('.tooltip');"
                                     ng-mouseenter="showToolTip('.tooltip', enterprise.linked)"
                                     ng-mouseleave="hideToolTip('.tooltip')">
                                    <a class="switch fa fa-save"
                                       ng-class="{'on': enterprise.linked, 'off': !enterprise.linked}">
                                    </a>
                                </div>

                                <div class="guid">{{enterprise.guid}}</div>
                                <div class="name">{{enterprise.name}}</div>
                                <div class="address">{{enterprise.addressView}}</div>
                            </div>
                        </div>

                        <div class="form-group" ng-if="loading">
                            <div class="alert alert-loading" role="alert">
                                <img src="images/loading.gif"/>
                            </div>
                        </div>

                        <div class="form-group" ng-if="foundNothing">
                            <div class="alert alert-danger" role="alert">
                                Ничего не найдено
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <a class="btn btn-danger" onclick="hide('findEnterprise')" ng-click="clearFields(); getEnterprises()">
                        <span class="fa fa-times"></span> &nbsp; закрыть
                    </a>
                </div>
            </div>
        </div>
    </div><!-- /.modal -->


    <div class="modal fade" role="dialog" id="beDetailsDlg" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>{{selectedBusinessEntity.name}}</h3>
                </div>

                <div class="modal-body">

                    <div class="panel-body">
                        <div class="card businessEntity">
                            <div class="type">
                                &nbsp;{{selectedBusinessEntity.type.name}}
                            </div>

                            <div class="guid">{{selectedBusinessEntity.guid}}</div>
                            <table class="bank_plus_address">
                                <tbody>
                                <tr>
                                    <td>
                                        <div class="bank_data">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td>ИНН</td>
                                                    <td>{{selectedBusinessEntity.inn}}</td>
                                                </tr>
                                                <tr>
                                                    <td>КПП</td>
                                                    <td>{{selectedBusinessEntity.kpp}}</td>
                                                </tr>
                                                <tr>
                                                    <td>ОГРН</td>
                                                    <td>{{selectedBusinessEntity.ogrn}}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>

                                    <td>
                                        <div class="address">{{selectedBusinessEntity.addressView}}</div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="btn-group">
                                <a class="btn btn-default"
                                   ng-click="showBESelect();">
                                    <span class="fa fa-link"/>
                                    Редактировать связи
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="spacediv"></div>

                    <div class="row">
                        <div class="col-md-12">
                            <table class="table bordered pressable">
                                <tr>
                                    <th>#</th>
                                    <th>название</th>
                                    <th></th>
                                </tr>

                                <tr ng-repeat="item in businessEntityEnterprises track by item.id">
                                    <td>{{item.id}}</td>
                                    <td>{{item.name}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">

                    <a class="btn btn-danger" onclick="hide('beDetailsDlg');">
                        <span class="fa fa-times"></span> &nbsp; Закрыть
                    </a>
                </div>
            </div>
        </div>
    </div> <!-- //beDetailsDlg -->


    <div class="modal fade" role="dialog" id="beSelectDlg" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Укажите связанные с ХС предприятия</h3>
                </div>

                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <select chosen multiple
                                    ng-options="option as option.name for option in enterprises track by option.id"
                                    ng-model="chosenBE">
                            </select>
                        </div>

                    </div>
                </div>

                <div class="modal-footer">

                    <a class="btn btn-default btn-secondary" ng-click="saveBusinessEntityEnterprises()">
                        <span class="fa fa-check"/> Сохранить
                    </a>

                    <a class="btn btn-danger" ng-click="hideBESelect()">
                        <span class="fa fa-times"></span> &nbsp; Отмена
                    </a>
                </div>
            </div>
        </div>
    </div> <!-- //beSelectDlg -->



</div>
