<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" language="java" %>

<script>
    $(document).ready(initialize())
</script>

<div class="container-fluid">
    <h1>Главная.web</h1>

    <div class="panel-default">
        <div class="row">
            <div class="col-md-1">
                <div class="btn-group">
                    <a class="btn btn-default" ng-click="sendMsg()" disabled="true"><span class="fa fa-plug"/> send message to Core</a>
                </div>
            </div>
        </div>

        <select chosen
                placeholder-text-multiple="'Выбирай'"
                no-results-text="'Ничего не найденено по запросу'"
                style="width:200px;"
                class="chosenone">
            <option value="AL">Alabama</option>
            <option value="WY">Wyoming</option>
        </select>

        <select chosen
                multiple
                placeholder-text-multiple="'Выбирай'"
                no-results-text="'Ничего не найденено по запросу'"
                style="width:200px;"
                class="chosenone">
            <option value="AL">Alabama</option>
            <option value="WY">Wyoming</option>
        </select>

        <div class="spacediv"></div>

    </div>

</div>
