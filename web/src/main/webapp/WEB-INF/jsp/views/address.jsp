<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>

<script>
    $(document).ready(initialize())
</script>

<div class="container-fluid" ng-controller="AddressController">
    <h1>Адреса</h1>

    <div class="panel panel-default">
        <div class="panel-heading">Типы адресов</div>
        <div class="row">
            <div class="col-md-3">
                <div class="input-group">
                    <input type="number" class="form-control" placeholder="#id" ng-model="id"/>
                    <span class="input-group-btn">
                    <a class="btn btn-default btn-secondary" ng-click="getType()">
                        <span class="fa fa-address-card"/> getTypeById
                    </a>
                </span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="#name" ng-model="name"/>
                    <span class="input-group-btn">
                    <a class="btn btn-default btn-secondary" ng-click="getTypeByName()">
                        <span class="fa fa-address-card"/> getTypeByName
                    </a>
                </span>
                </div>
            </div>

            <div class="col-md-1">
                <div class="btn-group">
                    <a class="btn btn-default" ng-click="getTypes()"><span class="fa fa-address-book"/> getAll</a>
                </div>
            </div>
        </div>

        <div class="spacediv"/>

        <label class="control-label" ng-if="showTypeSingleResult">{{typeSingleResult}}</label>
        <table class="table bordered" ng-if="showTypeTableResult">
            <tr>
                <th>#</th>
                <th>название</th>
            </tr>
            <tr ng-repeat="item in typeTableResult">
                <td>{{item.id}}</td>
                <td>{{item.name}}</td>
            </tr>
        </table>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Селекторы</div>

        <div class="panel-body">

            <select chosen style="width: 23%"
                    ng-options="option.name for option in countries track by option.id"
                    ng-model="country" ng-change="getRegions()">
                <option value=""></option>
            </select>

            <select chosen singl style="width: 23%"
                    ng-options="option.name for option in regions track by option.id"
                    ng-model="region" ng-change="getDistricts();">
                <option value=""></option>
            </select>

            <select chosen style="width: 23%"
                    allow-single-deselect="true"
                    ng-options="option.name for option in districts track by option.id"
                    ng-model="district" ng-change="getLocalities()">
                    <option value=""></option>
            </select>

            <select chosen style="width: 23%"
                    allow-single-deselect="true"
                    ng-options="option.name for option in localities track by option.id"
                    ng-model="locality">
                <option value=""></option>
            </select>

        </div>
    </div>

    <!--div class="panel panel-default">
        <div class="panel-heading">Автозаполнение</div>
        <div class="panel-body">

            <div class="form-horizontal">
                <div class="form-group">
                    <input class="form-control" placeholder="Страна"
                        ng-model="country" uib-typeahead="c.name for c in countries">
            </div>

                <div class="form-group">
                    <input class="form-control" placeholder="Регион"/>
                </div>

                <div class="form-group">
                    <input class="form-control" placeholder="Район"/>
                </div>

                <div class="form-group">
                    <input class="form-control" placeholder="Населённый пункт"/>
                </div>
            </div>
        </div>
    </div-->

    <div class="panel panel-default">
        <div class="panel-heading">Адреса</div>
        <div class="row">
            <div class="col-md-3">
                <div class="input-group">
                    <input type="number" class="form-control" placeholder="#id" ng-model="id"/>
                    <span class="input-group-btn">
                    <a class="btn btn-default btn-secondary" ng-click="getAddress()">
                        <span class="fa fa-address-card"/> getAddressById
                    </a>
                </span>
                </div>
            </div>

            <div class="col-md-3">
                <div class="btn-group">
                    <!--a class="btn btn-default" ng-click="getAddresses()"><span class="fa fa-address-book"/> getAll</a-->
                    <a class="btn btn-default" ng-click="getAddressesByTypeId(typeSingleResult.id)"><span class="fa fa-address-book"/> getByType</a>
                </div>
            </div>
        </div>

        <div class="spacediv"/>

        <label class="control-label" ng-if="showSingleResult">{{singleResult}}</label>
        <table class="table bordered" ng-if="showTableResult">
            <tr>
                <th>#</th>
                <th>guid</th>
                <th>название</th>
                <th>тип</th>

            </tr>
            <tr ng-repeat="item in tableResult">
                <td>{{item.id}}</td>
                <td>{{item.guid}}</td>
                <td>{{item.name}}</td>
                <td>{{item.type.name}}</td>
            </tr>
        </table>
    </div>


</div>
