<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="en" id="ng-app" ng-app="vetis">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <jsp:include page="includes/header.jsp"  />

    <link rel="icon"
          type="image/png"
          href="<c:url value="/images/favicon.ico"/>">

    <title>Ветис.API</title>

</head>
<%--<body>--%>

<script>
    $(document).ready(initialize())
</script>

<body data-ng-controller="MainController">

<div class="header">
    <!--div class="userinfo">
        <span id="username">
            Username
        </span>
    </div-->
    <div class="headermenu">
        <!--img src="< c:url value="/images/word_logo_small.png"/>"/-->
            <h1>ВЕТИС.web</h1>
    </div>

    <div class="notifier">
        <img src="<c:url value="/images/notifier.png"/>">
            <span id="notification-badge" class="badge" ng-if="unviewedAmount">{{unviewedAmount}}</span>
        </img>
    </div>
    <div id="notification-area">
        <div id="notification-area-header"></div>
        <div id="notifier-wrapper">
            <div class="notification" ng-repeat="(id, item) in notifications | orderBy:nt.date:reverse track by id" ng-class="{'new' : !item.viewed}">
                <div class="message">{{item.message}}</div>
                <div class="time">{{formatDate(item.date)}}</div>
            </div>
        </div>
    </div>
</div>

<div class="wrapper">

    <div id="notification-showarea">
        <%--<div id="notification-showarea-wrapper">--%>
            <div class="notification" ng-repeat="nt in notifications | orderBy:nt.date:reverse  track by nt.id" ng-if="!nt.viewed">
                <div class="close-btn" ng-click="setViewed(nt)"><span class="fa fa-close"></span></div>
                <div class="message">{{nt.message}}</div>
                <div class="time">{{formatDate(nt.date)}}</div>
            </div>
        <%--</div>--%>
    </div>

    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <!--li class="sidebar-brand"/-->

            <li class="sidebar-item">
                <a href="#" ui-sref="main" ui-sref-active="active">
                    <img class="sidebar-icon" alt="dic" src="<c:url value="/images/main.png" />"/>
                    Главная
                </a>
            </li>
            <li class="sidebar-item">
                <a href="#" ui-sref="address" ui-sref-active="active">
                    <img class="sidebar-icon" alt="rmv" src="<c:url value="/images/address.png" />"/>
                    Адреса
                </a>
            </li>
            <li class="sidebar-item">
                <a href="#" ui-sref="dictionary" ui-sref-active="active">
                    <img class="sidebar-icon" alt="rmv" src="<c:url value="/images/journal.png" />"/>
                    Справочные данные
                </a>
            </li>
            <li class="sidebar-item">
                <a href="#" ui-sref="enterprise" ui-sref-active="active">
                    <img class="sidebar-icon" alt="rmv" src="<c:url value="/images/enterprise.png" />"/>
                    ХС и подразделения
                </a>
            </li>
            <li class="sidebar-item">
                <a href="#" ui-sref="document" ui-sref-active="active">
                    <img class="sidebar-icon" alt="rmv" src="<c:url value="/images/documents.png" />"/>
                    Документы и отчёты
                </a>
            </li>
            <!--li class="sidebar-item" data-ng-show="{{ permission.journal }}">
                <a href="#" ui-sref="journal" ui-sref-active="active">
                    <img class="sidebar-icon" alt="rmv" src="< c:url value="/images/journal.png" />"/>
                    Журнал
                </a>
            </li>
            <li class="sidebar-item">
                <a href="#" ui-sref="reports" ui-sref-active="active">
                    <img class="sidebar-icon" alt="rmv" src="< c:url value="/images/reports.png" />"/>
                    Отчеты и акты
                </a>
            </li-->
        </ul>

    </div>
    <!-- /#sidebar-wrapper -->

    <div class="workarea">
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <ui-view></ui-view>
        </div>
        <!-- /#page-content-wrapper -->
    </div>

</div>

<div class="footer">
    Березовский Денис, 2017
</div>

</body>

</html>