function initialize() {

    $.fn.selection = function(){
        if(this.hasClass('selector')) {
            //this.children('li.selected')
            return this.children('li.selected').index();
        }
    };

    $('.selector li:first-child').toggleClass('selected');

    $('.selector li').click(function(e) {
        var target = $(e.target);
        $('.selector li.selected').toggleClass('selected');
        target.toggleClass('selected');
    })
}

function show(modalName){
    $('#' + modalName).modal('show');
}

function hide(modalName){
    $('#' + modalName).modal('hide');
}
