$(function() {

    // --------------------- <showing selected tab element ----------------------
    //getting page from url
    //var curtab = location.href.replace(/\?(\S+$)?/, "$1");
    // console.log(curtab);
    // if(curpage === '') {
    //     curpage = 'acts';
    // }
    //$('a[ui-sref="' + curpage + '"]').toggleClass('selected');

    // -------------------- highlighting current menu element/> ----------------------

    $('#tabnav a').click(function(e) {
        var curtabid = $('ul.nav li.active').attr("id");
        $('ul.nav li.active').toggleClass("active");//disable current
        var target = $(e.target);
        $(e.target).parent().toggleClass("active");//enable new
        var newtabid = $(e.target).parent().attr('id');
        if(newtabid !== curtabid) {
            $('div.tab-panel').toggleClass("hidden");
        }
    });

});