(function () {
    'use strict';

    function treatAsUTC(date) {
        var result = new Date(date);
        result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
        return result;
    }

    angular.module("vetis.main", [
        'ui.router',
        "vetis.rest",
        'ui.bootstrap'
    ]).config(['$stateProvider', function ($stateProvider) {
        $stateProvider.state('main', {
            url: '/main',
            templateUrl: 'views/main.html',
            controller: 'MainController'
        });
    }])
    // Контроллер для управления стартовой страницей и меню навигации
        .controller("MainController",
            [ '$scope', 'NotificationFactory', function($scope, notificationFactory) {

                /*
                $scope.fetchResult = "failure";
                $scope.showTypeSingleResult = false;

                $scope.notifications = [];//new Map();
                $scope.unviewedAmount = 0;

                $scope.fetchNotifications = function () {
                    var received;

                    notificationFactory.notifications.query(function (data) {
                        received = data;

                        received.forEach(function (entry) {
                            //$scope.notifications.set(entry.id, entry);
                            // if(!entry.viewed) {
                            //     newNotificationsAmount++;
                            // }

                            var i = 0;
                            var consist = false;

                            for(i; i < $scope.notifications.length; i++) {
                                if ($scope.notifications[i].id === entry.id) {
                                    consist = true;
                                    break;
                                }
                            }
                            if(!consist) {
                                $scope.notifications.push(entry);
                                if(!entry.viewed) {
                                    $scope.unviewedAmount++;
                                }

                            }
                        });

                        // console.log($scope.notifications);
                    });
                };

                $scope.formatDate = function(date){

                    var d = new Date(date);
                    var curdate = new Date();
                    var millisecondsPerDay = 24 * 60 * 60 * 1000;
                    var deltaDate = (treatAsUTC(curdate) - treatAsUTC(d)) / millisecondsPerDay;

                    var timestr = "";

                    if(deltaDate > 0 && deltaDate <= 1) {
                        timestr = "сегодня, ";
                    } else if(deltaDate > 1 && deltaDate <= 2) {
                        timestr = "вчера, ";
                    } else {
                        timestr = (d.getDate() < 9 ? "0" : "") + d.getDate() + "." +
                            (d.getMonth() < 9 ? "0" : "") + (d.getMonth() + 1) + "." +
                            (d.getYear() < 110 ? "0" : "") + (d.getYear() - 100) + " ";
                    }
                    timestr += (d.getHours() < 10 ? "0" : "") + d.getHours() + ":" +
                        (d.getMinutes() < 10 ? "0" : "") + d.getMinutes();

                    return timestr;
                };

                $scope.setViewed = function(item) {
                    item.viewed = true;
                    notificationFactory.Viewed.get({id: item.id});

                };

                $scope.expired = function(date) {
                    var curdate = new Date();
                    var millisecondsPerMinute = 60 * 1000;
                    var deltaMinute = (treatAsUTC(curdate) - treatAsUTC(date)) / millisecondsPerMinute;
                    return deltaMinute > 0.2;
                };

                $scope.fetchNotifications();
                */

            }]

        )

}());