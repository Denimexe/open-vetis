(function () {
    'use strict';

    angular.module("vetis.rest", [
        'ngResource'
    ])

    .factory('NotificationFactory', ['$resource', function ($resource) {
        var coreUrl = "http://localhost:8080/vetis_core/";
        return {
            notifications: "",//$resource(coreUrl + 'rest/notifications'),
            viewed: ""//$resource(coreUrl + 'rest/notifications/viewed/:id', {id: '@id'})
        }
    }]);



}());