$(function() {

    // --------------------- <highlighting current menu element ----------------------
    //getting page from url
    var toremove = window.location.protocol + "//" + window.location.host + "/";
    //removing htttp://site:port part
    var curpage = location.href.replace(toremove, '');
    curpage = curpage.replace(/^[^\/]+\/#\/([^\/]*)/, "$1");
    curpage = curpage.replace(/\?\S+/, "");
    if(curpage === '') {
        curpage = 'main';
    }
    $('a[ui-sref="' + curpage + '"]').toggleClass('selected');

    // -------------------- highlighting current menu element/> ----------------------

    $("#sidebar-wrapper").hover(function(e){
        $("#sidebar-wrapper").toggleClass("toggled");
    });

    $('ul.sidebar-nav a').click(function(e) {
        $('ul.sidebar-nav a.selected').toggleClass("selected");
        var target = $(e.target);
        if(target.is("a")) {
            $(e.target).toggleClass("selected");
        } else if(target.is("img")) {
            $(e.target).parent().toggleClass("selected");
        }
    });
});