<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="en" id="ng-app" ng-app="vetis">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <jsp:include page="includes/header.jsp"  />

    <link rel="icon"
          type="image/png"
          href="<c:url value="/images/favicon.ico"/>">

    <title>Ветис Core</title>

</head>
<body ng-controller="CoreController">

<div class="header">
    <div class="headermenu">
        <h1>ВЕТИС.core</h1>
    </div>
</div>

<div class="wrapper">

    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="sidebar-item">
                <a href="#" ui-sref="main" ui-sref-active="active">
                    <div class="menu-icon"/>
                    <img class="sidebar-icon" alt="dic" src="<c:url value="/images/main.png"/>"/>
                    Главная
                </a>
            </li>
        </ul>

    </div>
    <!-- /#sidebar-wrapper -->

    <div class="workarea">
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <ui-view></ui-view>
        </div>
        <!-- /#page-content-wrapper -->
    </div>
</div>

<div class="footer">
    Березовский Денис, 2017
</div>

</body>

</html>