<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>

<spring:url value="/webjars/jquery/2.1.4/jquery.js" var="jquery_js"/>
<script src="${jquery_js}"></script>
<spring:url value="/webjars/bootstrap/3.1.0/js/bootstrap.min.js" var="bootstrap_js"/>
<script src="${bootstrap_js}"></script>

<spring:url value="/webjars/bootstrap/3.1.0/css/bootstrap.min.css" var="bootstrap"/>
<link rel="stylesheet" href="${bootstrap}">

<spring:url value="/webjars/angularjs/1.2.28/angular.js" var="angularjs"/>
<script src="${angularjs}"></script>
<spring:url value="/webjars/angularjs/1.2.28/angular-resource.js" var="angularResource"/>
<script src="${angularResource}"></script>
<spring:url value="/webjars/angular-ui-router/0.2.18/angular-ui-router.min.js" var="uirouter"/>
<script src="${uirouter}"></script>


<spring:url value="/app/rest.js" var="rest"/>
<script src="${rest}"></script>
<spring:url value="/app/app.js" var="app"/>
<script src="${app}"></script>
<spring:url value="/app/main.js" var="main"/>
<script src="${main}"></script>

<spring:url value="/css/style.css" var="style_css" />
<link rel="stylesheet" href="${style_css}">
<spring:url value="/js/sidebar.js" var="sidebar_js"/>
<script src="${sidebar_js}"></script>

<!---spring:url value="/js/app/client/clientJournal.js" var="clientJournal"/>
<script src="$ {clientJournal}"></script-->

<!-- Custom CSS -->

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->