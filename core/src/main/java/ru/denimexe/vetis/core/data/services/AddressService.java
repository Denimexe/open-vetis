package ru.denimexe.vetis.core.data.services;

import ru.denimexe.vetis.core.data.entities.Address;
import ru.denimexe.vetis.core.data.entities.AddressType;

import java.util.List;

/**
 * Created by dirde on 15.11.2016.
 */
public interface AddressService {

    void save(Address address);

    Address findOne(Integer id);

    List<Address> findAll();

    Address findByGuid(String guid);

    List<Address> findByTypeId(Integer id);

    List<Address> findByNameContaining(String name);

    List<Address> findByParentGuid(String parent);

    List<Address> findByType(AddressType type);

    List<Address> findByTypeAndParentGuid(AddressType type, String parentGuid);

    void saveType(AddressType type);

    AddressType findType(Integer id);

    AddressType findTypeByName(String name);

    List<AddressType> findTypes();


}
