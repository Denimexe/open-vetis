package ru.denimexe.vetis.core.data.services;

import ru.denimexe.vetis.core.data.entities.Batch;

import java.util.List;

/**
 * Created by dirde on 15.11.2016.
 */
public interface BatchService {

    void save(Batch batch);

    Batch findOne(Integer id);

    List<Batch> findAll();
}
