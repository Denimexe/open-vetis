package ru.denimexe.vetis.core.data.services;

import ru.denimexe.vetis.core.data.entities.Product;
import ru.denimexe.vetis.core.data.entities.ProductType;

import java.util.List;

/**
 * Created by dirde on 15.11.2016.
 */
public interface ProductService {

    void save(Product product);

    Product findOne(Integer id);

    Product findByGuid(String guid);

    void saveType(ProductType type);

    ProductType findType(Integer id);

    ProductType findTypeByName(String id);

    List<Product> findAll();
}
