package ru.denimexe.vetis.core.data.services;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.denimexe.vetis.core.data.entities.BusinessEntity;
import ru.denimexe.vetis.core.data.entities.Enterprise;
import ru.denimexe.vetis.core.data.entities.Request;
import ru.denimexe.vetis.core.data.repositories.RequestRepository;

import java.util.List;

/**
 * Created by denimexe on 12.10.17.
 */
@Service
@Log4j2
public class RequestServiceImpl implements RequestService {

    @Autowired
    private RequestRepository repository;


    @Override
    public void save(Request request) {
        try {
            repository.save(request);
        } catch ( Exception ex) {
            log.error(ex);
        }
    }

    @Override
    public Request findOne(Integer id) {
        try {
            return repository.findOne(id);
        } catch ( Exception ex) {
            log.error(ex);
            return null;
        }
    }

    @Override
    public List<Request> findAll() {
        try {
        return repository.findAll();
        } catch ( Exception ex) {
            log.error(ex);
            return null;
        }
    }

    @Override
    public Request findByEndpoint(BusinessEntity businessEntity, Enterprise enterprise) {
        try {
            return repository.findByBusinessEntityAndEnterprise(businessEntity, enterprise);
        } catch ( Exception ex) {
            log.error(ex);
        }
        return null;
    }
}
