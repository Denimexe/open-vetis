package ru.denimexe.vetis.core.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import ru.denimexe.vetis.core.data.entities.Enterprise;
import ru.denimexe.vetis.core.data.entities.EnterpriseType;
import ru.denimexe.vetis.core.data.services.AddressService;
import ru.denimexe.vetis.core.data.services.EnterpriseService;
import ru.denimexe.vetis.core.data.services.EnterpriseTypeService;
import ru.denimexe.vetis.core.soap.WS_EnterpriseService;
import ru.denimexe.vetis.core.utils.EnterpriseParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by denimexe on 13.11.17.
 */
@RestController
@RequestMapping(value = "rest/enterprise")
public class EnterpriseRestController {
    private final Logger LOGGER = LogManager.getLogger(EnterpriseRestController.class);

    @Autowired
    private EnterpriseService enterpriseService;
    @Autowired
    private EnterpriseTypeService enterpriseTypeService;
//    @Autowired
//    private WebJMSSender sender;
    @Autowired
    private AddressService addressService;

    @RequestMapping(value="types", method = RequestMethod.GET)
    @ResponseBody
    public List<EnterpriseType> getEnterpriseTypeList() {
        try {
            List<EnterpriseType> enterpriseTypes = enterpriseTypeService.findAll();
            return enterpriseTypes;
        } catch (Exception ex){
            LOGGER.error("getEnterpriseTypeList()", ex);
        }

        return new ArrayList<EnterpriseType>();
    }


    @RequestMapping(value="type/{id}", method = RequestMethod.GET)
    @ResponseBody
    public EnterpriseType getEnterpriseType(@PathVariable("id") int id) {
        try {
            return enterpriseTypeService.findOne(id);
        } catch (Exception ex){
            LOGGER.error("getEnterpriseType()", ex);
            return null;
        }
    }

    @RequestMapping(value="get/{guid}", method = RequestMethod.GET)
    @ResponseBody
    public Enterprise getEnterprise(@PathVariable("guid") String guid) {
        try {
            Enterprise enterprise = enterpriseService.findByGuid(guid);
            if(enterprise == null) {
                HashMap<String, Object> map = EnterpriseParser.parse(WS_EnterpriseService.getEnterprise(guid));
                if(map == null)
                    return null;
                String c_guid = (String)map.get("country");
                String r_guid = (String)map.get("region");
                String d_guid = (String)map.get("district");
                String l_guid = (String)map.get("locality");
                Integer type_id = Integer.valueOf((String)map.get("type"));


                enterprise = (Enterprise) map.get("enterprise");
                
                if(c_guid != null && !"".equals(c_guid)) {
                    enterprise.setCountry(addressService.findByGuid(c_guid));
                }

                if(r_guid != null && !"".equals(r_guid)) {
                    enterprise.setRegion(addressService.findByGuid(r_guid));
                }

                if(d_guid != null && !"".equals(d_guid)) {
                    enterprise.setDistrict(addressService.findByGuid(d_guid));
                }

                if(l_guid != null && !"".equals(l_guid)) {
                    enterprise.setLocality(addressService.findByGuid(l_guid));
                }

                enterprise.setType(enterpriseTypeService.findOne(type_id));
            } else {
                enterprise.setLinked(true);
            }
            return enterprise;
        } catch (Exception ex){
            LOGGER.error("getEnterprise()", ex);
        }

        return null;
    }

    @RequestMapping(value="byBusinessEntity/{id}", method = RequestMethod.GET)
    @ResponseBody
    public List<Enterprise> getEnterprisesByBusinessEntity(@PathVariable("id") int id) {
        try {
//            List<Enterprise> enterpriseList = enterpriseService.findByBusinessEntityId(id);
//            return enterpriseList;
            return new ArrayList<>();
        } catch (Exception ex){
            LOGGER.error("getEnterprisesByBusinessEntity()", ex);
        }

        return new ArrayList<Enterprise>();
    }

    @RequestMapping(value="all", method = RequestMethod.GET)
    @ResponseBody
    public List<Enterprise> getEnterprises() {
        try {
//            Enterprise testE = enterpriseService.getFullOne(5);
            List<Enterprise> enterpriseList = enterpriseService.findAll();
            return enterpriseList;
        } catch (Exception ex){
            ex.printStackTrace();
            LOGGER.error("getEnterprisesByBusinessEntity()", ex);
        }

        return new ArrayList<Enterprise>();
    }

    @RequestMapping(value="save", method = RequestMethod.POST)
    @ResponseBody
    public boolean saveEnterprise(HttpEntity<String> httpEntity) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            Enterprise enterprise = mapper.readValue(httpEntity.getBody(), Enterprise.class);
            enterpriseService.save(enterprise);
            return true;
        } catch (Exception ex) {
            LOGGER.error("saveEnterprise", ex);
            return false;
        }

    }
}
