package ru.denimexe.vetis.core.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;
import ru.denimexe.vetis.core.data.entities.AddressType;

/**
 * Created by denimexe on 04.10.17.
 */
@Repository
public interface AddressTypeRepository extends JpaRepository<AddressType, Integer>, QueryDslPredicateExecutor<AddressType> {
    AddressType findByName(String name);
}
