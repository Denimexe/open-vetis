package ru.denimexe.vetis.core.data.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * Created by denimexe on 25.09.17.
 */
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@NamedEntityGraph(name = "Enterprise.businessEntities", attributeNodes = @NamedAttributeNode("businessEntities"))
@Table(name = "enterprise", schema = "vetis")
public class Enterprise implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    public Integer getId(){ return id; }
    public void setId(Integer id){ this.id = id; }

    private String guid;
    private String name;
    @Column(name = "address_view")
    private String addressView;

    @ManyToOne @JoinColumn(name = "ref_enterprise_type_id")
    private EnterpriseType type;

    @ManyToOne @JoinColumn(name = "country_id")
    private Address country;
    @ManyToOne @JoinColumn(name = "region_id")
    private Address region;
    @ManyToOne @JoinColumn(name = "district_id")
    private Address district;
    @ManyToOne @JoinColumn(name = "locality_id")
    private Address locality;


    @JsonIgnore

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinTable(name = "businessEntity_enterprise_link",
            joinColumns = @JoinColumn(name = "enterprise_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "businessEntity_id", referencedColumnName = "id")
    )

//    @ManyToMany( mappedBy = "enterprises" )
    private Set<BusinessEntity> businessEntities = new HashSet<>();


    @Override
    public String toString() {
        return "<id>" + id +
                "</id><guid>" + guid + '\'' +
                "</guid><name>" + name + '\'' +
                "</name><addressView>" + addressView + '\'' +
                "</addressView><type>" + type +
                "</type><country>" + country +
                "</country><region>" + region +
                "</region><district>" + district +
                "</district><locality>" + locality +
                "</locality>";//<businessEntityId>" +
                //businessEntity != null ? businessEntity.getId().toString() : "null" +
                //"</businessEntityId>";
    }

    @Transient
    private boolean linked = false;

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (guid != null ? guid.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);

        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Enterprise enterprise = (Enterprise) o;
        return Objects.equals(id, enterprise.id) &&
                Objects.equals(guid, enterprise.guid) &&
                Objects.equals(name, enterprise.name) &&
                Objects.equals(addressView, enterprise.addressView) &&
                Objects.equals(country, enterprise.country) &&
                Objects.equals(region, enterprise.region) &&
                Objects.equals(district, enterprise.district) &&
                Objects.equals(locality, enterprise.locality) &&
                Objects.equals(type, enterprise.type);
    }
}
