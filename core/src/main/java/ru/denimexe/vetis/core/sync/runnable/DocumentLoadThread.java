package ru.denimexe.vetis.core.sync.runnable;

import lombok.extern.log4j.Log4j2;
import org.springframework.transaction.annotation.Transactional;
import ru.denimexe.vetis.core.data.entities.*;
import ru.denimexe.vetis.core.data.services.*;
import ru.denimexe.vetis.core.soap.WS_ApplicationManagementService;
import ru.denimexe.vetis.core.soap.WS_EnterpriseService;
import ru.denimexe.vetis.core.soap.WS_ProductService;
import ru.denimexe.vetis.core.utils.BusinessEntityParser;
import ru.denimexe.vetis.core.utils.DocumentParser;
import ru.denimexe.vetis.core.utils.EnterpriseParser;

import java.util.*;

/**
 * Created by denimexe on 20.09.17.
 */

@Log4j2
public class DocumentLoadThread extends Thread {

    private BusinessEntity iniciatorBusinessEntity = null;
    private Enterprise iniciatorEnterprise = null;
    private String applicationId = "";

    private ProductService productService;
    private DocumentService documentService;
    private EnterpriseService enterpriseService;
    private BusinessEntityService businessEntityService;
    private SubProductService subProductService;
    private AddressService addressService;
    private UnitService unitService;

    public void init(BusinessEntity businessEntity,
                     Enterprise enterprise,
                     String applicationId,
                     ProductService productService,
                     DocumentService documentService,
                     EnterpriseService enterpriseService,
                     BusinessEntityService businessEntityService,
                     SubProductService subProductService,
                     AddressService addressService,
                     UnitService unitService) {

        this.iniciatorBusinessEntity = businessEntity;
        this.iniciatorEnterprise = enterprise;
        this.applicationId = applicationId;
        this.productService = productService;
        this.documentService = documentService;
        this.enterpriseService = enterpriseService;
        this.businessEntityService = businessEntityService;
        this.subProductService = subProductService;
        this.addressService = addressService;
        this.unitService = unitService;
    }

    @Override
    public void run() {

        try {

            HashMap<String, Object> map;

            int total_documents = Integer.MAX_VALUE;
            int read = 0;
            int request_count = 1;
            boolean first_lap = true;

            LinkedList<Document> documents = new LinkedList<>();

            while (read < total_documents) {

                Date issueDate = new Date();


                System.out.println("Lap #" + (request_count));
                int timeOut = 5;
                while (true) {
                    System.out.println("trying to get response");
                    map = DocumentParser.parse(WS_ApplicationManagementService.getApplicationsPage(applicationId));

                    if (map != null && ((Integer) map.get("status") != 0)) {
                        break;
                    }

                    if(--timeOut == 0) {
                        map = new HashMap<>();
                        map.put("status", -1);
                        map.put("errorMessage", "превышено время ожидания запроса");
                        break;
                    }

                    this.sleep(5000);
                }

                if ((Integer) map.get("status") < 0) {
                    log.info("error: " + map.get("errorMessage"));
                    return;
                } else {

                    if(first_lap) {
                        total_documents = Integer.valueOf((String)map.get("totalAmount"));
                        first_lap = false;
                    }
                    ArrayList<HashMap> docListMap = (ArrayList<HashMap>) map.get("result");

                    for (HashMap docMap : docListMap) {
                        String uuid = (String) docMap.get("uuid");

                        Document document = documentService.findByUUID(uuid);
                        if(document == null) {
                            document = new Document();
                            document.setUuid(uuid);
                        }

                        document.setUuid((String) docMap.get("uuid"));

                        String formName = (String) docMap.get("form");
                        String typeName = (String) docMap.get("type");
                        String statusName = (String) docMap.get("status");

                        document.setForm(documentService.findFormByName(formName));
                        document.setType(documentService.findTypeByName(typeName));
                        document.setStatus(documentService.findStatusByName(statusName));

                        String beGUID, eGUID;
                        BusinessEntity businessEntity;
                        Enterprise enterprise;

                        HashMap<String, String> consignor = (HashMap<String, String>) docMap.get("consignor");

                        if(consignor != null) {
                            beGUID = consignor.get("businessEntity");
                            eGUID = consignor.get("enterprise");
                            businessEntity = businessEntityService.getFullByGuid(beGUID);
                            enterprise = enterpriseService.findByGuid(eGUID);

                            if (
                                    (enterprise == null && (eGUID == null || "".equals(eGUID))) ||
                                            (businessEntity == null && (beGUID == null || "".equals(beGUID)))) {
                                continue;
                            }
                            if (!handleWithBusinessEntityAndEnterprise(businessEntity, beGUID, enterprise, eGUID)) {
                                log.info("problem with consignor");
                                return;
                            }

                            document.setConsignorBusinessEntity(businessEntity);
                            document.setConsignorEnterprise(enterprise);
                        }

                        HashMap<String, String> consignee = (HashMap<String, String>) docMap.get("consignee");

                        if(consignee != null) {
                            beGUID = consignee.get("businessEntity");
                            eGUID = consignee.get("enterprise");
                            businessEntity = businessEntityService.getFullByGuid(beGUID);
                            enterprise = enterpriseService.findByGuid(eGUID);

                            if (
                                    (enterprise == null && (eGUID == null || "".equals(eGUID))) ||
                                            (businessEntity == null && (beGUID == null || "".equals(beGUID)))) {
                                continue;
                            }

                            if (!handleWithBusinessEntityAndEnterprise(businessEntity, beGUID, enterprise, eGUID)) {
                                log.info("problem with consignee");
                                return;
                            }

                            document.setConsigneeBusinessEntity(businessEntity);
                            document.setConsigneeEnterprise(enterprise);
                        }

                        document.setIssueNumber((String) docMap.get("issueNumber"));
                        document.setIssueSeries((String) docMap.get("issueSeries"));
                        document.setIssueDate(issueDate);
                        Batch batch = new Batch();
                        HashMap<String, String> batchMap = (HashMap<String, String>) docMap.get("batch");

                        ProductType productType =
                                productService.findType(Integer.valueOf(batchMap.get("productType")));
                        if (productType == null) {
                            productType = new ProductType();
                            productType.setName("номер " + batchMap.get("productType"));
                            productService.saveType(productType);
                        }

                        Product product = productService.findByGuid(batchMap.get("product"));

                        if (product == null) {
                            product = new Product();
                            HashMap<String, String> productMap =
                                    WS_ProductService.getParsedProduct(batchMap.get("product"));

                            if (productMap == null) {
                                return;
                            }

                            product.setName(productMap.get("name"));
                            product.setCode(productMap.get("code"));
                            product.setGuid(productMap.get("guid"));
                            product.setType(productType);

                            log.info("creating new product: " + product.getName());
                            productService.save(product);
                        }


                        batch.setProduct(product);

                        // ~~~~

                        SubProduct subProduct = subProductService.findByGuid(batchMap.get("subProduct"));

                        if (subProduct == null) {
                            subProduct = new SubProduct();
                            HashMap<String, String> subProductMap =
                                    WS_ProductService.getParsedSubProduct(batchMap.get("subProduct"));

                            if (subProductMap == null) {
                                return;
                            }

                            subProduct.setName(subProductMap.get("name"));
                            subProduct.setCode(subProductMap.get("code"));
                            subProduct.setGuid(subProductMap.get("guid"));
                            subProduct.setProduct(product);

                            log.info("creating new subProduct: " + subProduct.getName());
                            subProductService.save(subProduct);
                        }

                        batch.setSubProduct(subProduct);
                        batch.setProductItem(batchMap.get("productItem"));
                        batch.setVolume(Float.valueOf(batchMap.get("volume")));

                        Unit unit = unitService.findByGuid(batchMap.get("unit"));
                        batch.setUnit(unit);

                        Address originCountry = addressService.findByGuid(batchMap.get("country"));
                        batch.setCountryOfOrigin(originCountry);

                        batch.setDateOfProduction(batchMap.get("dateOfProduction"));
                        batch.setExpiryDate(batchMap.get("expiryDate"));

                        documentService.save(batch);


                        document.setBatch(batch);
                        document.setRequestorBusinessEntity(iniciatorBusinessEntity);
                        document.setRequestorEnterprise(iniciatorEnterprise);

                        String expertFio = (String) docMap.get("expert");

                        Expert expert = documentService.findExpertByName(expertFio);
                        if(expert == null) {
                            expert = new Expert();
                            expert.setName(expertFio);
                            documentService.save(expert);
                        }

                        document.setExpert(expert);
                        documents.add(document);

                    }
                    read += docListMap.size();
                }

                try {
                    applicationId = WS_ApplicationManagementService.submitRequestInterval(
                            iniciatorBusinessEntity.getGuid(),
                            iniciatorEnterprise.getGuid(),
                            request_count++
                    );
                } catch ( Exception ex) {
                    log.error(ex);
                    System.out.println(ex.getMessage());
                    return;
                }

            }

        saveDocuments(documents);

        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("exception in " + ex);
        }

        log.info("document downloading completed");
    }

    @Transactional
    private void saveDocuments(LinkedList<Document> documents) {
        for(Document d : documents) {
            documentService.save(d);
        }
    }

    private boolean handleWithBusinessEntityAndEnterprise(BusinessEntity businessEntity, String beGUID, Enterprise enterprise, String eGUID) {
        try {

            if(enterprise == null) {
                HashMap<String, Object> map = EnterpriseParser.parse(WS_EnterpriseService.getEnterprise(eGUID));
                if(map == null)
                    return false;
                String c_guid = (String)map.get("country");
                String r_guid = (String)map.get("region");
                String d_guid = (String)map.get("district");
                String l_guid = (String)map.get("locality");
                Integer type_id = Integer.valueOf((String)map.get("type"));


                enterprise = (Enterprise) map.get("enterprise");

                if(c_guid != null && !"".equals(c_guid)) {
                    enterprise.setCountry(addressService.findByGuid(c_guid));
                }

                if(r_guid != null && !"".equals(r_guid)) {
                    enterprise.setRegion(addressService.findByGuid(r_guid));
                }

                if(d_guid != null && !"".equals(d_guid)) {
                    enterprise.setDistrict(addressService.findByGuid(d_guid));
                }

                if(l_guid != null && !"".equals(l_guid)) {
                    enterprise.setLocality(addressService.findByGuid(l_guid));
                }

                enterprise.setType(enterpriseService.findType(type_id));
                log.info("creating new enterprise: " + enterprise.getName());
                enterpriseService.save(enterprise);
            }

            if (businessEntity == null) {
                HashMap<String, Object> map = BusinessEntityParser.parse(WS_EnterpriseService.getBusinessEntity(beGUID));

                if(map == null)
                    return false;

                String c_guid = (String)map.get("country");
                String r_guid = (String)map.get("region");
                String d_guid = (String)map.get("district");
                String l_guid = (String)map.get("locality");
                Integer type_id = Integer.valueOf((String)map.get("type"));


                businessEntity = (BusinessEntity) map.get("businessEntity");

                if(c_guid != null && !"".equals(c_guid)) {
                    businessEntity.setCountry(addressService.findByGuid(c_guid));
                }

                if(r_guid != null && !"".equals(r_guid)) {
                    businessEntity.setRegion(addressService.findByGuid(r_guid));
                }

                if(d_guid != null && !"".equals(d_guid)) {
                    businessEntity.setDistrict(addressService.findByGuid(d_guid));
                }

                if(l_guid != null && !"".equals(l_guid)) {
                    businessEntity.setLocality(addressService.findByGuid(l_guid));
                }

                businessEntity.setType(businessEntityService.findType(type_id));
                businessEntityService.save(businessEntity);

                businessEntity = businessEntityService.getFullOne(businessEntity.getId());

                Set<Enterprise> set = new HashSet<>();
                set.add(enterprise);
                businessEntity.setEnterprises(set);

                log.info("creating new businessEntity: " + businessEntity.getName());
                businessEntityService.save(businessEntity);
            } else {

                if(businessEntity.getEnterprises() == null) {
                    businessEntity.setEnterprises(new HashSet<>());
                }

                Set<Enterprise> set = businessEntity.getEnterprises();

                if(!businessEntity.getEnterprises().contains(enterprise)) {
                    set.add(enterprise);
                    businessEntityService.save(businessEntity);
                }
            }

            return true;
        } catch (Exception ex) {
            log.error(ex);
            return false;
        }
    }
}

