package ru.denimexe.vetis.core.data.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 * Created by denimexe on 19.09.17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "request", schema = "vetis")
public class Request implements Serializable{

    @Transient
    protected final SimpleDateFormat format = new SimpleDateFormat("dd.MM.yy, HH:mm");

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    private Date since;
    private Date upto;
    private String applicationId;
    private Date time;

    private Boolean fetched = false;

    @ManyToOne
    @JoinColumn(name = "enterprise_id", nullable = true)
    @NonNull private Enterprise enterprise;

    @ManyToOne
    @JoinColumn(name = "business_entity_id", nullable = true)
    @NonNull private BusinessEntity businessEntity;

    @Transient
    public String getTimeStr(){
        return format.format(time);
    }

    @Override
    public String toString() {
        return  "<id>" + id +
                "</id>";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Request request = (Request) o;
        return Objects.equals(format, request.format) &&
                Objects.equals(id, request.id) &&
                Objects.equals(since, request.since) &&
                Objects.equals(upto, request.upto) &&
                Objects.equals(applicationId, request.applicationId) &&
                Objects.equals(time, request.time) &&
                Objects.equals(fetched, request.fetched) &&
                Objects.equals(enterprise, request.enterprise) &&
                Objects.equals(businessEntity, request.businessEntity);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), format, id, since, upto, applicationId, time, fetched, enterprise, businessEntity);
    }
}
