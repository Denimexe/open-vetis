package ru.denimexe.vetis.core.JMS;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by denimexe on 16.10.17.
 */
@Service
public class CoreJMSListener {

    private final Logger LOGGER = LogManager.getLogger(CoreJMSListener.class);

    @JmsListener(destination = "core")
//    @SendTo("web")
    public String processSendMessage(String text){
        LOGGER.info(new SimpleDateFormat("dd.MM.yy [hh:mm:ss]").format(new Date()) + ": '" + text + "' ");
        return "ack";// + new SimpleDateFormat("dd.MM.yy [hh:mm:ss]").format(new Date());
    }
}
