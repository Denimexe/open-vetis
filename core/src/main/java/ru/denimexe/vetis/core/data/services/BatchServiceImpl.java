package ru.denimexe.vetis.core.data.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.denimexe.vetis.core.data.entities.Batch;
import ru.denimexe.vetis.core.data.repositories.BatchRepository;

import java.util.List;

/**
 * Created by dirde on 15.11.2016.
 */
@Service
public class BatchServiceImpl implements BatchService {

    @Autowired
    private BatchRepository repository;

    @Override
    public void save(Batch batch) {
        repository.save(batch);
    }

    @Override
    public Batch findOne(Integer id) {
        return repository.findOne(id);
    }

    @Override
    public List<Batch> findAll() {
        return repository.findAll();
    }
}
