package ru.denimexe.vetis.core.data.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.denimexe.vetis.core.data.entities.Notification;
import ru.denimexe.vetis.core.data.repositories.NotificationRepository;

import java.util.List;

/**
 * Created by denimexe on 10.10.17.
 */
@Service
public class NotificationServiceImpl implements NotificationService {
    private final Logger LOGGER = LogManager.getLogger(NotificationServiceImpl.class);

    @Autowired
    private NotificationRepository repository;

    @Override
    public void save(Notification notification) { repository.save(notification); }

    @Override
    public Notification findOne(Integer id) { return repository.findOne(id); }

    @Override
    public List<Notification> findAll() { return repository.findAllByOrderByDateDesc(); }
}
