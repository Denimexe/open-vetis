package ru.denimexe.vetis.core.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import ru.denimexe.vetis.core.data.entities.Address;
import ru.denimexe.vetis.core.data.entities.AddressType;

import java.util.List;

/**
 * Created by denimexe on 04.10.17.
 */
@Repository
public interface AddressRepository extends JpaRepository<Address, Integer>, QueryDslPredicateExecutor<Address>{

    Address findByGuid(String guid);

    List<Address> findByTypeId(Integer id);

    List<Address> findByNameContaining(String name);

    List<Address> findByParentGuid(String parent);

    List<Address> findByType(AddressType type);

    List<Address> findByTypeAndParentGuid(AddressType type, String parentGuid);

}
