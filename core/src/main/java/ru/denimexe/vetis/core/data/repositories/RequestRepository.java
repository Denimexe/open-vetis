package ru.denimexe.vetis.core.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;
import ru.denimexe.vetis.core.data.entities.BusinessEntity;
import ru.denimexe.vetis.core.data.entities.Enterprise;
import ru.denimexe.vetis.core.data.entities.Request;

/**
 * Created by denimexe on 04.10.17.
 */
@Repository
public interface RequestRepository extends JpaRepository<Request, Integer>, QueryDslPredicateExecutor<Request> {

    Request findByBusinessEntityAndEnterprise(BusinessEntity businessEntity, Enterprise enterprise);

}
