package ru.denimexe.vetis.core.data.services;

import ru.denimexe.vetis.core.data.entities.*;

import java.util.HashMap;
import java.util.List;

/**
 * Created by dirde on 15.11.2016.
 */
public interface DocumentService {

    void save(Document document);

    Document findOne(Integer id);

    List<Document> findAll();

    Document findByUUID(String uuid);

    List<Document> findByRequestor(BusinessEntity businessEntity, Enterprise enterprise);
    
    
    DocumentType findType(Integer id);
    DocumentType findTypeByName(String name);

    DocumentStatus findStatus(Integer id);
    DocumentStatus findStatusByName(String name);

    DocumentForm findForm(Integer id);
    DocumentForm findFormByName(String name);

    Batch findBatch(Integer id);
    List<Batch> findBatches();
    void save(Batch batch);

    Expert findExpertByName(String name);
    void save(Expert expert);

    HashMap<String, String> getReport();
    
    
}
