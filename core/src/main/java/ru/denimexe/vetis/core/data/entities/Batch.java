package ru.denimexe.vetis.core.data.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Created by denimexe on 29.09.17.
 */
@Data
@Entity
@NoArgsConstructor
@Table(name = "batch", schema = "vetis")
public class Batch implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    private String productItem;
    private Float volume;
    private String dateOfProduction;
    private String expiryDate;

    @ManyToOne @JoinColumn(name = "ref_product_id")
    private Product product;
    @ManyToOne @JoinColumn(name = "ref_sub_product_id")
    private SubProduct subProduct;

    @ManyToOne @JoinColumn(name = "ref_unit_id")
    private Unit unit;

    @ManyToOne @JoinColumn(name = "country_of_origin_id")
    private Address countryOfOrigin;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Batch batch = (Batch) o;
        return Objects.equals(id, batch.id) &&
                Objects.equals(productItem, batch.productItem) &&
                Objects.equals(volume, batch.volume) &&
                Objects.equals(dateOfProduction, batch.dateOfProduction) &&
                Objects.equals(expiryDate, batch.expiryDate) &&
                Objects.equals(product, batch.product) &&
                Objects.equals(subProduct, batch.subProduct) &&
                Objects.equals(unit, batch.unit) &&
                Objects.equals(countryOfOrigin, batch.countryOfOrigin);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), id, productItem, volume, dateOfProduction, expiryDate, product, subProduct, unit, countryOfOrigin);
    }

    @Override
    public String toString() {
        return "<id>" + id +
                "</id><productItem>" + productItem +
                "</productItem><volume>" + volume +
                "</volume><dateOfProduction>" + dateOfProduction +
                "</dateOfProduction><expiryDate>" + expiryDate +
                "</expiryDate><product>" + product +
                "</product><subProduct>" + subProduct +
                "</subProduct><unit>" + unit +
                "</unit>";
    }
}
