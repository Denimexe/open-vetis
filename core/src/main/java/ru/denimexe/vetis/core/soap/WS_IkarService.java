package ru.denimexe.vetis.core.soap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import ru.denimexe.vetis.core.data.entities.Address;
import ru.denimexe.vetis.core.utils.Constants;
import ru.denimexe.vetis.core.utils.Namespace;
import ru.denimexe.vetis.core.utils.SoapServicesUtil;
import ru.denimexe.vetis.core.utils.SoapUtils;

import javax.xml.soap.*;
import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.util.ArrayList;

import static ru.denimexe.vetis.core.utils.SoapUtils.createRequest;

/**
 * Created by denimexe on 25.09.17.
 */
public class WS_IkarService {

    private static final String url = "https://api.vetrf.ru/platform/ikar/services/IkarService";

    public static final Namespace NS_WS = new Namespace("ws", "http://api.vetrf.ru/schema/cdm/ikar/ws-definitions");
    public static final Namespace NS_IKAR = new Namespace("ikar", "http://api.vetrf.ru/schema/cdm/ikar");

    public static String getRegion(String guid) throws Exception{

        String r_guid = "";

        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection connection = soapConnectionFactory.createConnection();

        SOAPMessage soapMessage = createRequest();

        //  ~~~~~~~~~

        SOAPPart soapPart = soapMessage.getSOAPPart();
        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        SoapServicesUtil.setNamespaces(envelope, new ArrayList<Namespace>() {{add(NS_WS); add(Constants.NS_BS);}});

        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        SOAPElement main = soapBody.addChildElement("getEnterpriseByGuidRequest", NS_WS.getName());
        main.addChildElement("guid", Constants.NS_BS.getName()).addTextNode(guid);
        soapMessage.saveChanges();

        //  ~~~~~~~~~

        SOAPMessage response = connection.call(soapMessage, new URL(url));


        ByteArrayOutputStream out = new ByteArrayOutputStream();
        response.writeTo(out);

        Document doc = Jsoup.parse(new String(out.toByteArray(), "UTF-8"));

        Element e = doc.select("ikar|region").first();
        r_guid = e.select("bs|guid").first().text().trim();

        return r_guid;
    }

    public static byte[] getCountryList(Integer request_count) throws Exception{

        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection connection = soapConnectionFactory.createConnection();

        SOAPMessage soapMessage = createRequest();

        //  ~~~~~~~~~

        SOAPPart soapPart = soapMessage.getSOAPPart();
        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        SoapServicesUtil.setNamespaces(envelope, new ArrayList<Namespace>() {{add(NS_WS); add(Constants.NS_BS);}});


        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        SOAPElement main = soapBody.addChildElement("getAllCountryListRequest", NS_WS.getName());
        SoapUtils.addListOptions(main, Constants.REQUEST_COUNT, request_count*Constants.REQUEST_COUNT);
        soapMessage.saveChanges();

        //  ~~~~~~~~~

        SOAPMessage response = connection.call(soapMessage, new URL(url));


        ByteArrayOutputStream out = new ByteArrayOutputStream();
        response.writeTo(out);

        return out.toByteArray();
    }

    public static byte[] getRegionListByCountryGuid(String guid, Integer request_count) throws Exception {
        return getSubAddressListByAddressGuid("getRegionListByCountryRequest", "countryGuid", guid, request_count);
    }

    public static byte[] getDistrictListByRegionGuid(String guid, Integer request_count) throws Exception {
        return getSubAddressListByAddressGuid("getDistrictListByRegionRequest", "regionGuid", guid, request_count);
    }

    public static byte[] getLocalityListByDistrictGuid(String guid, Integer request_count) throws Exception {
        return getSubAddressListByAddressGuid("getLocalityListByDistrictRequest", "districtGuid", guid, request_count);
    }

    public static byte[] getLocalityListByRegionGuid(String guid, Integer request_count) throws Exception {
        return getSubAddressListByAddressGuid("getLocalityListByRegionRequest", "regionGuid", guid, request_count);
    }

    public static byte[] getSubAddressListByAddressGuid(String request, String parentElement, String guid, Integer request_count) throws Exception{

        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection connection = soapConnectionFactory.createConnection();

        SOAPMessage soapMessage = createRequest();

        //  ~~~~~~~~~

        SOAPPart soapPart = soapMessage.getSOAPPart();
        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        SoapServicesUtil.setNamespaces(envelope, new ArrayList<Namespace>() {{
            add(NS_WS);
            add(Constants.NS_BS);
            add(NS_IKAR);
        }});


        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        SOAPElement main = soapBody.addChildElement(request, NS_WS.getName());
        main.addChildElement(parentElement, NS_IKAR.getName()).addTextNode(guid);
        SoapUtils.addListOptions(main, Constants.REQUEST_COUNT, request_count*Constants.REQUEST_COUNT);
        soapMessage.saveChanges();

        //  ~~~~~~~~~

        SOAPMessage response = connection.call(soapMessage, new URL(url));


        ByteArrayOutputStream out = new ByteArrayOutputStream();
        response.writeTo(out);

        return out.toByteArray();
    }
}
