package ru.denimexe.vetis.core.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.denimexe.vetis.core.data.entities.BusinessEntity;
import ru.denimexe.vetis.core.data.entities.Document;
import ru.denimexe.vetis.core.data.entities.Enterprise;
import ru.denimexe.vetis.core.data.entities.Request;
import ru.denimexe.vetis.core.data.services.BusinessEntityService;
import ru.denimexe.vetis.core.data.services.DocumentService;
import ru.denimexe.vetis.core.data.services.EnterpriseService;
import ru.denimexe.vetis.core.data.services.RequestService;
import ru.denimexe.vetis.core.soap.WS_ApplicationManagementService;
import ru.denimexe.vetis.core.utils.DocumentLoadService;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by denimexe on 13.11.17.
 */
@RestController
@RequestMapping(value = "rest/document")
public class DocumentRestController {
    private final Logger LOGGER = LogManager.getLogger(DocumentRestController.class);

    @Autowired
    private BusinessEntityService businessEntityService;
    @Autowired
    private EnterpriseService enterpriseService;
    @Autowired
    private RequestService requestService;
    @Autowired
    private DocumentService documentService;

    @Autowired
    private DocumentLoadService documentLoadService;


    @RequestMapping(value="request/all", method = RequestMethod.GET)
    @ResponseBody
    public List<Request> getRequests() {
        List<Request> result = new ArrayList<>();
        try {
            result = requestService.findAll();
        } catch ( Exception ex) {
        }

        return result;
    }

    @RequestMapping(value="request/:id", method = RequestMethod.POST)
    @ResponseBody
    public JsonObject getRequestedDocuments(@PathVariable("id") Integer id) {
        JsonObjectBuilder objectBuilder = Json.createObjectBuilder();

        Request request = requestService.findOne(id);

        if(request == null) {
            objectBuilder.add("result", false);
            objectBuilder.add("info", "Ошибка получения запроса");
        } else if (request.getFetched() == null || !request.getFetched()){
            objectBuilder.add("result", false);
            objectBuilder.add("info", "Данные загружаются, пожалуйста, попробуйте позднее");
        } else {

            try {
                JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
                List<Document> documents = documentService.findByRequestor(request.getBusinessEntity(), request.getEnterprise());

                ObjectMapper mapper = new ObjectMapper();
                for (Document doc : documents) {
                    arrayBuilder.add(mapper.writeValueAsString(doc));
                }
                objectBuilder.add("result", true);
                objectBuilder.add("resultData", arrayBuilder.build());
            } catch (Exception ex) {

            }
        }

        return objectBuilder.build();
    }


    @RequestMapping(value="request/{beId}/{eId}", method = RequestMethod.POST)
    @ResponseBody
    public String requestDocuments(@PathVariable("beId") Integer beId, @PathVariable("eId") Integer eId) {
        try {

            String applicationId = "";

            BusinessEntity businessEntity = businessEntityService.findOne(beId);
            Enterprise enterprise = enterpriseService.findOne(eId);

            Request request = requestService.findByEndpoint(businessEntity, enterprise);
            if(request == null) {
                applicationId = WS_ApplicationManagementService.submitRequestInterval(
                        businessEntity.getGuid(),
                        enterprise.getGuid(),
                        0
                );

                request = new Request();
                request.setBusinessEntity(businessEntity);
                request.setEnterprise(enterprise);
                request.setApplicationId(applicationId);
                request.setTime(new Date());
                requestService.save(request);

            } else {
                applicationId = request.getApplicationId();
            }

            documentLoadService.runThread(businessEntity, enterprise, applicationId);

            return applicationId;
        } catch (Exception ex){

            LOGGER.error("getBusinessEntityEnterprises()", ex);
            return null;
        }
    }

    @RequestMapping(value="report", method = RequestMethod.POST)
    @ResponseBody
    public JsonObject getReport() {
        JsonObjectBuilder objectBuilder = Json.createObjectBuilder();

//        Request request = .findOne(id);

//        if(request == null) {
            objectBuilder.add("result", false);
            objectBuilder.add("info", "Ошибка получения запроса");
//        } else if (request.getFetched() == null || !request.getFetched()){
//            objectBuilder.add("result", false);
//            objectBuilder.add("info", "Данные загружаются, пожалуйста, попробуйте позднее");
//        } else {
//
//            try {
//                JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
//                List<Document> documents = documentService.findByRequestor(request.getBusinessEntity(), request.getEnterprise());
//
//                ObjectMapper mapper = new ObjectMapper();
//                for (Document doc : documents) {
//                    arrayBuilder.add(mapper.writeValueAsString(doc));
//                }
//                objectBuilder.add("result", true);
//                objectBuilder.add("resultData", arrayBuilder.build());
//            } catch (Exception ex) {
//
//            }
//        }

        return objectBuilder.build();
    }



}
