package ru.denimexe.vetis.core.data.services;

import ru.denimexe.vetis.core.data.entities.SubProduct;

import java.util.List;

/**
 * Created by dirde on 15.11.2016.
 */
public interface SubProductService {

    void save(SubProduct subProduct);

    SubProduct findOne(Integer id);

    SubProduct findByGuid(String guid);

    List<SubProduct> findAll();
}
