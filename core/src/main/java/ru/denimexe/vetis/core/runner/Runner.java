package ru.denimexe.vetis.core.runner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.denimexe.vetis.core.data.entities.Address;
import ru.denimexe.vetis.core.data.entities.AddressType;
import ru.denimexe.vetis.core.data.services.AddressService;
import ru.denimexe.vetis.core.soap.WS_ApplicationManagementService;
import ru.denimexe.vetis.core.soap.WS_IkarService;

/**
 * Created by denimexe on 04.10.17.
 */
@Deprecated
@Service
public class Runner {
    private final Logger LOGGER = LogManager.getLogger(Runner.class);

//    @Autowired
//    private DictionaryManager dictionaryManager;
    @Autowired
    private AddressService addressService;
//    @Autowired
//    private JmsMessageSender sender;


    public void invokeMethod(){
        /*

        try {

            System.out.println(
                    WS_ApplicationManagementService.submitRequestInterval(
//                            new Date(117, 10, 8),
//                            new Date(117, 10, 9),
                            "9b7520f1-3b2f-4e92-8ae7-1fc00267f14c",
                            "18148d7c-ae46-44b6-9407-fd637e763f84"
                    )
            );

        } catch ( Exception ex) {
            LOGGER.error("exception", ex);
        }
    }

    public void fetchAllAddresses() {
        try {

            int c_count = 1, r_count = 1, d_count = 1, l_count = 1, rl_count = 1;
            int c_amount, r_amount, d_amount, l_amount, rl_amount, grand_counter = 0;

            // ~~~~~~~~~ COUNTRIES ~~~~~~~~~
            byte[] country_bytes = WS_IkarService.getCountryList(0);
            Document country_doc = Jsoup.parse(new String(country_bytes, "UTF-8"));

            Elements countries = country_doc.select("ikar|country");

            boolean block_country = true;
            boolean block_region = true;
            boolean block_district = true;

            final AddressType COUNTRY = addressService.findTypeByName("страна");
            final AddressType REGION = addressService.findTypeByName("регион");
            final AddressType DISTRICT = addressService.findTypeByName("район");
            final AddressType LOCALITY = addressService.findTypeByName("населённый пункт");

            c_amount = countries.size();

            for(Element c : countries){

                System.out.format("%8d ", grand_counter++);
                System.out.println(c_count++ + "/" + c_amount);

                String country_guid = c.select("bs|guid").text();

                if("21576e95-da37-610f-6074-b09dffba4947".equals(country_guid)) {
                    block_country = false;
                }

                if(block_country){
                    continue;
                }

                Address country = addressService.findByGuid(country_guid);

                if(country == null) {
                    country = new Address(
                            country_guid,
                            c.select("ikar|name").text(),
                            null,
                            null,
                            COUNTRY
                    );
                    addressService.save(country);
                }

                // ~~~~~~~~~ REGIONS ~~~~~~~~~
                byte[] region_bytes = WS_IkarService.getRegionListByCountryGuid(country_guid, 0);
                Document region_doc = Jsoup.parse(new String(region_bytes, "UTF-8"));

                Elements regions = region_doc.select("ikar|region");
                r_count = 1;
                r_amount = regions.size();

                for(Element r: regions) {

                    System.out.format("%8d ", grand_counter++);
                    System.out.println("\t" + r_count++ + "/" + r_amount);

                    String region_guid = r.select("bs|guid").text();

                    if("ddc385ae-3c1f-6ce2-1dbd-d94c7c8dcb34".equals(region_guid)) {
                        block_region = false;
                    }

                    if(block_region){
                        continue;
                    }

                    Address region = addressService.findByGuid(region_guid);

                    if(region == null) {
                        region = new Address(
                                region_guid,
                                r.select("ikar|name").text(),
                                r.select("ikar|type").text(),
                                country_guid,
                                REGION
                        );
                        addressService.save(region);
                    }

                    // ~~~~~~~~~ DISTRICTS ~~~~~~~~~

                    byte[] district_bytes = WS_IkarService.getDistrictListByRegionGuid(region_guid, 0);
                    Document district_doc = Jsoup.parse(new String(district_bytes, "UTF-8"));

                    Elements districts = district_doc.select("ikar|district");

                    d_amount = districts.size();
                    d_count = 1;

                    
                    for(Element d: districts) {

                        System.out.format("%8d ", grand_counter++);
                        System.out.println(d_count++ + "/" + d_amount);

                        String district_guid = d.select("bs|guid").text();

                        if("95a2eaf4-5dc5-131d-772b-45ed1bee6ba1".equals(district_guid)) {
                            block_district = false;
                        }

                        if(block_district){
                            continue;
                        }
                        Address district = addressService.findByGuid(district_guid);

                        if(district == null) {
                            district = new Address(
                                    district_guid,
                                    d.select("ikar|name").text(),
                                    d.select("ikar|type").text(),
                                    region_guid,
                                    DISTRICT
                            );
                            addressService.save(district);
                        }

                        // ~~~~~~~~~ LOCALITIES ~~~~~~~~~

                        byte[] locality_bytes = WS_IkarService.getLocalityListByDistrictGuid(district_guid, 0);
                        Document locality_doc = Jsoup.parse(new String(locality_bytes, "UTF-8"));

                        Elements localities = locality_doc.select("ikar|locality");

                        l_amount = localities.size();
                        l_count = 1;
                        
                        for(Element l: localities) {

                            System.out.format("%8d ", grand_counter++);
                            System.out.println(l_count++ + "/" + l_amount);
                            
                            String locality_guid = l.select("bs|guid").text();
                            Address locality = addressService.findByGuid(locality_guid);

                            if(locality == null) {
                                locality = new Address(
                                        locality_guid,
                                        l.select("ikar|name").text(),
                                        l.select("ikar|type").text(),
                                        district_guid,
                                        LOCALITY
                                );
                                addressService.save(locality);
                            }
                        }

                    }

                    // ~~~~~~~~~ LOCALITIES IN REGIONS ~~~~~~~~~

                    byte[] reg_locality_bytes = WS_IkarService.getLocalityListByDistrictGuid(region_guid, 0);
                    Document reg_locality_doc = Jsoup.parse(new String(reg_locality_bytes, "UTF-8"));

                    Elements reg_localities = reg_locality_doc.select("ikar|locality");

                    rl_amount = reg_localities.size();
                    rl_count = 1;
                    
                    for (Element rl: reg_localities) {

                        System.out.format("%8d ", grand_counter++);
                        System.out.println(rl_count++ + "/" + rl_amount);
                        
                        String locality_guid = rl.select("bs|guid").text();
                        Address locality = addressService.findByGuid(locality_guid);

                        if (locality == null) {
                            locality = new Address(
                                    locality_guid,
                                    rl.select("ikar|name").text(),
                                    rl.select("ikar|type").text(),
                                    region_guid,
                                    LOCALITY
                            );
                            addressService.save(locality);
                        }
                    }
                }

            }
        } catch (Exception ex) {
            LOGGER.error("fetchAllAddresses() ", ex);
        }
        */
    }
}
