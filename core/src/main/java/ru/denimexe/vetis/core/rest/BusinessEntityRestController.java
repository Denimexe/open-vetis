package ru.denimexe.vetis.core.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import ru.denimexe.vetis.core.data.entities.BusinessEntity;
import ru.denimexe.vetis.core.data.entities.BusinessEntityType;
import ru.denimexe.vetis.core.data.entities.Enterprise;
import ru.denimexe.vetis.core.data.services.*;
import ru.denimexe.vetis.core.soap.WS_EnterpriseService;
import ru.denimexe.vetis.core.utils.BusinessEntityParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created by denimexe on 13.11.17.
 */
@RestController
@RequestMapping(value = "rest/businessEntity")
public class BusinessEntityRestController {
    private final Logger LOGGER = LogManager.getLogger(BusinessEntityRestController.class);

    @Autowired
    private BusinessEntityService businessEntityService;
    @Autowired
    private EnterpriseService enterpriseService;

//    @Autowired
//    private WebJMSSender sender;
    @Autowired
    private AddressService addressService;

    @RequestMapping(value="types", method = RequestMethod.GET)
    @ResponseBody
    public List<BusinessEntityType> getBusinessEntityTypeList() {
        try {
            List<BusinessEntityType> businessEntityTypes = businessEntityService.findTypes();
            return businessEntityTypes;
        } catch (Exception ex){
            LOGGER.error("getBusinessEntityTypeList()", ex);
        }

        return new ArrayList<BusinessEntityType>();
    }

    @RequestMapping(value="all", method = RequestMethod.GET)
    @ResponseBody
    public List<BusinessEntity> getBusinessEntityList() {
        try {
//            BusinessEntity testBE = businessEntityService.getFullOne(2);

            List<BusinessEntity> BusinessEntities = businessEntityService.findAll();
            return BusinessEntities;
        } catch (Exception ex){
            ex.printStackTrace();
            LOGGER.error("getBusinessEntityList()", ex);
        }

        return new ArrayList<BusinessEntity>();
    }

    @RequestMapping(value="enterprises/{id}", method = RequestMethod.GET)
    @ResponseBody
    public List<Enterprise> getBusinessEntityFull(@PathVariable("id") int id) {
        try {
            BusinessEntity be = businessEntityService.getFullOne(id);
            return new ArrayList<>(be.getEnterprises());
        } catch (Exception ex){
            LOGGER.error("getBusinessEntityEnterprises()", ex);
            return new ArrayList<>();
        }
    }


    @RequestMapping(value="type/{id}", method = RequestMethod.GET)
    @ResponseBody
    public BusinessEntityType getBusinessEntityType(@PathVariable("id") int id) {
        try {
            return businessEntityService.findType(id);
        } catch (Exception ex){
            LOGGER.error("getBusinessEntityType()", ex);
            return null;
        }
    }

    @RequestMapping(value="get/{guid}", method = RequestMethod.GET)
    @ResponseBody
    public BusinessEntity getBusinessEntity(@PathVariable("guid") String guid) {
        try {
            BusinessEntity businessEntity = businessEntityService.findByGuid(guid);
            if(businessEntity == null) {
                HashMap<String, Object> map = BusinessEntityParser.parse(WS_EnterpriseService.getBusinessEntity(guid));
                if(map == null)
                    return null;
                String c_guid = (String)map.get("country");
                String r_guid = (String)map.get("region");
                String d_guid = (String)map.get("district");
                String l_guid = (String)map.get("locality");
                Integer type_id = Integer.valueOf((String)map.get("type"));


                businessEntity = (BusinessEntity) map.get("businessEntity");

                if(c_guid != null && !"".equals(c_guid)) {
                    businessEntity.setCountry(addressService.findByGuid(c_guid));
                }

                if(r_guid != null && !"".equals(r_guid)) {
                    businessEntity.setRegion(addressService.findByGuid(r_guid));
                }

                if(d_guid != null && !"".equals(d_guid)) {
                    businessEntity.setDistrict(addressService.findByGuid(d_guid));
                }

                if(l_guid != null && !"".equals(l_guid)) {
                    businessEntity.setLocality(addressService.findByGuid(l_guid));
                }

                businessEntity.setType(businessEntityService.findType(type_id));
            } else {
                businessEntity.setLinked(true);
            }
            return businessEntity;
        } catch (Exception ex){
            LOGGER.error("getBusinessEntity()", ex);
        }

        return null;
    }

    @RequestMapping(value="get/inn/{inn}", method = RequestMethod.GET)
    @ResponseBody
    public BusinessEntity getBusinessEntityByInn(@PathVariable("inn") String inn) {
        try {
            BusinessEntity businessEntity = businessEntityService.findByInn(inn);
            if(businessEntity == null) {
                HashMap<String, Object> map = BusinessEntityParser.parse(WS_EnterpriseService.getBusinessEntityByInn(inn));
                if(map == null)
                    return null;
                String c_guid = (String)map.get("country");
                String r_guid = (String)map.get("region");
                String d_guid = (String)map.get("district");
                String l_guid = (String)map.get("locality");
                Integer type_id = Integer.valueOf((String)map.get("type"));


                businessEntity = (BusinessEntity) map.get("businessEntity");

                if(c_guid != null && !"".equals(c_guid)) {
                    businessEntity.setCountry(addressService.findByGuid(c_guid));
                }

                if(r_guid != null && !"".equals(r_guid)) {
                    businessEntity.setRegion(addressService.findByGuid(r_guid));
                }

                if(d_guid != null && !"".equals(d_guid)) {
                    businessEntity.setDistrict(addressService.findByGuid(d_guid));
                }

                if(l_guid != null && !"".equals(l_guid)) {
                    businessEntity.setLocality(addressService.findByGuid(l_guid));
                }

                businessEntity.setType(businessEntityService.findType(type_id));
            } else {
                businessEntity.setLinked(true);
            }
            return businessEntity;
        } catch (Exception ex){
            LOGGER.error("getBusinessEntity()", ex);
        }

        return null;
    }

    @RequestMapping(value="save", method = RequestMethod.POST)
    @ResponseBody
    public boolean saveBusinessEntity(HttpEntity<String> httpEntity) {

        try {
            ObjectMapper mapper = new ObjectMapper();
            BusinessEntity businessEntity = mapper.readValue(httpEntity.getBody(), BusinessEntity.class);
            businessEntityService.save(businessEntity);
            return true;
        } catch (Exception ex) {
            LOGGER.error("saveBusinessEntity", ex);
            return false;
        }

    }


    @RequestMapping(value="saveEnterprises", method = RequestMethod.POST)
    @ResponseBody
    public boolean saveBusinessEntityEnterprises(HttpEntity<String> httpEntity) {

        try {

            ObjectMapper mapper = new ObjectMapper();
            ArrayList<Integer> strs = mapper.readValue(httpEntity.getBody(), mapper.getTypeFactory().constructCollectionType(List.class, Integer.class));

            if(strs == null || strs.isEmpty()) {
                return false;
            }

            int beId = strs.get(0);
            strs.remove(0);

            BusinessEntity businessEntity = businessEntityService.getFullOne(beId);

            if(strs.isEmpty()) {
                businessEntity.setEnterprises(new HashSet<Enterprise>());
            } else {
                ArrayList<Enterprise> enterprises = (ArrayList) enterpriseService.findByIdIn(strs);
                businessEntity.setEnterprises(new HashSet<Enterprise>(enterprises));
            }

            businessEntityService.save(businessEntity);

            return true;
        } catch (Exception ex) {
            LOGGER.error("saveBusinessEntity", ex);
            return false;
        }

    }


}
