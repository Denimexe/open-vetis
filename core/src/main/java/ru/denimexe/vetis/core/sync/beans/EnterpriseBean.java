package ru.denimexe.vetis.core.sync.beans;

import lombok.Getter;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ru.denimexe.vetis.core.utils.EnterpriseQueue;
import ru.denimexe.vetis.core.sync.runnable.EnterpriseParseThread;
import ru.denimexe.vetis.core.utils.Constants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by denimexe on 25.09.17.
 */
public class EnterpriseBean implements Serializable {
    @Getter
    private EnterpriseQueue queue;

    public EnterpriseBean() {
        queue = new EnterpriseQueue();
    }

    public void addData(org.jsoup.nodes.Document doc){
        List<Thread> threads = new ArrayList<>();
        Elements trs = doc.select("ent|enterprise");
        int count = trs.size();
        if(count == 0){
            return;
        }
        int bufSize = count < Constants.BUFFER_SIZE ? count : Constants.BUFFER_SIZE;
        int th_amount = ((Double)Math.ceil((double)count / bufSize)).intValue();
        int i = 0;

        while(i < th_amount) {
            int start_index = i * bufSize;
            int finish_index = ((i + 1) * bufSize);
            finish_index = count > finish_index ? finish_index : count;
            List<Element> list = trs.subList(start_index, finish_index);
            threads.add(
                    new EnterpriseParseThread(
                            list,
                            queue
                    )
            );
            i++;
        }

        for(Thread th: threads){
            try {
                th.join();
            } catch(Exception ex) {
                System.out.println("join: " + ex.toString());
            }
        }

    }
}
