package ru.denimexe.vetis.core.utils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;

/**
 * Created by denimexe on 13.09.17.
 */
public class VetDocumentJSONParser {
    public static JsonObjectBuilder parse (String data){
        try {
            Document doc = Jsoup.parse(data);
            Elements trs = doc.select("ns2|vetDocument");

            return toJsonObject(trs);
        } catch (Exception ex){
            System.out.println("VetDocumentJSONParser parse exception: " + ex.toString());
            return Json.createObjectBuilder();
        }
    }

    private static JsonObjectBuilder toJsonObject(Elements list){
        JsonArrayBuilder documents = Json.createArrayBuilder();

        for (Element e: list){
            JsonObjectBuilder document = Json.createObjectBuilder();

            document.add("uuid", e.select("bs|uuid").first().text().trim());

            String form = e.select("ns2|form").text().trim();

            if("PRODUCTIVE".equals(form)) {
                continue;
            }

            document.add("form", form);
            document.add("type", e.select("ns2|type").text().trim());
            document.add("status", e.select("ns2|status").text().trim());


            Elements consignee = e.select("ns2|consignee");

            if(consignee.size() != 0) {
                Elements businessEntity = consignee.select("ent|businessEntity");
                Elements enterprise = consignee.select("ent|enterprise");

                document.add("consignee",
                        Json.createObjectBuilder()
                                .add("businessEntity",
                                        Json.createObjectBuilder()
                                                .add("uuid", businessEntity.select("bs|uuid").text().trim())
                                                .add("guid", businessEntity.select("bs|guid").text().trim())
                               )
                                .add("BusinessEntity",
                                        Json.createObjectBuilder()
                                                .add("uuid", enterprise.select("bs|uuid").text().trim())
                                                .add("guid", enterprise.select("bs|guid").text().trim())
                               ).build()
               );

                String prodname = "";
                try {
                    prodname = e.select("prod|name").text().trim();
                } catch (Exception ex){}
                document.add("prodname", prodname);
                document.add("milkRaw",  "молоко сырое".equals(prodname));

            } else {
                document.addNull("consignee");
            }

            documents.add(                document.build()
           );
        }

        return Json.createObjectBuilder().add("vetDocuments", documents.build());
    }

}
