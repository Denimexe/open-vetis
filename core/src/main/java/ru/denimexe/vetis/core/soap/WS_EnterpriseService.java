package ru.denimexe.vetis.core.soap;

import ru.denimexe.vetis.core.utils.Constants;
import ru.denimexe.vetis.core.utils.Namespace;
import ru.denimexe.vetis.core.utils.SoapServicesUtil;
import ru.denimexe.vetis.core.utils.SoapUtils;

import javax.xml.soap.*;
import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.util.ArrayList;

import static ru.denimexe.vetis.core.utils.SoapUtils.createRequest;

/**
 * Created by denimexe on 25.09.17.
 */
public class WS_EnterpriseService {

    private static final String url = "https://api.vetrf.ru/platform/cerberus/services/EnterpriseService";

    public static final Namespace NS_WS_ENT = new Namespace("ws", "http://api.vetrf.ru/schema/cdm/cerberus/enterprise/ws-definitions");
    public static final Namespace NS_WS_BE = new Namespace("ws", "http://api.vetrf.ru/schema/cdm/cerberus/business-entity/ws-definitions");
    public static final Namespace NS_ENT = new Namespace("ent", "http://api.vetrf.ru/schema/cdm/cerberus/enterprise");
    public static final Namespace NS_IKAR = new Namespace("ikar", "http://api.vetrf.ru/schema/cdm/ikar");

    public static byte[] getEnterprisesByRegion(String guid, int request_count) throws Exception{

        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection connection = soapConnectionFactory.createConnection();

        SOAPMessage soapMessage = createRequest();

        //  ~~~~~~~~~

        SOAPPart soapPart = soapMessage.getSOAPPart();
        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();

        SoapServicesUtil.setNamespaces(
                envelope,
                new ArrayList<Namespace>() {{
                    add(Constants.NS_BS);
                    add(NS_WS_ENT);
                    add(NS_ENT);
                    add(NS_IKAR);
                }}
        );

        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        SOAPElement main = soapBody.addChildElement("getRussianEnterpriseListRequest", NS_WS_ENT.getName());

        SoapUtils.addListOptions(main, Constants.REQUEST_COUNT, request_count*Constants.REQUEST_COUNT);
        SOAPElement ent = main.addChildElement("enterprise", NS_ENT.getName());
        ent.addChildElement("address", NS_ENT.getName())
                .addChildElement("region", NS_IKAR.getName())
                .addChildElement("guid", Constants.NS_BS.getName()).addTextNode(guid);

        soapMessage.saveChanges();
        //  ~~~~~~~~~
        SOAPMessage response = connection.call(soapMessage, new URL(url));


        ByteArrayOutputStream out = new ByteArrayOutputStream();
        response.writeTo(out);

        return out.toByteArray();
    }

    public static byte[] getBusinessEntity(String guid) throws Exception{

        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection connection = soapConnectionFactory.createConnection();

        SOAPMessage soapMessage = createRequest();

        //  ~~~~~~~~~

        SOAPPart soapPart = soapMessage.getSOAPPart();
        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();

        SoapServicesUtil.setNamespaces(
                envelope,
                new ArrayList<Namespace>() {{
                    add(Constants.NS_BS);
                    add(NS_WS_BE);
                }}
        );

        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        SOAPElement main = soapBody.addChildElement("getBusinessEntityByGuidRequest", NS_WS_BE.getName())
                .addChildElement("guid", Constants.NS_BS.getName()).addTextNode(guid);

        soapMessage.saveChanges();
        //  ~~~~~~~~~
        SOAPMessage response = connection.call(soapMessage, new URL(url));


        ByteArrayOutputStream out = new ByteArrayOutputStream();
        response.writeTo(out);

        return out.toByteArray();
    }

    public static byte[] getBusinessEntityByInn(String inn) throws Exception{

        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection connection = soapConnectionFactory.createConnection();

        SOAPMessage soapMessage = createRequest();

        //  ~~~~~~~~~

        SOAPPart soapPart = soapMessage.getSOAPPart();
        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();

        SoapServicesUtil.setNamespaces(
                envelope,
                new ArrayList<Namespace>() {{
                    add(Constants.NS_BS);
                    add(NS_WS_BE);
                    add(NS_ENT);
                    add(NS_IKAR);
                }}
        );

        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        SOAPElement main = soapBody.addChildElement("getBusinessEntityListRequest", NS_WS_BE.getName())
                .addChildElement("businessEntity", NS_ENT.getName())
                .addChildElement("inn", NS_ENT.getName()).addTextNode(inn);

        soapMessage.saveChanges();
        //  ~~~~~~~~~
        SOAPMessage response = connection.call(soapMessage, new URL(url));


        ByteArrayOutputStream out = new ByteArrayOutputStream();
        response.writeTo(out);

        return out.toByteArray();
    }


    public static byte[] getEnterprise(String guid) throws Exception{

        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection connection = soapConnectionFactory.createConnection();

        SOAPMessage soapMessage = createRequest();

        //  ~~~~~~~~~

        SOAPPart soapPart = soapMessage.getSOAPPart();
        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();

        SoapServicesUtil.setNamespaces(
                envelope,
                new ArrayList<Namespace>() {{
                    add(Constants.NS_BS);
                    add(NS_WS_ENT);
                }}
        );

        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        SOAPElement main = soapBody.addChildElement("getEnterpriseByGuidRequest", NS_WS_ENT.getName())
                .addChildElement("guid", Constants.NS_BS.getName()).addTextNode(guid);

        soapMessage.saveChanges();
        //  ~~~~~~~~~
        SOAPMessage response = connection.call(soapMessage, new URL(url));


        ByteArrayOutputStream out = new ByteArrayOutputStream();
        response.writeTo(out);

        return out.toByteArray();
    }
}
