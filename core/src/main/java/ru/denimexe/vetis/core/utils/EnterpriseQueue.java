package ru.denimexe.vetis.core.utils;

import ru.denimexe.vetis.core.data.entities.Enterprise;

import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by denimexe on 22.09.17.
 */
public class EnterpriseQueue extends ConcurrentLinkedQueue<Enterprise> {
    @Override
    public String toString() {
        Iterator<Enterprise> it = iterator();
        if (! it.hasNext())
            return "<enterprises></enterprises>";

        StringBuilder sb = new StringBuilder();
        sb.append("<enterprises>");
        for (;;) {
            Enterprise ent = it.next();
            sb.append(ent instanceof Enterprise ? "<enterprise>" + ent + "</enterprise>" : "<somethingstrange/>");
            if (! it.hasNext())
                return sb.append("</enterprises>").toString();
        }
    }
}
