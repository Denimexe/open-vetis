package ru.denimexe.vetis.core.rest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.denimexe.vetis.core.data.entities.Notification;
import ru.denimexe.vetis.core.data.services.NotificationService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by denimexe on 19.10.17.
 */
@RestController
@RequestMapping(value = "rest/notifications")
public class NotificationRestController {

    private final Logger LOGGER = LogManager.getLogger(NotificationRestController.class);

    @Autowired
    private NotificationService notificationService;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<Notification> getNotificationList() {
        try {
            List<Notification> notifications = notificationService.findAll();
            return notifications;
        } catch (Exception ex){
            LOGGER.error("getNotificationList()", ex);
        }

        return new ArrayList<Notification>();
    }

    @RequestMapping(value = "/viewed/{id}", method = RequestMethod.GET)
    @ResponseBody
    public void setNotificationViewed(@PathVariable("id") int id) {
        Notification ntf = notificationService.findOne(id);
        if(ntf != null) {
            ntf.setViewed(true);
            notificationService.save(ntf);
        }

    }
}
