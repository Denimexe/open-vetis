package ru.denimexe.vetis.core.utils;

import org.apache.logging.log4j.LogManager;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ru.denimexe.vetis.core.data.entities.Address;
import ru.denimexe.vetis.core.data.entities.Enterprise;
import ru.denimexe.vetis.core.soap.WS_IkarService;

import java.util.HashMap;

/**
 * Created by denimexe on 13.09.17.
 */
public class EnterpriseParser {

    public static HashMap parse(byte[] bytes){

        try {
            Document doc = Jsoup.parse(new String(bytes, "UTF-8"));
            Element e = doc.select("ent|enterprise").first();
            if(e != null) {
                return parse(e);
            } else {
                e = doc.select("ent|businessEntity").first();
            }
        } catch ( Exception ex) {
            LogManager.getLogger("EnterpriseParser").error("parse()", ex);
        }

        return null;
    }

    public static HashMap<String, Object> parse(Element e){
        if(e == null)
            return null;

        try {
            HashMap<String, Object> map = new HashMap<>();
//            map.put("type", Constants.ENTERPRISE_TYPE);

            Enterprise ent = new Enterprise();

//        <ent:enterprise>
//            <bs:uuid>4f19141d-6178-41d6-a662-d972f7ef4c97</bs:uuid>
//            <bs:guid>8d2a70d4-b4fc-4a14-9f06-45f85c3b7c39</bs:guid>
//            <ent:name>ООО "Ритейл Центр "</ent:name>
//            <ent:address>
//                <ikar:region>
//                    <bs:uuid>1a8430ad-dcc9-43d9-81ea-948b90a25d8b</bs:uuid>
//                    <bs:guid>1ac46b49-3209-4814-b7bf-a509ea1aecd9</bs:guid>
//                    <ikar:name>Новосибирская область</ikar:name>
//                </ikar:region>
//            </ent:address>
//        </ent:enterprise>

            //id, guid, name, country_id, region_id, type_id, district_id, locality_id, address_view, businessEntity_id

            ent.setGuid(e.select("bs|guid").first().text().trim());
            ent.setName(e.select("ent|name").first().text().trim());
            ent.setAddressView(e.select("ikar|addressView").first().text().trim());

            Elements country = e.select("ikar|country");
            if (country.size() > 0) {
                map.put("country", country.first().select("bs|guid").first().text().trim());
            }

            Elements region = e.select("ikar|region");
            if (region.size() > 0) {
                map.put("region", region.first().select("bs|guid").first().text().trim());
            }

            Elements district = e.select("ikar|district");
            if (district.size() > 0) {
                if(district.first().select("bs|guid").size() > 0) {
                    map.put("district", district.first().select("bs|guid").first().text().trim());
                }
            }

            Elements locality = e.select("ikar|locality");
            if (locality.size() > 0) {
                if(locality.first().select("bs|guid").size() > 0) {
                    map.put("locality", locality.first().select("bs|guid").first().text().trim());
                }
            }

            map.put("type", e.select("ent|type").first().text().trim());
//        ent.setRegion();

            map.put("enterprise", ent);
            return map;
        } catch (Exception ex) {
            return null;
        }
    }

}