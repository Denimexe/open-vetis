package ru.denimexe.vetis.core.data.services;

import ru.denimexe.vetis.core.data.entities.BusinessEntity;
import ru.denimexe.vetis.core.data.entities.BusinessEntityType;

import java.util.List;

/**
 * Created by dirde on 15.11.2016.
 */
public interface BusinessEntityService {

    void save(BusinessEntity businessEntity);

    BusinessEntity findOne(Integer id);

    BusinessEntity findByGuid(String guid);

    List<BusinessEntity> findAll();

    BusinessEntity findByInn(String inn);

    BusinessEntity getFullOne(Integer id);

    BusinessEntity getFullByGuid(String guid);

    BusinessEntityType findType(Integer id);

    BusinessEntityType findTypeByName(String name);

    void saveType(BusinessEntityType type);

    List<BusinessEntityType> findTypes();

}
