package ru.denimexe.vetis.core.data.repositories;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;
import ru.denimexe.vetis.core.data.entities.Enterprise;

import java.util.List;

/**
 * Created by denimexe on 04.10.17.
 */
@Repository
public interface EnterpriseRepository extends JpaRepository<Enterprise, Integer>, QueryDslPredicateExecutor<Enterprise> {
    Enterprise findByGuid(String guid);

    List<Enterprise> findByIdIn(List<Integer> idList);

    /*
    @EntityGraph(value = "Enterprise.businessEntities", type = EntityGraph.EntityGraphType.LOAD)
    */
//    Enterprise findById(Integer id);

//    List<Enterprise> findByBusinessEntityId(Integer id);

}
