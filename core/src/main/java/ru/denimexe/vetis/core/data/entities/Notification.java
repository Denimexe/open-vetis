package ru.denimexe.vetis.core.data.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 * Created by denimexe on 29.09.17.
 */

@Data
@Entity
@AllArgsConstructor
@Table(name = "notification", schema = "vetis")
public class Notification implements Serializable{

    private static final long serialVersionUID = 1L;
    @Transient
    protected final SimpleDateFormat format = new SimpleDateFormat("dd.MM.yy, HH:mm");

    public Notification(){
        this.date = new Date();
    }

    public Notification(String message){
        this.date = new Date();
        this.message = message;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "message", nullable = false)
    private String message;

    private Date date;

    @Column(columnDefinition = "TINYINT", name = "viewed")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean viewed;

    @Override
    public String toString() {
        return "<id>" + id +
                "</id><message>" + message +
                "</message><date>" + format.format(date) +
                "</date><viewed>" + viewed +
                "</viewed>";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Notification that = (Notification) o;
        return viewed == that.viewed &&
                Objects.equals(format, that.format) &&
                Objects.equals(id, that.id) &&
                Objects.equals(message, that.message) &&
                Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), format, id, message, date, viewed);
    }
}