package ru.denimexe.vetis.core.data.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by denimexe on 03.10.17.
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ref_unit", schema = "vetis")
public class Unit  implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    private String name;
    private String guid;
    @Column(name = "full_name")
    private String fullName;

    @ManyToOne
    @JoinColumn (name = "ref_common_unit_id")
    private Unit commonUnit;

    private Integer factor;

    @Override
    public String toString() {
        return "<id>" + id +
                "</id><name>" + name +
                "</name><guid>" + guid +
                "</guid><fullName>" + fullName+
                "</fullName><commonUnitId>" + ((commonUnit != null) ? commonUnit.getId().toString() : "null") +
                "</commonUnitId><factor>" + factor +
                "</factor>";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Unit unit = (Unit) o;
        return Objects.equals(id, unit.id) &&
                Objects.equals(name, unit.name) &&
                Objects.equals(guid, unit.guid) &&
                Objects.equals(fullName, unit.fullName) &&
                Objects.equals(commonUnit, unit.commonUnit) &&
                Objects.equals(factor, unit.factor);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), id, name, guid, fullName, commonUnit, factor);
    }
}
