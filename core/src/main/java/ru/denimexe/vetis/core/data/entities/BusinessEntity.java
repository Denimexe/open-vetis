package ru.denimexe.vetis.core.data.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * Created by denimexe on 12.09.2017.
 */
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@NamedEntityGraph(name = "BusinessEntity.enterprises", attributeNodes = @NamedAttributeNode("enterprises"))
@Table(name = "business_entity", schema = "vetis")
public class BusinessEntity implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @NonNull private String guid;
    private String name;
    private String inn;
    private String kpp;
    private String ogrn;

    @Column(name = "address_view")
    private String addressView;

    @ManyToOne @JoinColumn(name = "ref_business_entity_type_id")
    private BusinessEntityType type;

    @ManyToOne @JoinColumn(name = "country_id")
    private Address country;
    @ManyToOne @JoinColumn(name = "region_id")
    private Address region;
    @ManyToOne @JoinColumn(name = "district_id")
    private Address district;
    @ManyToOne @JoinColumn(name = "locality_id")
    private Address locality;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinTable(name = "businessEntity_enterprise_link",
            joinColumns = @JoinColumn(name = "businessEntity_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "enterprise_id", referencedColumnName = "id")
    )
    private Set<Enterprise> enterprises = new HashSet<>();

    @Override
    public String toString() {
        return "<id>" + id +
                "</id><guid>" + guid +
                "</guid><name>" + name +
                "</name><inn>" + inn +
                "</inn><kpp>" + kpp +
                "</kpp><ogrn>" + ogrn +
                "</ogrn><addressView>" + addressView +
                "</addressView><type>" + type +
                "</type><country>" + country +
                "</country><region>" + region +
                "</region><district>" + district +
                "</district><locality>" + locality +
                "</locality>";
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (guid != null ? guid.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);

        return result;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BusinessEntity businessEntity = (BusinessEntity) o;
        return Objects.equals(id, businessEntity.id) &&
                Objects.equals(guid, businessEntity.guid) &&
                Objects.equals(name, businessEntity.name) &&
                Objects.equals(kpp, businessEntity.kpp) &&
                Objects.equals(ogrn, businessEntity.ogrn) &&
                Objects.equals(addressView, businessEntity.addressView) &&
                Objects.equals(country, businessEntity.country) &&
                Objects.equals(region, businessEntity.region) &&
                Objects.equals(district, businessEntity.district) &&
                Objects.equals(locality, businessEntity.locality) &&
                Objects.equals(type, businessEntity.type);
    }

    @Transient
    private boolean linked = false;
}
