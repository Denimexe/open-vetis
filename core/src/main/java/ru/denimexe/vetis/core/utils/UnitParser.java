package ru.denimexe.vetis.core.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.denimexe.vetis.core.data.entities.Unit;
import ru.denimexe.vetis.core.data.services.UnitService;
import ru.denimexe.vetis.core.soap.WS_DictionaryService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by denimexe on 13.09.17.
 */
@Service
public class UnitParser {
    private final Logger LOGGER = LogManager.getLogger("UnitParser");

    @Autowired
    private UnitService service;

    public List<Unit> parse(Elements elements){
        List<Unit> units = new ArrayList<>();

        try {
            for (Element e : elements) {
                Unit unit = parse(e);
                units.add(unit);
            }
        } catch (Exception ex) {
            LOGGER.error("parse()", ex);
        }

        return units;
    }


    public Unit parse(Element e){

        try {
            Unit unit = new Unit();

            String guid = e.select("bs|guid").first().text().trim();

            Unit db_unit = service.findByGuid(guid);
            if(db_unit != null) {
                return db_unit;
            }

            unit.setGuid(guid);
            unit.setName(e.select("com|name").first().text().trim());
            unit.setFullName(e.select("com|fullName").first().text().trim());
            int factor = Integer.valueOf(e.select("com|factor").first().text().trim());
            unit.setFactor(factor);
//            unit.setFactor(Integer.valueOf(e.select("com|factor").first().text().trim()));

            //todo get unit by guid or fetch a new one
            //unit.setCommonUnit(e.select("bs|fullname").first().text().trim());
            Elements commonGuidElement = e.select("com|commonUnitGuid");
            if(commonGuidElement != null && commonGuidElement.size() == 1) {
                String commonGuid = commonGuidElement.first().text().trim();

                if (commonGuid != null && !"".equals(commonGuid) && !guid.equals(commonGuid)) {
                    Unit newUnit;
                    newUnit = service.findByGuid(commonGuid);

                    //add new if nothing in DB
                    if (newUnit == null) {
                        byte[] data = WS_DictionaryService.getUnit(commonGuid);
                        Document doc = Jsoup.parse(new String(data, "UTF-8"));
                        newUnit = parse(doc.select("com|unit").first());
                        LOGGER.info(unit);
                        //service.save(newUnit);
                        service.save(newUnit);
                    }

                    unit.setCommonUnit(newUnit);
                }
            }


            service.save(unit);

            return unit;
        } catch (Exception ex) {
            LOGGER.error("parse()", ex);
            return null;
        }


    }

}