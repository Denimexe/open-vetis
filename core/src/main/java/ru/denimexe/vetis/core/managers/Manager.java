package ru.denimexe.vetis.core.managers;

/**
 * Created by denimexe on 12.10.17.
 */
public interface Manager {
    void uploadUnits();

    java.util.List<ru.denimexe.vetis.core.data.entities.Unit> getUnits();
}
