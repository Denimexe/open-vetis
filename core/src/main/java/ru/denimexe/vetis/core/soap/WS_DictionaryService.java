package ru.denimexe.vetis.core.soap;

import ru.denimexe.vetis.core.utils.Constants;
import ru.denimexe.vetis.core.utils.Namespace;
import ru.denimexe.vetis.core.utils.SoapServicesUtil;
import ru.denimexe.vetis.core.utils.SoapUtils;

import javax.xml.soap.*;
import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.util.ArrayList;

import static ru.denimexe.vetis.core.utils.SoapUtils.createRequest;

/**
 * Created by denimexe on 25.09.17.
 */
public class WS_DictionaryService {

    private static final String url = "https://api.vetrf.ru/platform/services/DictionaryService";

    public static final Namespace NS_WS = new Namespace("WS", "http://api.vetrf.ru/schema/cdm/argus/common/ws-definitions");
    public static final Namespace NS_COM = new Namespace("BS", "http://api.vetrf.ru/schema/cdm/argus/common");

    public static byte[] getUnits() throws Exception{

        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection connection = soapConnectionFactory.createConnection();

        SOAPMessage soapMessage = createRequest();

        //  ~~~~~~~~~

        SOAPPart soapPart = soapMessage.getSOAPPart();
        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();

        SoapServicesUtil.setNamespaces(
                envelope,
                new ArrayList<Namespace>() {{ add(NS_WS); }}
        );

        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        SOAPElement main = soapBody.addChildElement("getUnitListRequest", NS_WS.getName());
        soapMessage.saveChanges();
        //  ~~~~~~~~~
        SOAPMessage response = connection.call(soapMessage, new URL(url));


        ByteArrayOutputStream out = new ByteArrayOutputStream();
        response.writeTo(out);

        return out.toByteArray();
    }

    public static byte[] getUnit(String guid) throws Exception{

        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection connection = soapConnectionFactory.createConnection();

        SOAPMessage soapMessage = createRequest();

        //  ~~~~~~~~~

        SOAPPart soapPart = soapMessage.getSOAPPart();
        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();

        SoapServicesUtil.setNamespaces(
                envelope,
                new ArrayList<Namespace>() {{
                    add(NS_WS);
                    add(Constants.NS_BS);
                }}
        );

        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        SOAPElement main = soapBody
                .addChildElement("getUnitByGuidRequest", NS_WS.getName())
                .addChildElement("guid", Constants.NS_BS.getName()).addTextNode(guid);
        soapMessage.saveChanges();
        //  ~~~~~~~~~
        SOAPMessage response = connection.call(soapMessage, new URL(url));


        ByteArrayOutputStream out = new ByteArrayOutputStream();
        response.writeTo(out);

        return out.toByteArray();
    }
}
