package ru.denimexe.vetis.core.utils;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.json.JsonObject;

/**
 * Created by denimexe on 12.09.2017.
 */
public class Connector {

    private final Logger LOGGER = LogManager.getLogger(Connector.class);

    private HttpClient client;
    private String sessionId;
    private String execution;

    public Connector() {
        client = HttpClientBuilder.create().disableContentCompression().disableAutomaticRetries().evictExpiredConnections().build();
    }

    public JsonObject process(){
        try {
//            getMainPage();
//
//            int page = 1;
//
//            JsonArrayBuilder ar = Json.createArrayBuilder();
//
//            ar.add(XMLParser.parse(getFirstPage()));
//
//            int amount = getAmount();
//            do {
//                //System.out.println(page*100 + " / " + amount + " [" + String.format("%.1f", page*10000.0/amount) + "%]");
//                page++;
//                ar.add(XMLParser.parse(getNextPage(page)));
//            } while (page * 100 < amount);
//
//            return Json.createObjectBuilder().add("pages", ar.build()).build();

        } catch (Exception ex){
            LOGGER.error("process() ", ex);
        }

        return null;
    }
}
