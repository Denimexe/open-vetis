package ru.denimexe.vetis.core.data.services;

import ru.denimexe.vetis.core.data.entities.DocumentForm;

import java.util.List;

/**
 * Created by dirde on 15.11.2016.
 */
public interface DocumentFormService {

    void save(DocumentForm documentForm);

    DocumentForm findOne(Integer id);

    List<DocumentForm> findAll();

    DocumentForm findByName(String name);
}
