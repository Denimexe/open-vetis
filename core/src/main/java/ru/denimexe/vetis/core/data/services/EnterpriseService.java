package ru.denimexe.vetis.core.data.services;

import ru.denimexe.vetis.core.data.entities.Enterprise;
import ru.denimexe.vetis.core.data.entities.EnterpriseType;

import java.util.List;

/**
 * Created by dirde on 15.11.2016.
 */
public interface EnterpriseService {

    void save(Enterprise enterprise);

    Enterprise findOne(Integer id);

    Enterprise findByGuid(String guid);

    List<Enterprise> findAll();

    List<Enterprise> findByIdIn(List<Integer> idList);

    void saveType(EnterpriseType type);

    EnterpriseType findType(Integer id);

    List<EnterpriseType> findTypes();

    EnterpriseType findTypeByName(String name);

}
