package ru.denimexe.vetis.core.utils;

import ru.denimexe.vetis.core.data.entities.Document;

import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by denimexe on 22.09.17.
 */
public class DocumentQueue extends ConcurrentLinkedQueue<Document> {
    @Override
    public String toString() {
        Iterator<Document> it = iterator();
        if (! it.hasNext())
            return "<documents></documents>";

        StringBuilder sb = new StringBuilder();
        sb.append("<documents>");
        for (;;) {
            Document doc = it.next();
            sb.append(doc instanceof Document ? "<document>" + doc + "</document>" : "<somethingstrange/>");
            if (! it.hasNext())
                return sb.append("</documents>").toString();
        }
    }
}
