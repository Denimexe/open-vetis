package ru.denimexe.vetis.core.utils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by denimexe on 13.09.17.
 */
public class DocumentParser {

    public static HashMap<String, Object> parse(byte[] bytes){

        HashMap<String, Object> map = new HashMap<>();
        org.jsoup.nodes.Document doc;
        Element e;

        try {
            doc = Jsoup.parse(new String(bytes, "UTF-8"));
            e = doc.select("application").first();
        } catch (Exception ex) {
            return null;
        }

        String status = e.select("status").text().trim();

        boolean stop = true;

        switch (status) {
            case "REJECTED":
                map.put("status", -1);
                map.put("errorMessage",
                        e.select("errors").first().select("apl|error")
                                .first().text().trim());
                break;
            case "ACCEPTED": case "IN_PROCESS":
                map.put("status", 0);
                break;
            case "COMPLETED":
                map.put("status", 1);
                stop = false;
        }

        if(stop) {
            return map;
        }

        Elements documents = e.select("ns2|vetDocument");
        map.put("totalAmount", e.select("ns2|vetDocumentList").first().attributes().get("total"));

        ArrayList<HashMap> resultList = new ArrayList<>();

        for(Element el: documents) {
            HashMap<String, Object> docMap = new HashMap<>();
            
            docMap.put("uuid", el.select("bs|uuid").first().text().trim());
            docMap.put("form", el.select("ns2|form").text().trim());
            docMap.put("type", el.select("ns2|type").text().trim());
            docMap.put("status", el.select("ns2|status").text().trim());
            
            Element issDate =  el.select("ns2|issueDate").first();
            Element issNum =  el.select("ns2|issueNumber").first();
            Element issSer =  el.select("ns2|issueSeries").first();

            docMap.put("issueDate", issDate != null ? issDate.text().trim() : "");
            docMap.put("issueNumber", issNum != null ? issNum.text().trim() : "");
            docMap.put("issueSeries", issSer != null ? issSer.text().trim() : "");

            Elements consignor = el.select("ns2|consignor");

            if (consignor.size() != 0) {
                HashMap<String, String> consignorMap = new HashMap<>();

                consignorMap.put("businessEntity", consignor.select("ent|businessEntity").select("bs|guid").text().trim());
                consignorMap.put("enterprise", consignor.select("ent|enterprise").select("bs|guid").text().trim());

                docMap.put("consignor", consignorMap);
            }

            Elements consignee = el.select("ns2|consignee");

            if (consignee.size() != 0) {
                HashMap<String, String> consigneeMap = new HashMap<>();

                consigneeMap.put("businessEntity", consignee.select("ent|businessEntity").select("bs|guid").text().trim());
                consigneeMap.put("enterprise", consignee.select("ent|enterprise").select("bs|guid").text().trim());

                docMap.put("consignee", consigneeMap);
            }

            Elements batch = el.select("ns2|batch");

            HashMap<String, String> batchMap = new HashMap<>();


            batchMap.put("productType", batch.select("ns2|productType").text().trim());

            batchMap.put("product", batch.select("ns2|product").first().select("bs|guid").first().text());
            batchMap.put("subProduct", batch.select("ns2|subProduct").first().select("bs|guid").first().text());
            batchMap.put("productItem", batch.select("ns2|productItem").select("prod|name").first().text().trim());
            batchMap.put("volume", batch.select("ns2|volume").first().text().trim());
            batchMap.put("unit", batch.select("ns2|unit").select("bs|guid").first().text().trim());
            batchMap.put("country", batch.select("ns2|countryOfOrigin").select("bs|guid").first().text().trim());


            batchMap.put("dateOfProduction", batch.select("ns2|dateOfProduction").first().text().trim());
            batchMap.put("expiryDate", batch.select("ns2|expiryDate").first().text().trim());

            docMap.put("batch", batchMap);

            docMap.put("expert", el.select("ns2|confirmedBy").first().select("argc|fio").first().text().trim());

            resultList.add(docMap);

        }
        map.put("result", resultList);

        return map;
    }

}