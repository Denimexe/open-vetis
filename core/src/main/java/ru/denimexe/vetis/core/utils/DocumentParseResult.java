package ru.denimexe.vetis.core.utils;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by denimexe on 19.03.18.
 */
@Getter
@Setter
public class DocumentParseResult {
    private boolean success = false;
    byte[] bytes;
}
