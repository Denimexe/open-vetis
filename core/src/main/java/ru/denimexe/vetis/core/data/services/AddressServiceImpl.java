package ru.denimexe.vetis.core.data.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.denimexe.vetis.core.data.entities.Address;
import ru.denimexe.vetis.core.data.entities.AddressType;
import ru.denimexe.vetis.core.data.repositories.AddressRepository;
import ru.denimexe.vetis.core.data.repositories.AddressTypeRepository;

import java.util.List;

/**
 * Created by denimexe on 10.10.17.
 */
@Service
public class AddressServiceImpl implements AddressService {
    private final Logger LOGGER = LogManager.getLogger(AddressServiceImpl.class);

    @Autowired
    private AddressTypeRepository typeRepository;

    @Autowired
    private AddressRepository repository;

    @Override
    public void save(Address address) { repository.save(address); }

    @Override
    public Address findOne(Integer id) { return repository.findOne(id); }

    @Override
    public List<Address> findAll() { return repository.findAll(); }

    @Override
    public Address findByGuid(String guid) { return repository.findByGuid(guid); }

    @Override
    public List<Address> findByTypeId(Integer id){ return repository.findByTypeId(id); }

    @Override
    public List<Address> findByNameContaining(String name){ return repository.findByNameContaining(name); }

    @Override
    public List<Address> findByParentGuid(String parent) { return repository.findByParentGuid(parent); }

    @Override
    public List<Address> findByType(AddressType type) { return repository.findByType(type); }

    @Override
    public List<Address> findByTypeAndParentGuid(AddressType type, String parentGuid) {
        return repository.findByTypeAndParentGuid(type, parentGuid);
    }

    @Override
    public void saveType(AddressType type) {
        typeRepository.save(type);
    }

    @Override
    public AddressType findType(Integer id) {
        return typeRepository.findOne(id);
    }

    @Override
    public AddressType findTypeByName(String name) {
        return typeRepository.findByName(name);
    }

    @Override
    public List<AddressType> findTypes() {
        return typeRepository.findAll();
    }
}
