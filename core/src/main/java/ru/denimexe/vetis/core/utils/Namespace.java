package ru.denimexe.vetis.core.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by denimexe on 11.10.17.
 */
@Getter
@AllArgsConstructor
public class Namespace {
    private String name;
    private String url;
}
