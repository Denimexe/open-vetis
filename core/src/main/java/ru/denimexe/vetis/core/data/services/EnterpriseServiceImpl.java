package ru.denimexe.vetis.core.data.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.denimexe.vetis.core.data.entities.Enterprise;
import ru.denimexe.vetis.core.data.entities.EnterpriseType;
import ru.denimexe.vetis.core.data.repositories.EnterpriseRepository;
import ru.denimexe.vetis.core.data.repositories.EnterpriseTypeRepository;

import java.util.List;

/**
 * Created by denimexe on 12.10.17.
 */
@Service
public class EnterpriseServiceImpl implements EnterpriseService {

    @Autowired
    private EnterpriseRepository repository;

    @Autowired
    private EnterpriseTypeRepository typeRepository;


    @Override
    public void save(Enterprise enterprise) {
        try {
            repository.save(enterprise);
        } catch ( Exception ex) {
        }
    }

    @Override
    public Enterprise findOne(Integer id) {
        try {
            return repository.findOne(id);
        } catch ( Exception ex) {
            return null;
        }
    }

    @Override
    public List<Enterprise> findAll() {
        try {
        return repository.findAll();
        } catch ( Exception ex) {
            return null;
        }
    }

    @Override
    public Enterprise findByGuid(String guid) {
        try {
            return repository.findByGuid(guid);
        } catch ( Exception ex) {
            return null;
        }
    }

    @Override
    public List<Enterprise> findByIdIn(List<Integer> idList) {
        try {
            return repository.findByIdIn(idList);
        } catch ( Exception ex) {
            return null;
        }
    }

    @Override
    public void saveType(EnterpriseType enterpriseType) {
        typeRepository.save(enterpriseType);
    }

    @Override
    public EnterpriseType findType(Integer id) {
        return typeRepository.findOne(id);
    }

    @Override
    public List<EnterpriseType> findTypes() {
        return typeRepository.findAll();
    }

    @Override
    public EnterpriseType findTypeByName(String name) {
        return typeRepository.findByName(name);
    }
/*
    @Override
    public Enterprise getFullOne(Integer id) {
        return null;
        //return repository.findById(id);
    }
    */

    //    @Override
//    public List<Enterprise> findByBusinessEntityId(Integer id) {
//        try {
//            return repository.findByBusinessEntityId(id);
//        } catch ( Exception ex) {
//            return null;
//        }
//    }
}
