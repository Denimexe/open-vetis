package ru.denimexe.vetis.core.runner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.denimexe.vetis.core.soap.WS_ApplicationManagementService;
import ru.denimexe.vetis.core.sync.beans.DocumentBean;
import org.springframework.jms.annotation.EnableJms;

import java.util.Arrays;
import java.util.List;

/**
 * Created by denimexe on 12.09.17.
 */

@EnableJms
public class Initializer {
    private static final Logger LOGGER;

    static {
        LOGGER = LogManager.getLogger(Initializer.class);
    }

    public static void main(String[] args){
//        fetch();
//        doTheJob();

//        ConstantListManager constantListManager = new ConstantListManager();
//        System.out.println(constantListManager.getDocumentFormList());
//        Runner runner = new Runner();
//        runner.run();
        try {
            String appConfigLocation = "/META-INF/spring/core-context.xml";


//            GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
//            ctx.load(appConfigLocation);
//            ctx.refresh();

            ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(appConfigLocation);

            Runner bean = ctx.getBean(Runner.class);
            bean.invokeMethod();

            // get bean from context
//            CoreJMSSender coreJMSSender = (CoreJMSSender)ctx.getBean("coreJMSSender");
//
//            // send to default destination
//            coreJMSSender.send("Hi, core!");

//            // send to a code specified destination
//            Queue queue = new ActiveMQQueue("web");
//            jmsMessageSender.send(queue, "hello Another Message");

            // close spring application context
            ctx.close();

        } catch (Exception ex){
            LOGGER.error(ex);
        }


    }

    private static void fetch(){
        List<String> guids = Arrays.asList(new String[] {
                "1ac46b49-3209-4814-b7bf-a509ea1aecd9", //НСО
                "8276c6a1-1a86-4f0d-8920-aba34d4cc34a", //Алтайский край
                "5c48611f-5de6-4771-9695-7e36a4e7529d", //республика Алтай
                "889b1f3a-98aa-40fc-9d3d-0f41192758ab", //Томская обл
                "393aeccb-89ef-4a7e-ae42-08d5cebc2e30", //Кемеровская обл
                "db9c4f8b-b706-40e2-b2b4-d31b98dcd3d1", //Красноярский край
                "8d3f1d35-f0f4-41b5-b5b7-e7cadf3e7bd7", //республика Хакасия
                "c225d3db-1db6-4063-ace0-b3fe9ea3805f"  //Саха
        });

        ListFetcher.getEnterprisesByRegion(guids);
    }

    private static void doTheJob(){

        DocumentBean db = new DocumentBean();

        byte[] data;
        try {
            int counter = 1;
            data = new byte[] {};

//            do {
            data = WS_ApplicationManagementService.getApplicationsPage("8bb570b2-5c66-4c37-8e0a-cd6da03d5daf");
//            data = (Files.readAllBytes(Paths.get("/Users/denimexe/home/toParse1")));
            Document doc = Jsoup.parse(new String(data, "UTF-8"));
            if(doc.select("env|fault").size() == 0) {
                db.addData(doc);
            } else {
                //System.out.println();
                LOGGER.error("error during getting data:\n" + doc.select("env|fault").text());
            }
//                counter++;
//            } while(data.length > 0);

            System.out.println(db.getQueue());
//            System.out.println(db.getStat());

//            System.out.println(WS_EnterpriseService.getRegion("0fa48faa-7995-480c-ab48-4e8ca42f5162"));

        } catch (Exception ex){
            LOGGER.error("doTheJob()", ex);
        }
    }
}
