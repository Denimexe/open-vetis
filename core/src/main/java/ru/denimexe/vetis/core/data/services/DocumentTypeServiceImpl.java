package ru.denimexe.vetis.core.data.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.denimexe.vetis.core.data.entities.DocumentType;
import ru.denimexe.vetis.core.data.repositories.DocumentTypeRepository;

import java.util.List;

/**
 * Created by denimexe on 12.10.17.
 */
@Service
public class DocumentTypeServiceImpl implements DocumentTypeService {

    @Autowired
    private DocumentTypeRepository repository;

    @Override
    public void save(DocumentType documentType) {
        repository.save(documentType);
    }

    @Override
    public DocumentType findOne(Integer id) {
        return repository.findOne(id);
    }

    @Override
    public List<DocumentType> findAll() {
        return repository.findAll();
    }

    @Override
    public DocumentType findByName(String name) {
        return repository.findByName(name);
    }
}
