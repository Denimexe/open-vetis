package ru.denimexe.vetis.core.utils;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileOutputStream;
import java.util.*;

/**
 * Created by denimexe on 12.09.2017.
 */
public class XLSCreator {


    public static String CreateEnterpriseList(Document doc, String filename) {

        try {

            if(doc == null){
                return null;
            }

            HSSFWorkbook workbook = new HSSFWorkbook();
            int sheetcount = 1;
            HSSFSheet sheet = workbook.createSheet("Enterprises" + sheetcount++);

            Map<String, Object[]> data = new HashMap<String, Object[]>();
//            data.put("1", new Object[]{
//                    "название",
//                    "guid",
//                    "регион",
//            });

            //JsonArray elements = jsonObject.getJsonArray("elements");

            int counter = 1;

            //Document doc = Jsoup.parse(new String(bytes, "UTF-8"));
            Elements elements = doc.select("guid");

//            for(int i=0; i<elements.size(); i++) {
            for(Element e: elements){
//                Element e = elements.get(i);
                data.put(String.valueOf(counter),
                        new Object[]{
                                e.select("guid").text().trim()//,
//                                e.select("guid").text().trim(),
//                                e.select("regionName").text().trim()
                        }
                );

                counter++;
            }

            Set<String> keyset = data.keySet();
            int rownum = 0;

            for (int i = 1; i <= counter - 1; i++) {
                Row row = sheet.createRow(rownum++);

                Object[] objArr = data.get(String.valueOf(i));
                int cellnum = 0;
                for (Object obj : objArr) {
                    Cell cell = row.createCell(cellnum++);
                    //                    if (obj instanceof Date)
                    //                        cell.setCellValue((Date) obj);
                    //                    else if (obj instanceof Boolean)
                    //                        cell.setCellValue((Boolean) obj);
                    //                    else
                    if (obj instanceof String)
                        cell.setCellValue((String) obj);
                        //                    else if (obj instanceof Double)
                        //                        cell.setCellValue((Double) obj);
                    else if (obj instanceof Integer)
                        cell.setCellValue((Integer) obj);
                }
//                if(rownum%65535 == 0 && rownum > 0){
//                    for(int j = 0; j<4; j++){
//                        sheet.autoSizeColumn(j, true);
//                    }
//
//                    sheet = workbook.createSheet("Enterprises" + sheetcount++);
//                    rownum = 0;
//                }
            }

//            for(int i = 0; i<4; i++){
//                sheet.autoSizeColumn(i, true);
//            }

            try {
                FileOutputStream out =
                        new FileOutputStream(new File("/Users/denimexe/home/" + filename + ".xls"));
                workbook.write(out);
                out.close();

                return "/Users/denimexe/home/" + filename + ".xls";

            } catch (Exception ex) {
                ex.printStackTrace();
            }

        } catch (Exception ex) {
//            System.out.println("Create Exception: " + ex.toString());
        }
        return null;
    }


/*
    public static String CreateEnterpriseList(Document doc, String filename) {

        try {

            if(doc == null){
                return null;
            }

            HSSFWorkbook workbook = new HSSFWorkbook();
            int sheetcount = 1;
            HSSFSheet sheet = workbook.createSheet("Enterprises" + sheetcount++);

            Map<String, Object[]> data = new HashMap<String, Object[]>();
            data.put("1", new Object[]{
                    "название",
                    "ИНН",
                    "КПП",
                    "guid",
                    "адрес",
                    "ХС",
                    "ХС_guid",
                    "регион"
            });

            int counter = 1;
            int progress = 1;

            Elements elements = doc.select("be");

            int size = elements.size();

            for(Element e: elements){

                System.out.println(progress++ + "/" + size);

                String origin_inn = e.select("inn").text().trim();
                String origin_kpp = e.select("kpp").text().trim();
                String origin_guid = e.select("guid").first().text().trim();
                String origin_name = e.select("name").first().text().trim();
                String origin_region = e.select("region").text().trim();

                Elements arr = e.select("enterprise");

                int inner_progress = 1;
                int inner_size = arr.size();

                System.out.print("\tL\n");

                for(Element ei: arr){

                    System.out.println("\t\t" + inner_progress++ + "/" + inner_size);

                    data.put(String.valueOf(++counter), new Object[] {
                            ei.select("name").text().trim(),
                            origin_inn,
                            origin_kpp,
                            ei.select("guid").text().trim(),
                            ei.select("address").text().trim(),
                            origin_name,
                            origin_guid,
                            origin_region
                    });
                }
            }

            int rownum = 0;

            for (int i = 1; i <= data.size() - 1; i++) {
                Row row = sheet.createRow(rownum++);

                Object[] objArr = data.get(String.valueOf(i));
                int cellnum = 0;
                for (Object obj : objArr) {
                    Cell cell = row.createCell(cellnum++);
                    //                    if (obj instanceof Date)
                    //                        cell.setCellValue((Date) obj);
                    //                    else if (obj instanceof Boolean)
                    //                        cell.setCellValue((Boolean) obj);
                    //                    else
                    if (obj instanceof String)
                        cell.setCellValue((String) obj);
                        //                    else if (obj instanceof Double)
                        //                        cell.setCellValue((Double) obj);
                    else if (obj instanceof Integer)
                        cell.setCellValue((Integer) obj);
                }
                if(rownum%65535 == 0 && rownum > 0){
                    for(int j = 0; j<data.get("1").length; j++){
                        sheet.autoSizeColumn(j, true);
                    }

                    sheet = workbook.createSheet("Enterprises" + sheetcount++);
                    rownum = 0;
                }
            }

            for(int i = 0; i<data.get("1").length; i++){
                sheet.autoSizeColumn(i, true);
            }

            try {
                FileOutputStream out =
                        new FileOutputStream(new File("/Users/denimexe/home/" + filename + ".xls"));
                workbook.write(out);
                out.close();

                return "/Users/denimexe/home/" + filename + ".xls";

            } catch (Exception ex) {
                ex.printStackTrace();
            }

        } catch (Exception ex) {
//            System.out.println("Create Exception: " + ex.toString());
            LogManager.getLogger("XLSCreator").info(ex);
        }
        return null;
    }
*/
}
