package ru.denimexe.vetis.core.data.services;

import ru.denimexe.vetis.core.data.entities.EnterpriseType;

import java.util.List;

/**
 * Created by dirde on 15.11.2016.
 */
public interface EnterpriseTypeService{

    void save(EnterpriseType enterpriseType);

    EnterpriseType findOne(Integer id);

    List<EnterpriseType> findAll();

    EnterpriseType findByName(String name);
}
