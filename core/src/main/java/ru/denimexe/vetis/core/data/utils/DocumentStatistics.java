package ru.denimexe.vetis.core.data.utils;

import lombok.Data;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.denimexe.vetis.core.data.entities.Document;
import ru.denimexe.vetis.core.utils.Constants;

/**
 * Created by denimexe on 15.09.17.
 */
@Data
public class DocumentStatistics{

    private final Logger LOGGER = LogManager.getLogger(DocumentStatistics.class);

    private int inner           =   0;
    private int outer           =   0;

    private int milkRaw         =   0;
    private int milkProd        =   0;

    private int inner_utilized  =   0;
    private int incoming  =   0;

    private void addMilkRaw(int count){ milkRaw += count > 0 ? count : 0; }
    private void addMilkProd(int count){ milkProd += count > 0 ? count : 0; }
    private void addInner(int count){ inner += count > 0 ? count : 0; }
    private void addOuter(int count){ outer += count > 0 ? count : 0; }
    private void addInnerUtilized(int count){ inner_utilized += count > 0 ? count : 0; }
    private void addIncoming(int count){ incoming += count > 0 ? count : 0; }


    public synchronized void put(Document doc){
        try {
//        if(doc.isMilkProd())
//            addMilkProd(1);
//        if(doc.isMilkRaw())
//            addMilkRaw(1);

//        Endpoint consignor = doc.getConsignor();
//        Endpoint consignee = doc.getConsignor();
//        if(consignee != null && consignor != null) {
//            if (consignor.getRegionGuid().equals(consignee.getRegionGuid()) && consignor.getRegionGuid().equals(Constants.NSO_GUID)){
//                addInner(1);
//            }
//        }
        } catch (Exception ex) {
            LOGGER.error("put() ", ex);
        }

    }

}
