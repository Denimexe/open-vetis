package ru.denimexe.vetis.core.rest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.denimexe.vetis.core.data.entities.Unit;
import ru.denimexe.vetis.core.data.repositories.UnitRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anton Anikin on 30.06.2017.
 */
@RestController
public class DictionaryRestController {
    private final Logger LOGGER = LogManager.getLogger(DictionaryRestController.class);

    @Autowired
    private UnitRepository repository;

    @RequestMapping(value="rest/units", method = RequestMethod.GET)
    @ResponseBody
    public List<Unit> getAddressTypeList() {
        try {
            return repository.findAll();
        } catch (Exception ex){
            LOGGER.error("getAddressTypeList()", ex);
        }

        return new ArrayList<>();
    }

}
