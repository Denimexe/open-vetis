package ru.denimexe.vetis.core.data.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.denimexe.vetis.core.data.entities.SubProduct;
import ru.denimexe.vetis.core.data.repositories.SubProductRepository;

import java.util.List;

/**
 * Created by denimexe on 12.10.17.
 */
@Service
public class SubProductServiceImpl implements SubProductService{

    @Autowired
    private SubProductRepository repository;

    @Override
    public void save(SubProduct subProduct) {
        repository.save(subProduct);
    }

    @Override
    public SubProduct findOne(Integer id) {
        return repository.findOne(id);
    }

    @Override
    public SubProduct findByGuid(String guid) {
        return repository.findByGuid(guid);
    }

    @Override
    public List<SubProduct> findAll() {
        return repository.findAll();
    }
}
