package ru.denimexe.vetis.core.data.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.denimexe.vetis.core.data.entities.Unit;
import ru.denimexe.vetis.core.data.repositories.UnitRepository;

import java.util.List;

/**
 * Created by denimexe on 12.10.17.
 */
@Service
public class UnitServiceImpl implements UnitService {

    private final Logger LOGGER = LogManager.getLogger(UnitService.class);

    @Autowired
    private UnitRepository repository;

    @Override
    public void save(Unit unit) {
        repository.save(unit);
    }

    @Override
    public Unit findOne(Integer id) {
        return repository.findOne(id);
    }

    @Override
    public List<Unit> findAll() {
        return repository.findAll();
    }

    public Unit findByGuid(String guid) {
        return repository.findByGuid(guid);
    }
}
