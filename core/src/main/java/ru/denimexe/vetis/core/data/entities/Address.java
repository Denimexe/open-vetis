package ru.denimexe.vetis.core.data.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by denimexe on 29.09.17.
 */

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "address", schema = "vetis")
//@ToString(exclude = {"region",  "area", "settlement", "building",  "cityFias", "streetFias"})
public class Address implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "guid", nullable = false)
    private String guid;

    @Column(name = "name", nullable = true)
    private String name;

    @Column(name = "shortname", nullable = true)
    private String shortname;

    @Column(name = "parent_guid", nullable = true)
    private String parentGuid;

    @ManyToOne
    @JoinColumn(name = "ref_address_type_id", nullable = true)
    private AddressType type;

    public Address (String guid, String name, String shortname, String parentGuid, AddressType type) {
        this.guid = guid;
        this.name = name;
        this.shortname = shortname;
        this.parentGuid = parentGuid;
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Address address = (Address) o;
        return Objects.equals(id, address.id) &&
                Objects.equals(guid, address.guid) &&
                Objects.equals(name, address.name) &&
                Objects.equals(shortname, address.shortname) &&
                Objects.equals(parentGuid, address.parentGuid) &&
                Objects.equals(type, address.type);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), id, guid, name, shortname, parentGuid, type);
    }

    @Override
    public String toString() {
        return "<id>" + id +
                "</id><guid>" + guid +
                "</guid><name>" + name +
                "</name><parentGuid>" + parentGuid +
                "</parentGuid><type>" + type +
                "</type>";
    }
}