package ru.denimexe.vetis.core.data.repositories;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;
import ru.denimexe.vetis.core.data.entities.BusinessEntity;

import java.util.List;

/**
 * Created by denimexe on 04.10.17.
 */
@Repository
public interface BusinessEntityRepository extends JpaRepository<BusinessEntity, Integer>, QueryDslPredicateExecutor<BusinessEntity> {
    BusinessEntity findByGuid(String guid);
    BusinessEntity findByInn(String inn);

    @EntityGraph(value = "BusinessEntity.enterprises", type = EntityGraph.EntityGraphType.LOAD)
    BusinessEntity findById(Integer id);

    @EntityGraph(value = "BusinessEntity.enterprises", type = EntityGraph.EntityGraphType.LOAD)
    BusinessEntity findByGuidOrderByIdAsc(String guid);
}
