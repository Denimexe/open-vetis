package ru.denimexe.vetis.core.data.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.denimexe.vetis.core.data.entities.BusinessEntity;
import ru.denimexe.vetis.core.data.entities.BusinessEntityType;
import ru.denimexe.vetis.core.data.repositories.BusinessEntityRepository;
import ru.denimexe.vetis.core.data.repositories.BusinessEntityTypeRepository;

import java.util.List;

/**
 * Created by denimexe on 12.10.17.
 */
@Service
public class BusinessEntityServiceImpl implements BusinessEntityService {

    @Autowired
    private BusinessEntityRepository repository;

    @Autowired
    private BusinessEntityTypeRepository typeRepository;

    @Override
    public void save(BusinessEntity businessEntity) {
        repository.save(businessEntity);
    }

    @Override
    public BusinessEntity findOne(Integer id) {
        return repository.findOne(id);
    }

    @Override
    public BusinessEntity findByInn(String inn) { return repository.findByInn(inn); }

    @Override
    public List<BusinessEntity> findAll() {
        return repository.findAll();
    }

    @Override
    public BusinessEntity findByGuid(String guid) { return repository.findByGuid(guid); }

    public BusinessEntity getFullOne(Integer id) { return repository.findById(id);}

    public BusinessEntity getFullByGuid(String guid) { return repository.findByGuidOrderByIdAsc(guid); }

    @Override
    public void saveType(BusinessEntityType businessEntityType) {
        typeRepository.save(businessEntityType);
    }

    @Override
    public BusinessEntityType findType(Integer id) {
        return typeRepository.findOne(id);
    }

    @Override
    public List<BusinessEntityType> findTypes() {
        return typeRepository.findAll();
    }

    @Override
    public BusinessEntityType findTypeByName(String name) {
        return typeRepository.findByName(name);
    }
}
