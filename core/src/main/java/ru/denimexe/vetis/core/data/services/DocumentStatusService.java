package ru.denimexe.vetis.core.data.services;

import ru.denimexe.vetis.core.data.entities.DocumentStatus;

import java.util.List;

/**
 * Created by dirde on 15.11.2016.
 */
public interface DocumentStatusService {

    void save(DocumentStatus documentStatus);

    DocumentStatus findOne(Integer id);

    List<DocumentStatus> findAll();

    DocumentStatus findByName(String name);
}
