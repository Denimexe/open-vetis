package ru.denimexe.vetis.core.utils;

/**
 * Created by denimexe on 15.09.17.
 */
public final class Constants {
    public static final String NSO_GUID = "1ac46b49-3209-4814-b7bf-a509ea1aecd9";
    public static final String RUSSIA_GUID = "74a3cbb1-56fa-94f3-ab3f-e8db4940d96b";

    public static final int REQUEST_COUNT = 1000;
    public static final int BUFFER_SIZE = 125;


    public static final String API_KEY = "***";
    public static final String ISSUER_ID = "***";
    public static final String SERVICE_ID = "mercury-vu.service";
    public static final String LOGIN = "***";

    public static final Namespace NS_BS = new Namespace("base", "http://api.vetrf.ru/schema/cdm/base");



    // ~~~~~~~~~~~~~~~~ UNIVERSAL MAPPER ~~~~~~~~~~~~~~~~~~
    public static final byte ENTERPRISE_TYPE = 1;
    public static final byte BUSINESS_ENTITY_TYPE = 2;

}
