package ru.denimexe.vetis.core.sync.runnable;

import lombok.Data;
import org.jsoup.nodes.Element;
import ru.denimexe.vetis.core.utils.EnterpriseQueue;
import ru.denimexe.vetis.core.data.entities.Enterprise;
import ru.denimexe.vetis.core.utils.EnterpriseParser;

import java.util.HashMap;
import java.util.List;

/**
 * Created by denimexe on 20.09.17.
 */
@Data
public class EnterpriseParseThread extends Thread{

    private List<Element> elements;
    private EnterpriseQueue queue;

    public EnterpriseParseThread(List<Element> elements, EnterpriseQueue queue, String name) {
        super(name);
        this.elements = elements;
        this.queue = queue;
        start();
    }

    public EnterpriseParseThread(List<Element> elements, EnterpriseQueue queue) {
        this.elements = elements;
        this.queue = queue;
        start();
    }

    @Override
    public void run() {
        String name = this.getName();
        for(Element e : elements){

            HashMap<String, Object> map = EnterpriseParser.parse(e);

            Enterprise ent = (Enterprise) map.get("enterprise");


            if(ent != null) {
                queue.offer(ent);
            }

        }
    }
}
