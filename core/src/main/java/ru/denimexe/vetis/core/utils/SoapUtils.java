package ru.denimexe.vetis.core.utils;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPMessage;

/**
 * Created by denimexe on 25.09.17.
 */
public class SoapUtils {
    public static SOAPMessage createRequest() throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("Authorization", "Basic " + "bm92b3NpYnZ1LTE3MDQxMDpTb2U4TXVhag==");

        soapMessage.saveChanges();
        return soapMessage;
    }

    public static SOAPElement addListOptions(SOAPElement parent, int count, int offset) throws Exception{
        SOAPElement element = parent.addChildElement("listOptions", Constants.NS_BS.getName());
        element.addChildElement("count", Constants.NS_BS.getName()).addTextNode(String.valueOf(count));
        element.addChildElement("offset", Constants.NS_BS.getName()).addTextNode(String.valueOf(offset));
        return element;
    }
}
