package ru.denimexe.vetis.core.runner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import ru.denimexe.vetis.core.soap.WS_EnterpriseService;
import ru.denimexe.vetis.core.sync.beans.EnterpriseBean;
import ru.denimexe.vetis.core.utils.Constants;
import ru.denimexe.vetis.core.utils.XLSCreator;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by denimexe on 25.09.17.
 */
public class ListFetcher {

    /**
     *
     * @param guids
     */
    public static void getEnterprisesByRegion(List<String> guids){
        try {
//            long st, en;
//            st = System.nanoTime();

            EnterpriseBean ebean = new EnterpriseBean();
            int total = 0;
            int count = 0;
            int num = 0;

            for(String guid: guids) {
                count = 0;
                num = 0;

                System.out.println("fetching data for: " + guid);
                do {
                    byte[] bytes = WS_EnterpriseService.getEnterprisesByRegion(guid, num++);
//                    byte[] bytes = Files.readAllBytes(Paths.get("/Users/denimexe/home/enterprises" + (num++ + 1)));
                    Document doc = Jsoup.parse(new String(bytes, "UTF-8"));
                    count = Integer.valueOf(doc.select("ent|enterpriseList").first().attributes().get("count"));
                    ebean.addData(doc);
                    System.out.print(". ");
                } while (count == Constants.REQUEST_COUNT);
                System.out.print("\t - " + (Constants.REQUEST_COUNT*num + count) + "\n");
            }
            total += Constants.REQUEST_COUNT*num + count;
            System.out.println("total amount: " + total);

            SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yy");
            String date = sdf.format(new Date());

            Path p = Paths.get("/Users/denimexe/home/enterprises" + date);
            p = Files.createFile(p);
            Files.write(p, ebean.getQueue().toString().getBytes());

            XLSCreator.CreateEnterpriseList(Jsoup.parse(new String(ebean.getQueue().toString().getBytes(), "UTF-8")), "Miratorg enterprises" + date);

        } catch (Exception ex) {
            System.out.println("listFetcher exception: " + ex.toString());
        }
    }
}
