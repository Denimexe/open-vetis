package ru.denimexe.vetis.core.soap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import ru.denimexe.vetis.core.utils.Constants;
import ru.denimexe.vetis.core.utils.Namespace;
import ru.denimexe.vetis.core.utils.SoapServicesUtil;
import ru.denimexe.vetis.core.utils.SoapUtils;

import javax.xml.soap.*;
import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static ru.denimexe.vetis.core.utils.SoapUtils.createRequest;

/**
 * Created by denimexe on 25.09.17.
 */
public class WS_ApplicationManagementService {

    private static final String url = "https://api.vetrf.ru/platform/services/ApplicationManagementService";

    public static final Namespace NS_WS = new Namespace("ws", "http://api.vetrf.ru/schema/cdm/application/ws-definitions");
    public static final Namespace NS_APP = new Namespace("app", "http://api.vetrf.ru/schema/cdm/application");
    public static final Namespace NS_MERCVU = new Namespace("mercvu", "http://api.vetrf.ru/schema/cdm/mercury/vu/applications");
    public static final Namespace NS_ENT = new Namespace("ent", "http://api.vetrf.ru/schema/cdm/cerberus/enterprise");
    public static final Namespace NS_COM = new Namespace("com", "http://api.vetrf.ru/schema/cdm/argus/common");



    public static byte[] getApplicationsPage(String applicationId) throws Exception{
        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection connection = soapConnectionFactory.createConnection();

        SOAPMessage soapMessage = createRequest();

        //  ~~~~~~~~~

        SOAPPart soapPart = soapMessage.getSOAPPart();
        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();

        SoapServicesUtil.setNamespaces(envelope, new ArrayList<Namespace>() {{add(NS_WS);}});
        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        SOAPElement main = soapBody.addChildElement("receiveApplicationResultRequest", NS_WS.getName());
        main.addChildElement("apiKey", NS_WS.getName()).addTextNode(Constants.API_KEY);
        main.addChildElement("issuerId", NS_WS.getName()).addTextNode(Constants.ISSUER_ID);
        main.addChildElement("applicationId", NS_WS.getName()).addTextNode(applicationId);
        soapMessage.saveChanges();

        //  ~~~~~~~~~

        SOAPMessage response = connection.call(soapMessage, new URL(url));

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        response.writeTo(out);

        return out.toByteArray();
    }

    public static String submitRequestInterval(String businessEntityGuid, String enterpriseGuid, int request_count) throws Exception{
        Calendar cal = Calendar.getInstance();
        int date = cal.get(Calendar.DAY_OF_MONTH) - 7;
        if(date < 1) {
            date = 1;
        }

        //cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), date);
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.JANUARY), 1);
        Date upto = new Date();
        Date since = cal.getTime();

        return submitRequestInterval(since, upto, businessEntityGuid, enterpriseGuid, request_count);
    }

    public static String submitRequestInterval(Date since, String businessEntityGuid, String enterpriseGuid, int request_count) throws Exception {
        return submitRequestInterval(since, new Date(), businessEntityGuid, enterpriseGuid, request_count);
    }

    public static String submitRequestInterval(Date since, Date upto, String businessEntityGuid, String enterpriseGuid, int request_count) throws Exception{
        String applicationId = "";

        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection connection = soapConnectionFactory.createConnection();

        SOAPMessage soapMessage = createRequest();

        //  ~~~~~~~~~

        SOAPPart soapPart = soapMessage.getSOAPPart();
        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();

        SoapServicesUtil.setNamespaces(envelope, new ArrayList<Namespace>() {{
            add(Constants.NS_BS);
            add(NS_WS);
            add(NS_APP);
            add(NS_MERCVU);
            add(NS_ENT);
            add(NS_COM);
        }});

        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        SOAPElement main = soapBody.addChildElement("submitApplicationRequest", NS_WS.getName());
        main.addChildElement("apiKey", NS_WS.getName()).addTextNode(Constants.API_KEY);
        SOAPElement app = main.addChildElement("application", NS_APP.getName());

        app.addChildElement("serviceId", NS_APP.getName()).addTextNode(Constants.SERVICE_ID);
        app.addChildElement("issuerId", NS_APP.getName()).addTextNode(Constants.ISSUER_ID);
        app.addChildElement("issueDate", NS_APP.getName()).addTextNode(SoapServicesUtil.format(new Date()));

        SOAPElement data = app.addChildElement("data", NS_APP.getName()).addChildElement("getVetDocumentChangesListRequest", NS_MERCVU.getName());

        data.addChildElement("localTransactionId", NS_MERCVU.getName()).addTextNode(new SimpleDateFormat("yyMMddHHmmssSSS").format(new Date()));
        data.addChildElement("initiator", NS_MERCVU.getName()).addChildElement("login", NS_COM.getName()).addTextNode(Constants.LOGIN);

        SoapUtils.addListOptions(data, Constants.REQUEST_COUNT, request_count*Constants.REQUEST_COUNT);

        if(since != null) {
            SOAPElement dates = data.addChildElement("updateDateInterval", Constants.NS_BS.getName());
            dates.addChildElement("beginDate", Constants.NS_BS.getName()).addTextNode(SoapServicesUtil.since_format(since));
            dates.addChildElement("endDate", Constants.NS_BS.getName()).addTextNode(SoapServicesUtil.upto_format(upto));
        }

        SOAPElement member = data.addChildElement("businessMember", NS_MERCVU.getName());
        member.addChildElement("businessEntity", NS_ENT.getName())
                .addChildElement("guid", Constants.NS_BS.getName())
                .addTextNode(businessEntityGuid);
        member.addChildElement("enterprise", NS_ENT.getName())
                .addChildElement("guid", Constants.NS_BS.getName())
                .addTextNode(enterpriseGuid);



        soapMessage.saveChanges();

        //  ~~~~~~~~~

        SOAPMessage response = connection.call(soapMessage, new URL(url));

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        response.writeTo(out);
        Document doc = Jsoup.parse(new String(out.toByteArray(), "UTF-8"));

        return doc.select("applicationId").text();
    }
}
