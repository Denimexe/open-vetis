package ru.denimexe.vetis.core.data.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Objects;

/**
 * Created by denimexe on 19.09.17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "expert", schema = "vetis")
public class Expert implements Serializable {

    @Transient
    protected final SimpleDateFormat format = new SimpleDateFormat("dd.MM.yy, HH:mm");

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    private String name;

    @Override
    public String toString() {
        return  "<id>" + id +
                "</id><name>" + name +
                "</name>";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Expert expert = (Expert) o;
        return Objects.equals(format, expert.format) &&
                Objects.equals(id, expert.id) &&
                Objects.equals(name, expert.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), format, id, name);
    }
}
