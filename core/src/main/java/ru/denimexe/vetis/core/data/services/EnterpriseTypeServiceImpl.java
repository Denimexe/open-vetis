package ru.denimexe.vetis.core.data.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.denimexe.vetis.core.data.entities.EnterpriseType;
import ru.denimexe.vetis.core.data.repositories.EnterpriseTypeRepository;

import java.util.List;

/**
 * Created by denimexe on 12.10.17.
 */
@Service
public class EnterpriseTypeServiceImpl implements EnterpriseTypeService {

    @Autowired
    private EnterpriseTypeRepository repository;

    @Override
    public void save(EnterpriseType enterpriseType) {
        repository.save(enterpriseType);
    }

    @Override
    public EnterpriseType findOne(Integer id) {
        return repository.findOne(id);
    }

    @Override
    public List<EnterpriseType> findAll() {
        return repository.findAll();
    }

    @Override
    public EnterpriseType findByName(String name) { return repository.findByName(name); }
}
