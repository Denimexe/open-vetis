package ru.denimexe.vetis.core.data.services;

import ru.denimexe.vetis.core.data.entities.Unit;

import java.util.List;

/**
 * Created by dirde on 15.11.2016.
 */
public interface UnitService {

    void save(Unit unit);

    Unit findOne(Integer id);

    List<Unit> findAll();

    Unit findByGuid(String guid);
}
