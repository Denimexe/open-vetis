package ru.denimexe.vetis.core.rest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.denimexe.vetis.core.JMS.CoreJMSSender;

/**
 * Created by Anton Anikin on 30.06.2017.
 */
@RestController
@RequestMapping(value = "rest")
public class TestRestController {
    private final Logger LOGGER = LogManager.getLogger(TestRestController.class);

    @Autowired
    private CoreJMSSender sender;

    @RequestMapping(value="test", method = RequestMethod.GET)
    @ResponseBody
    public void getTestData() {
        //return "{\"data\":\"test\"}";
        sender.send("Rest sends Hello through Core to Web");
    }

    @RequestMapping(value="test/request/{value}", method = RequestMethod.GET)
    @ResponseBody
    public String getMoreTestData(@PathVariable("value") String value) {
        return "{\"data\":\"" + value + "\"}";
    }
}
