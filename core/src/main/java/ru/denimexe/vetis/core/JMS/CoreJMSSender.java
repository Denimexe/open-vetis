package ru.denimexe.vetis.core.JMS;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;

/**
 * Created by denimexe on 16.10.17.
 */
@Service
public class CoreJMSSender {

    private final Logger LOGGER = LogManager.getLogger(CoreJMSSender.class);

    @Autowired
    private JmsTemplate coreJMS;

    public void send(final String text) {

        coreJMS.send(new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                Message message = session.createTextMessage(text);
                //set ReplyTo header of Message, pretty much like the concept of email.
//                message.setJMSReplyTo(new ActiveMQQueue("core"));
                return message;
            }
        });
    }

}
