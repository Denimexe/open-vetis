package ru.denimexe.vetis.core.data.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.denimexe.vetis.core.data.entities.Product;
import ru.denimexe.vetis.core.data.entities.ProductType;
import ru.denimexe.vetis.core.data.repositories.ProductRepository;
import ru.denimexe.vetis.core.data.repositories.ProductTypeRepository;

import java.util.List;

/**
 * Created by denimexe on 12.10.17.
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository repository;

    @Autowired
    private ProductTypeRepository typeRepository;

    @Override
    public void save(Product product) {
        repository.save(product);
    }

    @Override
    public ProductType findType(Integer id) {
        return typeRepository.findOne(id);
    }

    @Override
    public ProductType findTypeByName(String name) {
        return typeRepository.findByName(name);
    }

    @Override
    public Product findOne(Integer id) {
        return repository.findOne(id);
    }

    @Override
    public Product findByGuid(String guid) {
        return repository.findByGuid(guid);
    }

    @Override
    public List<Product> findAll() {
        return repository.findAll();
    }

    @Override
    public void saveType(ProductType type) {
        typeRepository.save(type);
    }
}
