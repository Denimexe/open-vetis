package ru.denimexe.vetis.core.utils;

import javafx.fxml.FXMLLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;
import ru.denimexe.vetis.core.data.entities.BusinessEntity;
import ru.denimexe.vetis.core.data.entities.Enterprise;
import ru.denimexe.vetis.core.data.services.*;
import ru.denimexe.vetis.core.rest.DocumentRestController;
import ru.denimexe.vetis.core.sync.runnable.DocumentLoadThread;

import java.io.IOException;

/**
 * Created by denimexe on 19.03.18.
 */

@Service
public class DocumentLoadService {

    @Autowired
    private ProductService productService;
    @Autowired
    private DocumentService documentService;
    @Autowired
    private EnterpriseService enterpriseService;
    @Autowired
    private BusinessEntityService businessEntityService;
    @Autowired
    private SubProductService subProductService;
    @Autowired
    private AddressService addressService;
    @Autowired
    private UnitService unitService;

    public void runThread(BusinessEntity businessEntity, Enterprise enterprise, String applicationId) {

        DocumentLoadThread thread = new DocumentLoadThread();

        thread.init(businessEntity, enterprise, applicationId, productService, documentService, enterpriseService,
                businessEntityService, subProductService,
                addressService, unitService);
        thread.start();
    }
}
