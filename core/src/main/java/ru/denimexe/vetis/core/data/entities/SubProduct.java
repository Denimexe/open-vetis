package ru.denimexe.vetis.core.data.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by denimexe on 03.10.17.
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ref_sub_product", schema = "vetis")
public class SubProduct  implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    private String guid;
    private String name;
    private String code;

    @ManyToOne
    @JoinColumn(name = "ref_product_id")
    private Product product;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SubProduct that = (SubProduct) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(guid, that.guid) &&
                Objects.equals(name, that.name) &&
                Objects.equals(code, that.code) &&
                Objects.equals(product, that.product);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), id, guid, name, code, product);
    }
}
