package ru.denimexe.vetis.core.utils;

import javax.xml.soap.SOAPEnvelope;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by denimexe on 11.10.17.
 */
public class SoapServicesUtil {
    private static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    private static final SimpleDateFormat sinceFormat = new SimpleDateFormat("yyyy-MM-dd'T00:00:00'");
    private static final SimpleDateFormat uptoFormat = new SimpleDateFormat("yyyy-MM-dd'T23:59:59'");


    public static void setNamespaces(SOAPEnvelope envelope, List<Namespace> namespaces) throws Exception{
        if(namespaces == null || namespaces.isEmpty() || envelope == null) {
            throw new NullPointerException("namespaces or envelope is null");
        }
        for(Namespace ns: namespaces) {
            envelope.addNamespaceDeclaration(ns.getName(), ns.getUrl());
        }
    }

    public static String format(Date date){
        return format.format(date);
    }

    public static String since_format(Date date){
        return sinceFormat.format(date);
    }

    public static String upto_format(Date date){
        return uptoFormat.format(date);
    }
}
