package ru.denimexe.vetis.core.data.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.denimexe.vetis.core.data.entities.*;
import ru.denimexe.vetis.core.data.repositories.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by denimexe on 12.10.17.
 */
@Service
public class DocumentServiceImpl  implements DocumentService {

    @Autowired
    private DocumentRepository repository;

    @Autowired
    private DocumentFormRepository formRepository;

    @Autowired
    private DocumentTypeRepository typeRepository;

    @Autowired
    private DocumentStatusRepository statusRepository;

    @Autowired
    private BatchRepository batchRepository;

    @Autowired
    private ExpertRepository expertRepository;

    @Override
    public void save(Document document) {
        repository.save(document);
    }

    @Override
    public Document findOne(Integer id) {
        return repository.findOne(id);
    }

    @Override
    public List<Document> findAll() {
        return repository.findAll();
    }

    @Override
    public Document findByUUID(String uuid) {
        return repository.findByUuid(uuid);
    }

    @Override
    public List<Document> findByRequestor(BusinessEntity businessEntity, Enterprise enterprise) {
        return repository.findByRequestorBusinessEntityAndRequestorEnterprise(businessEntity, enterprise);
    }

    @Override
    public DocumentType findType(Integer id) {
        return typeRepository.findOne(id);
    }

    @Override
    public DocumentType findTypeByName(String name) {
        return typeRepository.findByName(name);
    }

    @Override
    public DocumentForm findForm(Integer id) {
        return formRepository.findOne(id);
    }

    @Override
    public DocumentForm findFormByName(String name) {
        return formRepository.findByName(name);
    }

    @Override
    public DocumentStatus findStatus(Integer id) {
        return statusRepository.findOne(id);
    }

    @Override
    public DocumentStatus findStatusByName(String name) {
        return statusRepository.findByName(name);
    }

    @Override
    public Batch findBatch(Integer id) {
        return batchRepository.findOne(id);
    }

    @Override
    public List<Batch> findBatches() {
        return batchRepository.findAll();
    }

    @Override
    public void save(Batch batch) {
        batchRepository.save(batch);
    }

    @Override
    public HashMap<String, String> getReport() {
        HashMap<String, String> map = new HashMap<>();

        ArrayList<Document> documents = new ArrayList<>();


        return map;
    }

    @Override
    public Expert findExpertByName(String name) { return expertRepository.findByName(name); }

    @Override
    public void save(Expert expert) { expertRepository.save(expert); }
}