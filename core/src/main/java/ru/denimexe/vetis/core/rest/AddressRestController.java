package ru.denimexe.vetis.core.rest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.denimexe.vetis.core.data.entities.Address;
import ru.denimexe.vetis.core.data.entities.AddressType;
import ru.denimexe.vetis.core.data.services.AddressService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anton Anikin on 30.06.2017.
 */
@RestController
@RequestMapping(value = "rest")
public class AddressRestController {
    private final Logger LOGGER = LogManager.getLogger(AddressRestController.class);

    @Autowired
    private AddressService addressService;
//    @Autowired
//    private WebJMSSender sender;

    @RequestMapping(value="types", method = RequestMethod.GET)
    @ResponseBody
    public List<AddressType> getAddressTypeList() {
        try {
            List<AddressType> AddressTypes = addressService.findTypes();
            return AddressTypes;
        } catch (Exception ex){
            LOGGER.error("getAddressTypeList()", ex);
        }

        return new ArrayList<AddressType>();
    }


    @RequestMapping(value="type/{id}", method = RequestMethod.GET)
    @ResponseBody
    public AddressType getAddressType(@PathVariable("id") int id) {
        try {
            return addressService.findType(id);
        } catch (Exception ex){
            LOGGER.error("getAddressType()", ex);
            return null;
        }
    }

    @RequestMapping(value="type/name/{name}", method = RequestMethod.GET)
    @ResponseBody
    public AddressType getAddressTypeByName(@PathVariable("name") String name) {
        try {
            return addressService.findTypeByName(name);
        } catch (Exception ex){
            LOGGER.error("getAddressType()", ex);
            return null;
        }
    }

    @RequestMapping(value="values", method = RequestMethod.GET)
    @ResponseBody
    public List<Address> getAddressList() {
        try {
            return addressService.findAll();
        } catch (Exception ex){
            LOGGER.error("getAddressList()", ex);
        }

        return new ArrayList<Address>();
    }

    @RequestMapping(value="value/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Address getAddress(@PathVariable("id") int id) {
        try {
            return addressService.findOne(id);
        } catch (Exception ex){
            LOGGER.error("getAddress()", ex);
            return null;
        }
    }

    @RequestMapping(value="value/guid/{guid}", method = RequestMethod.GET)
    @ResponseBody
    public Address getAddressByGuid(@PathVariable("guid") String guid) {
        try {
            return addressService.findByGuid(guid);
        } catch (Exception ex){
            LOGGER.error("getAddressByGuid()", ex);
            return null;
        }
    }

    @RequestMapping(value="values/bytype/{typeid}", method = RequestMethod.GET)
    @ResponseBody
    public List<Address> getAddressListByType(@PathVariable("typeid") int id ) {
        try {
            return addressService.findByTypeId(id);
        } catch (Exception ex){
            LOGGER.error("getAddress()", ex);
            return null;
        }
    }

    @RequestMapping(value="values/byparentguid/{guid}", method = RequestMethod.GET)
    @ResponseBody
    public List<Address> getAddressListByParentGuid(@PathVariable("guid") String guid ) {
        try {
            return addressService.findByParentGuid(guid);
        } catch (Exception ex){
            LOGGER.error("getAddressListByParentGuid()", ex);
            return null;
        }
    }

//    @RequestMapping(value="test", method = RequestMethod.GET)
//    @ResponseBody
//    public void getTestData() {
//        sender.send("hi Core, it's Web!");
//    }

}
