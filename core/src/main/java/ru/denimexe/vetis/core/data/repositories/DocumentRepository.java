package ru.denimexe.vetis.core.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;
import ru.denimexe.vetis.core.data.entities.*;

import java.util.List;

/**
 * Created by denimexe on 04.10.17.
 */
@Repository
public interface DocumentRepository extends JpaRepository<Document, Integer>, QueryDslPredicateExecutor<Document> {
    Document findByUuid(String uuid);

    List<Document> findByForm(DocumentForm form);//Integer id);

    List<Document> findByRequestorBusinessEntityAndRequestorEnterprise(BusinessEntity businessEntity, Enterprise enterprise);

//    List<Document> findByRef_document_type_id(Integer id);

//    List<Document> findByDocumentStatus(DocumentStatus Status);
}
