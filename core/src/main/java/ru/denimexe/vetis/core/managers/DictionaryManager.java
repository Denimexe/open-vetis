package ru.denimexe.vetis.core.managers;

import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.denimexe.vetis.core.data.entities.Unit;
import ru.denimexe.vetis.core.soap.WS_DictionaryService;
import ru.denimexe.vetis.core.utils.UnitParser;

import java.util.List;

/**
 * Created by denimexe on 12.10.17.
 */
@Service
public class DictionaryManager implements Manager {

    private final Logger LOGGER = LogManager.getLogger(DictionaryManager.class);

    @Getter
    private List<Unit> units;
    @Autowired
    private UnitParser parser;

    public DictionaryManager(){

    }

    @Override
    public void uploadUnits() {
        try {
            byte[] data = WS_DictionaryService.getUnits();
            Document doc = Jsoup.parse(new String(data, "UTF-8"));
            Elements trs = doc.select("com|unit");
            units = parser.parse(trs);
        } catch (Exception ex) {
            LOGGER.error("uploadUnits() ", ex);
        }

    }
}
