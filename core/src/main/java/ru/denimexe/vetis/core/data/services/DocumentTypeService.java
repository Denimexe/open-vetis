package ru.denimexe.vetis.core.data.services;

import ru.denimexe.vetis.core.data.entities.DocumentType;

import java.util.List;

/**
 * Created by dirde on 15.11.2016.
 */
public interface DocumentTypeService {

    void save(DocumentType documentType);

    DocumentType findOne(Integer id);

    List<DocumentType> findAll();

    DocumentType findByName(String name);
}
