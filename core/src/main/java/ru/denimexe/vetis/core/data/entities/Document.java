package ru.denimexe.vetis.core.data.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Created by denimexe on 13.09.17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "vet_document", schema = "vetis")
public class Document implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    private String uuid;
    private String issueSeries;
    private String issueNumber;
    private Date issueDate;

    @ManyToOne @JoinColumn(name = "ref_vet_document_form_id", nullable = true)
    private DocumentForm form;
    @ManyToOne @JoinColumn(name = "ref_vet_document_type_id", nullable = true)
    private DocumentType type;
    @ManyToOne @JoinColumn(name = "ref_vet_document_status_id", nullable = true)
    private DocumentStatus status;

    @ManyToOne @JoinColumn(name = "consignor_enterprise_id", nullable = true)
    private Enterprise consignorEnterprise;
    
    @ManyToOne @JoinColumn(name = "consignor_business_entity_id", nullable = true)
    private BusinessEntity consignorBusinessEntity;


    @ManyToOne @JoinColumn(name = "consignee_enterprise_id", nullable = true)
    private Enterprise consigneeEnterprise;

    @ManyToOne @JoinColumn(name = "consignee_business_entity_id", nullable = true)
    private BusinessEntity consigneeBusinessEntity;

    @ManyToOne @JoinColumn(name = "requestor_enterprise_id", nullable = true)
    private Enterprise requestorEnterprise;

    @ManyToOne @JoinColumn(name = "requestor_business_entity_id", nullable = true)
    private BusinessEntity requestorBusinessEntity;

    @ManyToOne @JoinColumn(name = "batch_id")
    private Batch batch;

    @ManyToOne @JoinColumn(name = "ref_expert_id")
    private Expert expert;

    @Override
    public String toString() {
        return "<id>" + id +
                "</id><uuid>" + uuid +
                "</uuid><issueSeries>" + issueSeries +
                "</issueSeries><issueNumber>" + issueNumber +
                "</issueNumber><issueDate>" + issueDate +
                "</issueDate><form>" + form +
                "</form><type>" + type +
                "</type><status>" + status +
                "</status>";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Document document = (Document) o;
        return Objects.equals(id, document.id) &&
                Objects.equals(uuid, document.uuid) &&
                Objects.equals(issueSeries, document.issueSeries) &&
                Objects.equals(issueNumber, document.issueNumber) &&
                Objects.equals(issueDate, document.issueDate) &&
                Objects.equals(form, document.form) &&
                Objects.equals(type, document.type) &&
                Objects.equals(status, document.status) &&
                Objects.equals(consignorEnterprise, document.consignorEnterprise) &&
                Objects.equals(consignorBusinessEntity, document.consignorBusinessEntity) &&
                Objects.equals(consigneeEnterprise, document.consigneeEnterprise) &&
                Objects.equals(consigneeBusinessEntity, document.consigneeBusinessEntity) &&
                Objects.equals(requestorEnterprise, document.requestorEnterprise) &&
                Objects.equals(requestorBusinessEntity, document.requestorBusinessEntity) &&
                Objects.equals(batch, document.batch) &&
                Objects.equals(expert, document.expert);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), id, uuid, issueSeries, issueNumber, issueDate, form, type, status, consignorEnterprise, consignorBusinessEntity, consigneeEnterprise, consigneeBusinessEntity, requestorEnterprise, requestorBusinessEntity, batch, expert);
    }
}
