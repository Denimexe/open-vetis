package ru.denimexe.vetis.core.data.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.denimexe.vetis.core.data.entities.DocumentStatus;
import ru.denimexe.vetis.core.data.repositories.DocumentStatusRepository;

import java.util.List;

/**
 * Created by denimexe on 12.10.17.
 */
@Service
public class DocumentStatusServiceImpl implements DocumentStatusService {

    @Autowired
    private DocumentStatusRepository repository;

    @Override
    public void save(DocumentStatus documentStatus) {
        repository.save(documentStatus);
    }

    @Override
    public DocumentStatus findOne(Integer id) {
        return repository.findOne(id);
    }

    @Override
    public List<DocumentStatus> findAll() {
        return repository.findAll()
                ;
    }

    @Override
    public DocumentStatus findByName(String name) {
        return repository.findByName(name);
    }
}
