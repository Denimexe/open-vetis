package ru.denimexe.vetis.core.utils;

import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.LogManager;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ru.denimexe.vetis.core.data.entities.BusinessEntity;
import ru.denimexe.vetis.core.soap.WS_EnterpriseService;

import java.util.HashMap;

/**
 * Created by denimexe on 13.09.17.
 */
@Log4j2
public class BusinessEntityParser {

    public static HashMap parse(byte[] bytes){

        try {
            Document doc = Jsoup.parse(new String(bytes, "UTF-8"));
            Element e = doc.select("ent|businessEntity").first();
            if(e != null) {
                return parse(e);
            } else {
                e = doc.select("ent|businessEntity").first();
            }
        } catch ( Exception ex) {
            LogManager.getLogger("BusinessEntityParser").error("parse()", ex);
        }

        return null;
    }

    public static HashMap<String, Object> parse(Element e){
        if(e == null)
            return null;

        try {
            HashMap<String, Object> map = new HashMap<>();
//            map.put("type", Constants.BUSINESS_ENTITY_TYPE);

            BusinessEntity bEnt = new BusinessEntity();

//            <ent:businessEntity>
//                <bs:guid>9b7520f1-3b2f-4e92-8ae7-1fc00267f14c</bs:guid>
//                <ent:type>1</ent:type>
//                <ent:name>ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "УЖАНИХИНСКИЙ МОЛОЧНЫЙ ЗАВОД"</ent:name>
//                <ent:inn>5406759542</ent:inn>
//                <ent:kpp>540601001</ent:kpp>
//                <ent:ogrn>1135476144363</ent:ogrn>
//                <ent:juridicalAddress>
//                    <ikar:country>
//                        <bs:guid>74a3cbb1-56fa-94f3-ab3f-e8db4940d96b</bs:guid>
//                    </ikar:country>
//                    <ikar:region>
//                        <bs:guid>1ac46b49-3209-4814-b7bf-a509ea1aecd9</bs:guid>
//                    </ikar:region>
//                    <ikar:district/>
//                    <ikar:locality>
//                        <bs:guid>8dea00e3-9aab-4d8e-887c-ef2aaa546456</bs:guid>
//                    </ikar:locality>
//                    <ikar:addressView>630017, Российская Федерация, Новосибирская обл., г. Новосибирск, Гаранина ул., д. 21, 212</ikar:addressView>
//                </ent:juridicalAddress>
//            </ent:businessEntity>

            //id, guid, name, country_id, region_id, type_id, district_id, locality_id, address_view, businessEntity_id

            bEnt.setGuid(e.select("bs|guid").first().text().trim());
            Elements name = e.select("ent|name");
            if(name.size() > 0 ) {
                bEnt.setName(name.first().text().trim());
            } else {
                name = e.select("ent|name");
                if(name.size() > 0 ) {
                    bEnt.setName(name.first().text().trim());
                }
            }
            bEnt.setInn(e.select("ent|inn").first().text().trim());

            Elements kpp = e.select("ent|kpp");
            if(kpp.size() > 0) {
                bEnt.setKpp(kpp.first().text().trim());
            }

            Elements ogrn = e.select("ent|ogrn");
            if(ogrn.size() > 0) {
                bEnt.setOgrn(ogrn.first().text().trim());
            }

            bEnt.setAddressView(e.select("ikar|addressView").first().text().trim());


            Elements country = e.select("ikar|country");
            if (country.size() > 0) {
                map.put("country", country.first().select("bs|guid").first().text().trim());
            }

            Elements region = e.select("ikar|region");
            if (region.size() > 0) {
                map.put("region", region.first().select("bs|guid").first().text().trim());
            }

            Elements district = e.select("ikar|district");
            if (district.size() > 0) {
                if(district.first().select("bs|guid").size() > 0) {
                    map.put("district", district.first().select("bs|guid").first().text().trim());
                }
            }

            Elements locality = e.select("ikar|locality");
            if (locality.size() > 0) {
                if(locality.first().select("bs|guid").size() > 0) {
                    map.put("locality", locality.first().select("bs|guid").first().text().trim());
                }
            }

            map.put("type", e.select("ent|type").first().text().trim());

            map.put("businessEntity", bEnt);
            return map;
        } catch (Exception ex) {
            log.error("parse()", ex);
            return null;
        }
    }

}