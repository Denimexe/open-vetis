package ru.denimexe.vetis.core.sync.runnable;

import lombok.Data;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import ru.denimexe.vetis.core.utils.DocumentQueue;
import ru.denimexe.vetis.core.data.entities.Document;
import ru.denimexe.vetis.core.utils.DocumentParser;

import java.util.List;

/**
 * Created by denimexe on 20.09.17.
 */
@Data
public class DocumentParseThread extends Thread{
    private List<Element> elements;
    private DocumentQueue queue;

//    public DocumentParseThread(List<Element> elements, DocumentQueue queue, DocumentStatistics stat, String name) {
//        super(name);
//        this.elements = elements;
//        this.queue = queue;
//        this.stat = stat;
//        start();
//    }

    public DocumentParseThread(List<Element> elements, DocumentQueue queue) {
        super();
        this.elements = elements;
        this.queue = queue;
//        this.stat = stat;
        start();
    }

    @Override
    public void run() {
//        String name = this.getName();
//        for(Element e : elements){
//            Document doc = DocumentParser.parse(e);
////            try {
////                if(doc.getConsignorGuid() != null) {
////                    doc.setConsignorRegion(WS_EnterpriseService.getRegion(doc.getConsignorGuid()));
////                }
////            } catch ( Exception ex ){ }
////            try {
////                if(doc.getConsigneeGuid() != null) {
////                    doc.setConsigneeRegion(WS_EnterpriseService.getRegion(doc.getConsigneeGuid()));
////                }
////            } catch ( Exception ex ){ }
//
//            if(doc != null) {
//                queue.offer(doc);
//            }
//
//        }
    }
}
