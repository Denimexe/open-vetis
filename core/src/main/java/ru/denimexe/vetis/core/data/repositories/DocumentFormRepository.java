package ru.denimexe.vetis.core.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;
import ru.denimexe.vetis.core.data.entities.DocumentForm;

/**
 * Created by denimexe on 04.10.17.
 */
@Repository
public interface DocumentFormRepository extends JpaRepository<DocumentForm, Integer>, QueryDslPredicateExecutor<DocumentForm> {
    DocumentForm findByName(String name);
}
