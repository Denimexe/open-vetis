package ru.denimexe.vetis.core.data.services;

import ru.denimexe.vetis.core.data.entities.BusinessEntity;
import ru.denimexe.vetis.core.data.entities.Enterprise;
import ru.denimexe.vetis.core.data.entities.Request;

import java.util.List;

/**
 * Created by dirde on 15.11.2016.
 */
public interface RequestService {

    void save(Request request);

    Request findOne(Integer id);

    Request findByEndpoint(BusinessEntity businessEntity, Enterprise enterprise);

    List<Request> findAll();
}
