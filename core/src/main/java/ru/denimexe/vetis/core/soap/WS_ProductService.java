package ru.denimexe.vetis.core.soap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import ru.denimexe.vetis.core.data.entities.Product;
import ru.denimexe.vetis.core.utils.Constants;
import ru.denimexe.vetis.core.utils.Namespace;
import ru.denimexe.vetis.core.utils.SoapServicesUtil;
import ru.denimexe.vetis.core.utils.SoapUtils;

import javax.xml.soap.*;
import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import static ru.denimexe.vetis.core.utils.SoapUtils.createRequest;

/**
 * Created by denimexe on 25.09.17.
 */
public class WS_ProductService {

    private static final String url = "https://api.vetrf.ru/platform/services/ProductService";

    public static final Namespace NS_WS = new Namespace("ws", "http://api.vetrf.ru/schema/cdm/argus/production/ws-definitions");

    public static HashMap<String, String> getParsedProduct(String guid) {
        try {
            HashMap<String, String> product = new HashMap<>();
            Document doc = Jsoup.parse(new String(getProduct(guid)), "UTF-8");
            Element e = doc.select("pro|product").first();

            product.put("guid", e.select("bs|guid").first().text().trim());
            product.put("name", e.select("pro|name").first().text().trim());
            product.put("code", e.select("pro|code").first().text().trim());

            return product;

        } catch ( Exception ex) {
            return null;
        }
    }

    public static HashMap<String, String> getParsedSubProduct(String guid) {
        try {
            HashMap<String, String> subProduct = new HashMap<>();
            Document doc = Jsoup.parse(new String(getSubProduct(guid)), "UTF-8");
            Element e = doc.select("pro|subProduct").first();

            subProduct.put("guid", e.select("bs|guid").first().text().trim());
            subProduct.put("name", e.select("pro|name").first().text().trim());
            subProduct.put("code", e.select("pro|code").first().text().trim());

            return subProduct;

        } catch ( Exception ex) {
            return null;
        }
    }


    public static byte[] getProduct(String guid) throws Exception {
        return productGetter("getProductByGuidRequest", guid);
    }

    public static byte[] getSubProduct(String guid) throws Exception {
        return productGetter("getSubProductByGuidRequest", guid);
    }

    public static byte[] productGetter(String method, String guid) throws Exception{



        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection connection = soapConnectionFactory.createConnection();

        SOAPMessage soapMessage = createRequest();

        //  ~~~~~~~~~

        SOAPPart soapPart = soapMessage.getSOAPPart();
        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        SoapServicesUtil.setNamespaces(envelope, new ArrayList<Namespace>() {{add(NS_WS); add(Constants.NS_BS);}});

        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        SOAPElement main = soapBody.addChildElement(method, NS_WS.getName());
        main.addChildElement("guid", Constants.NS_BS.getName()).addTextNode(guid);
        soapMessage.saveChanges();

        //  ~~~~~~~~~

        SOAPMessage response = connection.call(soapMessage, new URL(url));


        ByteArrayOutputStream out = new ByteArrayOutputStream();
        response.writeTo(out);

        return out.toByteArray();
    }

}
