package ru.denimexe.vetis.core.data.services;

import ru.denimexe.vetis.core.data.entities.Notification;

import java.util.List;

/**
 * Created by dirde on 15.11.2016.
 */
public interface NotificationService {

    void save(Notification notification);

    Notification findOne(Integer id);

    List<Notification> findAll();
}
