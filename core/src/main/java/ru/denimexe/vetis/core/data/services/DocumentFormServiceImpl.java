package ru.denimexe.vetis.core.data.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.denimexe.vetis.core.data.entities.DocumentForm;
import ru.denimexe.vetis.core.data.repositories.DocumentFormRepository;

import java.util.List;

/**
 * Created by denimexe on 12.10.17.
 */
@Service
public class DocumentFormServiceImpl implements DocumentFormService {

    @Autowired
    private DocumentFormRepository repository;

    @Override
    public void save(DocumentForm documentForm) {
        repository.save(documentForm);
    }

    @Override
    public DocumentForm findOne(Integer id) {
        return repository.findOne(id);
    }

    @Override
    public List<DocumentForm> findAll() {
        return repository.findAll();
    }

    @Override
    public DocumentForm findByName(String name) {
        return repository.findByName(name);
    }
}
